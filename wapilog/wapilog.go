// Copyright (c) 2023 WAPI TEAM
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package waLog contains a simple logger interface used by the other whatsmeow packages.
package wapilog

import (
	"encoding/json"
	"fmt"
	"path"
	"runtime"
	"strings"
	"time"
)

var log Logger
var logLevel string

// NewWapiLogger экземпляр логгера loglevel строка, servise стиль (жук=bugs)
func NewWapiLogger(loglevel string, servise string, tags ...string) Logger {
	log = Stdout(GetCurrentPackageName(), logLevel, true, servise, tags)
	return log
}

// Logger is a simple logger interface that can have subloggers for specific areas.
type Logger interface {
	Warnf(msg string, args ...interface{})
	Errorf(msg string, args ...interface{})
	Infof(msg string, args ...interface{})
	Debugf(data interface{})
	Sub(module string) Logger
	Resultf(msg string, args ...interface{}) // новый метод
	FlushTags()
	TagsAdd(tags ...string)
}

type noopLogger struct{}

func (n *noopLogger) Errorf(_ string, _ ...interface{})  {}
func (n *noopLogger) Warnf(_ string, _ ...interface{})   {}
func (n *noopLogger) Infof(_ string, _ ...interface{})   {}
func (n *noopLogger) Debugf(data interface{})            {}
func (n *noopLogger) Sub(_ string) Logger                { return n }
func (n *noopLogger) Resultf(_ string, _ ...interface{}) {}
func (n *noopLogger) FlushTags()                         {}
func (n *noopLogger) TagsAdd(...string)                  {}

// Noop is a no-op Logger implementation that silently drops everything.
var Noop Logger = &noopLogger{}

type stdoutLogger struct {
	mod     string
	color   bool
	min     int
	servise string
	tags    []string
}

var colors = map[string]string{
	"INFO":   "\033[36m", // циан
	"WARN":   "\033[33m", // желтый
	"ERROR":  "\033[31m", // красный
	"RESULT": "\033[94m", // светло-синий
	"DEBUG":  "\033[94m", // светло-синий
}

var levelToInt = map[string]int{
	"":       -1,
	"DEBUG":  0,
	"INFO":   1,
	"WARN":   2,
	"ERROR":  3,
	"RESULT": 4, // новый уровень
}

func (s *stdoutLogger) outputf(level, msg string, args ...interface{}) {
	if levelToInt[level] < s.min {
		return
	}
	var colorStart, colorReset string
	if s.color {
		colorStart = colors[level]
		colorReset = "\033[0m"
	}

	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	}
	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}

	if len(s.servise) > 0 {
		fmt.Printf("%s%s [%s %s] [%s] [%s:%d] %s%s\n", time.Now().Format("15:04:05.000"), colorStart, s.mod, level, s.servise, short, line, fmt.Sprintf("%s %s", s.getTagStr(), fmt.Sprintf(msg, args...)), colorReset)
	} else {
		fmt.Printf("%s%s [%s %s]  [%s:%d] %s%s\n", time.Now().Format("15:04:05.000"), colorStart, s.mod, level, short, line, fmt.Sprintf("%s %s", s.getTagStr(), fmt.Sprintf(msg, args...)), colorReset)
	}

}

func (s *stdoutLogger) outputPretter(level string, data interface{}) {
	if levelToInt[level] < s.min {
		return
	}
	var colorStart, colorReset string
	if s.color {
		colorStart = colors[level]
		colorReset = "\033[0m"
	}

	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	}
	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}

	wapiJSON, _ := json.MarshalIndent(data, "", "  ")

	fmt.Printf("%s%s [%s %s]  [%s:%d] %s %s%s\n", time.Now().Format("15:04:05.000"), colorStart, s.mod, level, short, line, s.getTagStr(), wapiJSON, colorReset)
}

func (s *stdoutLogger) Resultf(msg string, args ...interface{}) { s.outputf("RESULT", msg, args...) }
func (s *stdoutLogger) Errorf(msg string, args ...interface{})  { s.outputf("ERROR", msg, args...) }
func (s *stdoutLogger) Warnf(msg string, args ...interface{})   { s.outputf("WARN", msg, args...) }
func (s *stdoutLogger) Infof(msg string, args ...interface{})   { s.outputf("INFO", msg, args...) }
func (s *stdoutLogger) Debugf(data interface{}) {
	s.outputPretter("DEBUG", data)
}
func (s *stdoutLogger) Sub(mod string) Logger {
	return &stdoutLogger{mod: fmt.Sprintf("%s/%s", s.mod, mod), color: s.color, min: s.min}
}

// Stdout is a simple Logger implementation that outputs to stdout. The module name given is included in log lines.
//
// minLevel specifies the minimum log level to output. An empty string will output all logs.
//
// If color is true, then info, warn and error logs will be colored cyan, yellow and red respectively using ANSI color escape codes.
func Stdout(module string, minLevel string, color bool, servise string, tags []string) Logger {
	return &stdoutLogger{mod: module, color: color, min: levelToInt[strings.ToUpper(minLevel)], servise: servise, tags: tags}
}

func GetCurrentPackageName() string {
	_, fullPath, _, ok := runtime.Caller(1) // 1 - это уровень выше по стеку вызовов, т.е. вызывающая функция
	if !ok {
		return "UNKNOWN"
	}

	// Получаем путь к каталогу (пакету) из полного пути
	dir := path.Dir(fullPath)
	return path.Base(dir) // Base возвращает последний элемент пути
}

func (s *stdoutLogger) getTagStr() (tagstr string) {
	for _, tag := range s.tags {
		tagstr += "[" + tag + "]"
	}
	return
}

func (s *stdoutLogger) FlushTags() {
	s.tags = make([]string, 0)
}

func (s *stdoutLogger) TagsAdd(tags ...string) {
	s.tags = append(s.tags, tags...)
}
