package wapihelpers

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"unicode"

	"gitlab.com/wapiteam/wapiteam/config"
)

func PrepareTGRecipient(recipient string) string {
	recipient = strings.TrimPrefix(recipient, "@")
	for _, r := range recipient {
		if unicode.IsLetter(r) {
			return recipient // Возвращаем исходную строку, если есть букв
		}
	}

	recipient = strings.ReplaceAll(recipient, " ", "") // Удаляем пробелы
	recipient = strings.TrimPrefix(recipient, "+")     // Удаляем '+' в начале строки

	// Удаляем все недопустимые символы, оставляя только цифры
	digits := ""
	for _, r := range recipient {
		if unicode.IsDigit(r) {
			digits += string(r)
		}
	}

	// Если номер начинается на '89' и длина 11 символов, заменяем '8' на '7'
	if len(digits) == 11 && strings.HasPrefix(digits, "89") {
		digits = "7" + digits[1:]
	}

	return digits

}

func SendTgMsg(msg string, channelName string) error {
	config, err := config.NewWapiConfig()
	if err != nil {
		return err
	}

	var channel string
	switch channelName {
	case "wappistat":
		channel = "-824582735"
	case "zhlupek":
		channel = "-1001253677919"
	}

	tgAPI := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage", config.BOT_TOKEN)

	data := url.Values{}
	data.Set("chat_id", channel)
	data.Set("text", msg)

	req, err := http.NewRequest(http.MethodPost, tgAPI, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		fmt.Println("body: " + string(bodyBytes))
		return fmt.Errorf("unexpected response status code: %d", resp.StatusCode)
	}

	return nil
}

func YaDiskSupport(source_url string) string {
	if strings.Contains(source_url, "yadi.sk") || strings.Contains(source_url, "disk.yandex.ru") {
		return "https://getfile.dokpub.com/yandex/get/" + source_url
	} else {
		return source_url
	}
}
