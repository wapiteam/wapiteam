package hurricanecli

type SocketMsg struct {
	Type string `json:"type,omitempty"`
	Data Data   `json:"data,omitempty"`
}

type Data struct {
	ProfileId     string                 `json:"profile_id,omitempty"`
	Status        interface{}            `json:"status,omitempty"`
	Reason        string                 `json:"reason,omitempty"`
	Token         string                 `json:"token,omitempty"`
	QR            string                 `json:"qr,omitempty"`
	Phone         string                 `json:"phone,omitempty"`
	URL           string                 `json:"url,omitempty"`
	Message       map[string]interface{} `json:"message,omitempty"`
	Timeout       float64                `json:"timeout,omitempty"`
	MassPostingID string                 `json:"mass_posting_id,omitempty"`
	Platform      string                 `json:"platform,omitempty"`
	Floodwait     int                    `json:"floodwait,omitempty"`
}
