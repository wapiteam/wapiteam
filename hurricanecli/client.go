package hurricanecli

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/wapiteam/wapiteam/wapi"
	"gitlab.com/wapiteam/wapiteam/wapilog"
	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

var log wapilog.Logger

type HurricaneClient struct {
	exchangeName string
	queueName    string
	broker       *wapirabbit.AmqpBroker
}

func init() {
	log = wapilog.NewWapiLogger("INFO", "")
}

// NewHuricanClient - экземпляр хурикан клиента
func NewHuricanClient(broker *wapirabbit.AmqpBroker, exchangeName string, queueName string) *HurricaneClient {
	return &HurricaneClient{
		exchangeName: exchangeName,
		queueName:    queueName,
		broker:       broker,
	}
}

/*
Send HurricaneClient Message - послать сообщение в хурикан

доступные типы сообщений:

wa_status_open

wa_status_connecting

wa_status_close

update_auth_false

update_auth_true

need_restore_false

need_restore_true

send_qr_code

restart_container

save_phone

retry_webhook

stop_and_start_continer

synced_false

synced_true
*/
func (hc *HurricaneClient) Send(ctx context.Context, allWapi *wapi.Wapi, event string) error {
	var socketWsConn SocketMsg
	switch event {
	case "wa_status_open":
		socketWsConn = SocketMsg{
			Type: "update_wa_status",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Status:    "open",
				Phone:     allWapi.Profile.Phone,
			},
		}
		allWapi.Profile.Authorized = true
	case "wa_status_connecting":
		socketWsConn = SocketMsg{
			Type: "update_wa_status",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Status:    "connecting",
			},
		}
	case "wa_status_close":
		socketWsConn = SocketMsg{
			Type: "update_wa_status",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Status:    "close",
			},
		}
	case "save_phone":
		socketWsConn = SocketMsg{
			Type: "save_phone",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Phone:     allWapi.Profile.Phone,
			},
		}
	case "update_auth_false_without_docker":
		socketWsConn = SocketMsg{
			Type: "update_auth_false_without_docker",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "floodwait":
		if allWapi.Profile.Floodwait != nil {
			socketWsConn = SocketMsg{
				Type: "floodwait",
				Data: Data{
					Token:     allWapi.Token.Key,
					ProfileId: allWapi.Profile.UUID,
					Floodwait: *allWapi.Profile.Floodwait,
				},
			}
		} else {
			return fmt.Errorf("floodwait is nil")
		}
	case "update_auth_false":
		socketWsConn = SocketMsg{
			Type: "update_auth",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Status:    false,
				Reason:    allWapi.Authorization.Reason,
			},
		}
		allWapi.Profile.Authorized = false
	case "update_auth_true":
		socketWsConn = SocketMsg{
			Type: "update_auth",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Platform:  allWapi.Profile.Platform,
				Status:    true,
			},
		}
		allWapi.Profile.Authorized = true
	case "need_restore":
		socketWsConn = SocketMsg{
			Type: "need_restore",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Status:    true,
			},
		}
	case "need_restore_true":
		socketWsConn = SocketMsg{
			Type: "need_restore_true",
			Data: Data{
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "need_restore_false":
		socketWsConn = SocketMsg{
			Type: "need_restore_false",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "send_qr_code":
		socketWsConn = SocketMsg{
			Type: "send_qr_code",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				QR:        *allWapi.Profile.QR,
			},
		}
	case "restart_container":
		socketWsConn = SocketMsg{
			Type: "restart_container",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "change_proxy_pool":
		socketWsConn = SocketMsg{
			Type: "change_proxy_pool",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "stop_and_start_continer":
		socketWsConn = SocketMsg{
			Type: "stop_and_start_continer",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
				Timeout:   allWapi.Profile.TemporyBan,
			},
		}
	case "retry_webhook":
		socketWsConn = SocketMsg{
			Type: "retry_webhook",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "synced_true":
		socketWsConn = SocketMsg{
			Type: "synced_true",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "2fa":
		socketWsConn = SocketMsg{
			Type: "2fa",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	case "synced_false":
		socketWsConn = SocketMsg{
			Type: "synced_false",
			Data: Data{
				Token:     allWapi.Token.Key,
				ProfileId: allWapi.Profile.UUID,
			},
		}
	default:
		socketWsConn = SocketMsg{
			Type: "mass_posting",
			Data: Data{
				Token:         allWapi.Token.Key,
				ProfileId:     allWapi.Profile.UUID,
				MassPostingID: event,
			},
		}
	}

	jsonMsg, err := json.Marshal(socketWsConn)
	if err != nil {
		log.Errorf("Ошибка Marshal HurricaneClient message: %s", err.Error())
		return err
	}

	if err := hc.broker.Publish(ctx, hc.exchangeName, hc.queueName, jsonMsg); err != nil {
		log.Errorf("error HurricaneClient Publish: %s", err.Error())
		return err
	}

	log.Resultf("HurricaneClient Publish succes {msg_type: %s}	{profile.uuid: %s}	{status: %s}", socketWsConn.Type, socketWsConn.Data.ProfileId, socketWsConn.Data.Status)
	return nil
}
