package wapirabbit

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/wapiteam/wapiteam/wapierror"

	"github.com/rabbitmq/amqp091-go"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/wapiteam/wapiteam/wapilog"
)

var (
	log            wapilog.Logger
	confirmsDoneCh = make(chan struct{})
	publishOkCh    = make(chan struct{}, 1)
	confirmsCh     = make(chan *amqp.DeferredConfirmation)
	exitCh         = make(chan struct{})
)

type AmqpBroker struct {
	Uri            string
	Vhost          string
	Properties     amqp.Table
	connection     *amqp.Connection
	SleepDialTimer time.Duration
	OnReconnect    func(amqpBroker *AmqpBroker)
	AmqpMessage    chan amqp091.Delivery
	ConsumeChannel *amqp091.Channel
}

func init() {
	log = wapilog.NewWapiLogger("INFO", "")
}

// NewBroker создание экземпляра брокера с соединением внутри, необходимо задавать имя соединения
func NewBroker(uri string, vhost string, connectionName string) (*AmqpBroker, error) {
	amqpBroker := &AmqpBroker{
		Uri:            uri,
		Vhost:          vhost,
		SleepDialTimer: 10 * time.Second,
	}
	config := amqp.Config{
		Vhost:      amqpBroker.Vhost,
		Properties: amqp.NewConnectionProperties(),
	}
	config.Properties.SetClientConnectionName(connectionName)

	amqpBroker.AmqpMessage = make(chan amqp091.Delivery)

	conn, err := amqp.DialConfig(amqpBroker.Uri, config)
	if err != nil {
		return nil, &wapierror.WapiError{
			Message:   "rabbit connection error: " + err.Error(),
			ErrorCode: 700,
		}
	}
	amqpBroker.connection = conn
	go amqpBroker.handleReconnect(config)

	return amqpBroker, nil
}

func (amqpBroker *AmqpBroker) handleReconnect(config amqp.Config) {
	for {
		log.Infof("Слушаю канал закрытия соединения")
		err, ok := <-amqpBroker.connection.NotifyClose(make(chan *amqp.Error))
		if !ok {
			break
		}
		log.Errorf("Сломался реббит пытаюсь переконнектиться: %s", err)
		if amqpBroker.ConsumeChannel != nil {
			amqpBroker.ConsumeChannel.Close()
		}

		for {
			conn, err := amqp.DialConfig(amqpBroker.Uri, config)
			if err != nil {
				log.Errorf("Сломался реббит пытаюсь переконнектиться: %s", err)
				time.Sleep(amqpBroker.SleepDialTimer)
				continue
			} else {
				log.Infof("Восстановил коннект к реббиту")
				amqpBroker.connection = conn
				if amqpBroker.ConsumeChannel != nil {
					amqpBroker.ConsumeChannel.Close()
				}
				if amqpBroker.OnReconnect != nil {
					amqpBroker.OnReconnect(amqpBroker)
				}

				go amqpBroker.handleReconnect(config)
				return
			}
		}
	}
}

func (ab *AmqpBroker) BackReconnect(OnReconnect func(amqpBroker *AmqpBroker)) {
	ab.OnReconnect = OnReconnect
}

func (ab *AmqpBroker) Publish(ctx context.Context, exchange string, routingKey string, data []byte) error {
	channel, err := ab.connection.Channel()
	if err != nil {
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	if err := channel.Confirm(false); err != nil {
		log.Errorf("channel.Confirm error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit channel.Confirm error: " + err.Error(),
			ErrorCode: 705,
		}
	}

	_, err = channel.PublishWithDeferredConfirmWithContext(
		ctx,
		exchange,
		routingKey,
		true,
		false,
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			DeliveryMode:    amqp.Persistent,
			Priority:        0,
			AppId:           "sequential-producer",
			Body:            data,
		},
	)
	if err != nil {
		log.Errorf("error in publish: %s", err)
	}

	return nil
}

// Consume создание именного консьюмера (под капотом запускается горутина по которой мы итерируемся)
//
// возвращает:
//
// chan amqp.Delivery - непосредственно go канал для получений сообщений
//
// *amqp.Channel - rabbitmq канал в рамках соединения, по которому мы получаем сообщения (это нужно чтобы потом в дефере его закрыть)
//
// error - типа WapiError
func (ab *AmqpBroker) Consume(ctx context.Context, queue string, tag string, autoAck bool) (<-chan amqp.Delivery, *amqp.Channel, error) {

	channel, err := ab.connection.Channel()
	if err != nil {
		return nil, nil, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	deliveries, err := channel.Consume(
		queue,   // name
		tag,     // consumerTag,
		autoAck, // autoAck
		false,   // exclusive
		false,   // noLocal
		false,   // noWait
		nil,     // arguments
	)
	if err != nil {
		return nil, nil, fmt.Errorf("Queue Consume: %s", err)
	}

	return deliveries, channel, nil
}

func (ab *AmqpBroker) ConsumeInBroker(ctx context.Context, queue string, tag string, autoAck bool) error {
	var err error
	ab.ConsumeChannel, err = ab.connection.Channel()
	if err != nil {
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	deliveries, err := ab.ConsumeChannel.Consume(
		queue,   // name
		tag,     // consumerTag,
		autoAck, // autoAck
		false,   // exclusive
		false,   // noLocal
		false,   // noWait
		nil,     // arguments
	)
	if err != nil {
		return fmt.Errorf("Queue Consume: %s", err)
	}

	go func() {
		for message := range deliveries {
			ab.AmqpMessage <- message
			if err != nil {
				fmt.Println(err)
			}
		}
	}()

	return nil
}

func (ab *AmqpBroker) ConsumeWithQOS(ctx context.Context, queue string, tag string, autoAck bool, prefetchCount int, prefetchSize int, global bool) (<-chan amqp.Delivery, *amqp.Channel, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		return nil, nil, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	channel.Qos(prefetchCount, prefetchSize, global)

	deliveries, err := channel.Consume(
		queue,   // name
		tag,     // consumerTag,
		autoAck, // autoAck
		false,   // exclusive
		false,   // noLocal
		false,   // noWait
		nil,     // arguments
	)
	if err != nil {
		return nil, nil, fmt.Errorf("Queue Consume: %s", err)
	}

	return deliveries, channel, nil
}

func (ab *AmqpBroker) ConsumeWithQOSIbBroker(ctx context.Context, queue string, tag string, autoAck bool, prefetchCount int, prefetchSize int, global bool) error {
	var err error
	ab.ConsumeChannel, err = ab.connection.Channel()
	if err != nil {
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	ab.ConsumeChannel.Qos(prefetchCount, prefetchSize, global)

	deliveries, err := ab.ConsumeChannel.Consume(
		queue,   // name
		tag,     // consumerTag,
		autoAck, // autoAck
		false,   // exclusive
		false,   // noLocal
		false,   // noWait
		nil,     // arguments
	)
	if err != nil {
		return fmt.Errorf("Queue Consume: %s", err)
	}

	go func() {
		for message := range deliveries {
			ab.AmqpMessage <- message
			if err != nil {
				fmt.Println(err)
			}
		}
	}()

	return nil
}

// ExchangeDeclare создание эксченжа, по дефолту exchangeType = direct, доступны варианты: fanout|topic|x-custom
func (ab *AmqpBroker) ExchangeDeclare(ctx context.Context, exchange string, exchangeType string, durable bool, autodelete bool) error {
	channel, err := ab.connection.Channel()
	if err != nil {
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		durable,      // durable
		autodelete,   // auto-delete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		log.Errorf("ExchangeDeclare error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit ExchangeDeclare error: " + err.Error(),
			ErrorCode: 702,
		}
	} else {
		log.Infof("declaring exchange %s, type %s", exchange, exchangeType)
	}

	return nil
}

// QueueDeclare создание очереди и бинд ее к эксченжу
func (ab *AmqpBroker) QueueDeclare(ctx context.Context, queue string, routingKey string, exchange string, durable bool, settings amqp.Table, autodelete bool) (amqp.Queue, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	q, err := channel.QueueDeclare(
		queue,      // name of the queue
		durable,    // durable
		autodelete, // delete when unused
		false,      // exclusive
		false,      // noWait
		settings,   // arguments
	)
	if err != nil {
		log.Errorf("QueueDeclare error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit QueueDeclare error: " + err.Error(),
			ErrorCode: 703,
		}
	} else {
		log.Infof("QueueDeclare (%q %d messages, %d consumers), binding to Exchange (key %q)",
			q.Name, q.Messages, q.Consumers, routingKey)
	}

	if err := channel.QueueBind(q.Name, routingKey, exchange, false, nil); err != nil {
		log.Errorf("Queue Bind: %s", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit Queue Bind error: " + err.Error(),
			ErrorCode: 704,
		}
	}

	return q, nil
}

func (ab *AmqpBroker) ExclusiveQueueDeclare(ctx context.Context, queue string, routingKey string, exchange string, durable bool, settings amqp.Table, autodelete bool) (amqp.Queue, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	q, err := channel.QueueDeclare(
		queue,      // name of the queue
		durable,    // durable
		autodelete, // delete when unused
		true,       // exclusive
		false,      // noWait
		settings,   // arguments
	)
	if err != nil {
		log.Errorf("QueueDeclare error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit QueueDeclare error: " + err.Error(),
			ErrorCode: 703,
		}
	} else {
		log.Infof("QueueDeclare (%q %d messages, %d consumers), binding to Exchange (key %q)",
			q.Name, q.Messages, q.Consumers, routingKey)
	}

	if err := channel.QueueBind(q.Name, routingKey, exchange, false, nil); err != nil {
		log.Errorf("Queue Bind: %s", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit Queue Bind error: " + err.Error(),
			ErrorCode: 704,
		}
	}

	return q, nil
}

func (ab *AmqpBroker) QueueDeclarePassive(ctx context.Context, queue string) (amqp.Queue, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	q, err := channel.QueueDeclarePassive(
		queue, // name of the queue
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)

	if err != nil {
		log.Errorf("QueueDeclare error: %s ", err.Error())
		return amqp.Queue{}, &wapierror.WapiError{
			Message:   "rabbit QueueDeclare error: " + err.Error(),
			ErrorCode: 703,
		}
	}

	return q, nil
}

func (ab *AmqpBroker) QueueDelete(ctx context.Context, queue string, key string, exchange string) error {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	if err := channel.QueueUnbind(
		queue,    // name of the queue
		key,      // delete when unused
		exchange, // exclusive
		nil,      // noWait
	); err != nil {
		log.Errorf("QueueUnbind error: %s ", err.Error())
	}

	if _, err := channel.QueueDelete(
		queue, // name of the queue
		false, // delete when unused
		false, // exclusive
		true,  // noWait
	); err != nil {
		log.Errorf("QueueDelete error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit QueueDelete error: " + err.Error(),
			ErrorCode: 703,
		}
	}
	return nil
}

func (ab *AmqpBroker) ExchangeDelete(ctx context.Context, exchange string) error {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	if err := channel.ExchangeDelete(
		exchange, // name of the queue
		false,    // delete when unused
		true,     // noWait
	); err != nil {
		log.Errorf("QueueDelete error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit QueueDelete error: " + err.Error(),
			ErrorCode: 703,
		}
	}
	return nil
}

func (ab *AmqpBroker) GetChannel(prefetchCount int, prefetchSize int, global bool) (*amqp.Channel, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return nil, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	channel.Qos(prefetchCount, prefetchSize, global)

	return channel, nil
}

func (ab *AmqpBroker) QueuePurge(queue string) (int, error) {
	channel, err := ab.connection.Channel()
	if err != nil {
		log.Errorf("QueueDeclare channel create error: %s ", err.Error())
		return 0, &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}

	defer channel.Close()

	count, err := channel.QueuePurge(queue, false)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (ab *AmqpBroker) PublishWithIncrement(ctx context.Context, exchange string, routingKey string, data []byte, retryCount int32) error {
	channel, err := ab.connection.Channel()
	if err != nil {
		return &wapierror.WapiError{
			Message:   "rabbit channel create error: " + err.Error(),
			ErrorCode: 701,
		}
	}
	defer channel.Close()

	if err := channel.Confirm(false); err != nil {
		log.Errorf("channel.Confirm error: %s ", err.Error())
		return &wapierror.WapiError{
			Message:   "rabbit channel.Confirm error: " + err.Error(),
			ErrorCode: 705,
		}
	}

	_, err = channel.PublishWithDeferredConfirmWithContext(
		ctx,
		exchange,
		routingKey,
		true,
		false,
		amqp.Publishing{
			Headers:         amqp.Table{"x-retry-count": retryCount},
			ContentType:     "text/plain",
			ContentEncoding: "",
			DeliveryMode:    amqp.Persistent,
			Priority:        0,
			AppId:           "sequential-producer",
			Body:            data,
		},
	)
	if err != nil {
		log.Errorf("error in publish: %s", err)
	}

	return nil
}
