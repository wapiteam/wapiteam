package wapierror

import "fmt"

type WapiError struct {
	Message   string
	ErrorCode int
}

func (we *WapiError) Error() string {
	return fmt.Sprintf("ErrorCode: %d, Message: %s", we.ErrorCode, we.Message)
}
