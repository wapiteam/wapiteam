package wapis3

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/url"
	"time"

	"github.com/google/uuid"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/wapiteam/wapiteam/mimechecker"
	"gitlab.com/wapiteam/wapiteam/wapi"
)

type WapiS3 struct {
	s3Client *minio.Client
	location string
}

func NewWapiS3(key string, keyID string) (*WapiS3, error) {

	s3Client, err := minio.New("s3.wappi.pro", &minio.Options{
		Creds:  credentials.NewStaticV4(keyID, key, ""),
		Secure: true,
	})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &WapiS3{
		s3Client: s3Client,
		location: "us-east-1",
	}, nil
}

func (s *WapiS3) PutAudioConverter(ctx context.Context, file []byte) (string, string, error) {
	fileName := uuid.NewString() + ".mp3"

	if err := s.PutBackUp(ctx, fileName, file, "bitrix-voicemessages-wapi"); err != nil {
		return "", "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "bitrix-voicemessages-wapi", fileName, 7*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", "", err
	}

	return resignedURL.String(), fileName, nil
}

func (s *WapiS3) isBucketExists(ctx context.Context, bucket string) (bool, error) {
	exists, errBucketExists := s.s3Client.BucketExists(ctx, bucket)
	if errBucketExists != nil {
		return false, errBucketExists
	}

	if !exists {
		return false, fmt.Errorf("bucket not found")
	}

	return true, nil
}

func (s *WapiS3) IsBackUpExist(ctx context.Context, backUpName string, bucket string) (bool, error) {

	isEx, err := s.isBucketExists(ctx, bucket)
	if err != nil {
		return false, err
	}

	if !isEx {
		return false, fmt.Errorf("bucket not found")
	}

	objectCh := s.s3Client.ListObjects(context.Background(), bucket, minio.ListObjectsOptions{Recursive: true})
	for object := range objectCh {
		if object.Err != nil {
			return false, object.Err
		}

		if object.Key == backUpName {
			return true, nil
		}
	}

	return false, fmt.Errorf("IsBackUpExist backUp: %s, was not found", backUpName)
}

func (s *WapiS3) PutBackUp(ctx context.Context, backUpName string, data []byte, bucket string) error {
	if data == nil {
		return fmt.Errorf("file is nil")
	}

	reader := bytes.NewReader(data)
	fileSize := int64(len(data))

	_, err := s.s3Client.PutObject(context.TODO(), bucket, backUpName, reader, fileSize, minio.PutObjectOptions{ContentType: "application/octet-stream", ContentDisposition: `attachment; filename*=UTF-8''` + url.PathEscape(backUpName)})
	if err != nil {
		log.Errorf("Ошибка при загрузке файла: %s", err)
		return err
	}

	log.Resultf("Файл успешно загружен: %s", backUpName)
	return nil

}

func (s *WapiS3) PutBackUpBitrix24HWithLink(ctx context.Context, backUpName string, data []byte) (string, error) {
	if err := s.PutBackUp(ctx, "bitrix24/"+backUpName, data, "wapi-uploads24"); err != nil {
		return "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads24", "bitrix24/"+backUpName, 24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", err
	}

	return resignedURL.String(), nil
}

func (s *WapiS3) PutBackUp7DaysHWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error) {
	if err := s.PutBackUp(ctx, uuid+"/"+backUpName, data, "wapi-uploads7d"); err != nil {
		return "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads7d", uuid+"/"+backUpName, 7*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", err
	}

	return resignedURL.String(), nil
}

func (s *WapiS3) PutBackUp180DaysWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error) {
	if err := s.PutBackUp(ctx, uuid+"/"+backUpName, data, "wapi-uploads30"); err != nil {
		return "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads30", uuid+"/"+backUpName, 7*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", err
	}

	return resignedURL.String(), nil
}

func (s *WapiS3) DeleteBackUp(ctx context.Context, backUpName string, bucket string) error {

	isEx, err := s.IsBackUpExist(ctx, backUpName, bucket)
	if err != nil {
		return err
	}

	if !isEx {
		return fmt.Errorf("backUp: %s, was not found", backUpName)
	}

	if err := s.s3Client.RemoveObject(ctx, bucket, backUpName, minio.RemoveObjectOptions{}); err != nil {
		log.Errorf("Ошибка при удалении объекта: %s", err)
		return err
	}
	return nil
}

func (s *WapiS3) GetBackUp(ctx context.Context, backUpName string, bucket string) ([]byte, error) {

	isEx, err := s.IsBackUpExist(ctx, backUpName, bucket)
	if err != nil {
		return nil, err
	}

	if !isEx {
		return nil, fmt.Errorf("backUp: %s, was not found", backUpName)
	}

	object, err := s.s3Client.GetObject(ctx, bucket, backUpName, minio.GetObjectOptions{})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer object.Close()

	data, err := io.ReadAll(object)
	if err != nil {
		log.Errorf("Ошибка при чтении данных из объекта: %s", err)
		return nil, err
	}

	return data, nil
}

func (s *WapiS3) Put7DayLinkFilename(ctx context.Context, evt *wapi.IncomingOutgoingMessage, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	if evt.Type == "chat" {
		return "", "", fmt.Errorf("this is not media")
	}

	var fileName string

	if allWapi.User.ID == 165 {
		extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
		fileName = uuid.NewString() + "." + extension
	} else {
		if len(evt.FileName) > 0 {
			fileName = evt.FileName
		} else {
			extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
			fileName = uuid.NewString() + "." + extension
		}
	}

	if err := s.PutBackUp(ctx, allWapi.Profile.UUID+"/"+fileName, file, "wapi-uploads7d"); err != nil {
		return "", "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads7d", allWapi.Profile.UUID+"/"+fileName, 7*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", "", err
	}

	return resignedURL.String(), fileName, nil

}

func (s *WapiS3) Put2DayLinkFilename(ctx context.Context, evt *wapi.IncomingOutgoingMessage, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	if evt.Type == "chat" {
		return "", "", fmt.Errorf("this is not media")
	}

	var fileName string

	if allWapi.User.ID == 165 {
		extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
		fileName = uuid.NewString() + "." + extension
	} else {
		if len(evt.FileName) > 0 {
			fileName = evt.FileName
		} else {
			extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
			fileName = uuid.NewString() + "." + extension
		}
	}

	if err := s.PutBackUp(ctx, allWapi.Profile.UUID+"/"+fileName, file, "wapi-uploads7d"); err != nil {
		return "", "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads7d", allWapi.Profile.UUID+"/"+fileName, 7*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", "", err
	}

	return resignedURL.String(), fileName, nil

}

func (s *WapiS3) Put3DayLinkFilename(ctx context.Context, fileName string, mime string, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	var newFileName string

	if len(fileName) > 0 {
		newFileName = fileName
	} else {
		if mime != "" {
			extension := mimechecker.GetExtensionFromMime(mime)
			newFileName = uuid.NewString() + "." + extension
		} else {
			newFileName = uuid.NewString()
		}
	}

	if err := s.PutBackUp(ctx, allWapi.Profile.UUID+"/"+newFileName, file, "wapi-uploads3d"); err != nil {
		return "", "", err
	}

	resignedURL, err := s.s3Client.PresignedGetObject(context.Background(), "wapi-uploads3d", allWapi.Profile.UUID+"/"+newFileName, 3*24*time.Hour, nil)
	if err != nil {
		fmt.Println("Failed to generate presigned URL", err)
		return "", "", err
	}

	return resignedURL.String(), newFileName, nil

}
