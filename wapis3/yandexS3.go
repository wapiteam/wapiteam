package wapis3

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/google/uuid"
	"gitlab.com/wapiteam/wapiteam/mimechecker"
	"gitlab.com/wapiteam/wapiteam/wapi"
	"gitlab.com/wapiteam/wapiteam/wapilog"
)

var log = wapilog.NewWapiLogger("INFO", "")

type YandexS3 struct {
	s3Client *s3.Client
}

func NewYandexS3(key string, keyID string) (*YandexS3, error) {
	os.Setenv("AWS_CONFIG_FILE", "")
	os.Setenv("AWS_SHARED_CREDENTIALS_FILE", "")
	os.Setenv("AWS_SECRET_ACCESS_KEY", key)
	os.Setenv("AWS_ACCESS_KEY_ID", keyID)
	os.Setenv("AWS_DEFAULT_REGION", "ru-central1")
	customResolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
		if service == s3.ServiceID && region == "ru-central1" {
			return aws.Endpoint{
				PartitionID:   "yc",
				URL:           "https://storage.yandexcloud.net",
				SigningRegion: "ru-central1",
			}, nil
		}
		return aws.Endpoint{}, fmt.Errorf("unknown endpoint requested")
	})

	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithEndpointResolverWithOptions(customResolver))

	if err != nil {
		log.Errorf("error load s3 config %s", err.Error())
		return nil, err
	}

	log.Resultf("s3 init success")

	return &YandexS3{
		s3Client: s3.NewFromConfig(cfg),
	}, nil
}

func (s *YandexS3) PutAudioConverter(ctx context.Context, file []byte) (string, string, error) {
	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	r := bytes.NewReader(file)

	fileName := uuid.NewString() + ".mp3"

	enable := true
	input := &s3.PutObjectInput{
		Bucket:           aws.String("bitrix-voicemessages-wapi"),
		Key:              aws.String(fileName),
		Body:             r,
		BucketKeyEnabled: &enable,
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("bitrix-voicemessages-wapi"),
		Key:    aws.String(fileName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 7 * 24 * time.Hour
	})
	if err != nil {
		return "", "", err
	}

	return presignedReq.URL, fileName, nil
}

func (s *YandexS3) IsBackUpExist(ctx context.Context, backUpName string, bucket string) (bool, error) {
	result, err := s.s3Client.ListObjectsV2(context.TODO(), &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
	})
	if err != nil {
		return false, err
	}

	for _, object := range result.Contents {
		if strings.Contains(backUpName, aws.ToString(object.Key)) {
			return true, nil
		}
	}

	return false, fmt.Errorf("IsBackUpExist backUp: %s, was not found", backUpName)
}

func (s *YandexS3) PutBackUp(ctx context.Context, backUpName string, data []byte, bucket string) error {
	if data == nil {
		return fmt.Errorf("file is nil")
	}

	time.Sleep(10 * time.Second)

	r := bytes.NewReader(data)

	if _, err := s.s3Client.PutObject(ctx, &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(backUpName),
		Body:   r,
	}); err != nil {
		log.Errorf("PutBackUp error: %s", err.Error())
		return err
	}

	return nil
}

func (s *YandexS3) PutBackUpAMO24HWithLink(ctx context.Context, backUpName string, data io.Reader) (string, error) {

	if data == nil {
		return "", fmt.Errorf("file is nil")
	}

	enable := true
	input := &s3.PutObjectInput{
		Bucket:             aws.String("wapi-uploads24"),
		Key:                aws.String("bitrix24/" + backUpName),
		Body:               data,
		BucketKeyEnabled:   &enable,
		ContentDisposition: aws.String(`attachment; filename*=UTF-8''` + url.PathEscape(backUpName)),
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads24"),
		Key:    aws.String("bitrix24/" + backUpName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 24 * time.Hour
	})
	if err != nil {
		return "", err
	}

	return presignedReq.URL, nil
}

func (s *YandexS3) PutBackUpBitrix24HWithLink(ctx context.Context, backUpName string, data []byte) (string, error) {

	if data == nil {
		return "", fmt.Errorf("file is nil")
	}

	r := bytes.NewReader(data)
	enable := true
	input := &s3.PutObjectInput{
		Bucket:             aws.String("wapi-uploads24"),
		Key:                aws.String("bitrix24/" + backUpName),
		Body:               r,
		BucketKeyEnabled:   &enable,
		ContentDisposition: aws.String(`attachment; filename*=UTF-8''` + url.PathEscape(backUpName)),
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads24"),
		Key:    aws.String("bitrix24/" + backUpName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 24 * time.Hour
	})
	if err != nil {
		return "", err
	}

	return presignedReq.URL, nil
}

func (s *YandexS3) PutBackUp7DaysHWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error) {
	if data == nil {
		return "", fmt.Errorf("file is nil")
	}

	r := bytes.NewReader(data)

	enable := true
	input := &s3.PutObjectInput{
		Bucket:           aws.String("wapi-uploads7d"),
		Key:              aws.String(uuid + "/" + backUpName),
		Body:             r,
		BucketKeyEnabled: &enable,
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads7d"),
		Key:    aws.String(uuid + "/" + backUpName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 7 * 24 * time.Hour
	})
	if err != nil {
		return "", err
	}

	return presignedReq.URL, nil
}

func (s *YandexS3) PutBackUp180DaysWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error) {
	if data == nil {
		return "", fmt.Errorf("file is nil")
	}

	r := bytes.NewReader(data)

	enable := true
	input := &s3.PutObjectInput{
		Bucket:           aws.String("wapi-uploads30"),
		Key:              aws.String(uuid + "/" + backUpName),
		Body:             r,
		BucketKeyEnabled: &enable,
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads30"),
		Key:    aws.String(uuid + "/" + backUpName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 30 * 24 * time.Hour
	})
	if err != nil {
		return "", err
	}

	return presignedReq.URL, nil
}

func (s *YandexS3) DeleteBackUp(ctx context.Context, backUpName string, bucket string) error {
	isExist, err := s.IsBackUpExist(ctx, backUpName, bucket)
	if err != nil {
		return err
	}

	if isExist {
		request := &s3.DeleteObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(backUpName),
		}

		if _, err := s.s3Client.DeleteObject(ctx, request); err != nil {
			return err
		}
		return nil
	} else {
		return fmt.Errorf("DeleteBackUp not found backUp: %s", backUpName)
	}
}

func (s *YandexS3) GetBackUp(ctx context.Context, backUpName string, bucket string) ([]byte, error) {
	// isExist, err := s.IsBackUpExist(ctx, backUpName, bucket)
	// if err != nil {
	// 	return nil, err
	// }

	// if isExist {
	file_backup, err := s.s3Client.GetObject(ctx, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(backUpName),
	})

	if err != nil {
		return nil, err
	}

	data, err := io.ReadAll(file_backup.Body)
	if err != nil {
		return nil, err
	}

	defer file_backup.Body.Close()

	return data, nil
	// } else {
	// 	return nil, fmt.Errorf("GetBackUp not found backUp: %s", backUpName)
	// }
}

func (s *YandexS3) Put7DayLinkFilename(ctx context.Context, evt *wapi.IncomingOutgoingMessage, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	if evt.Type == "chat" {
		return "", "", fmt.Errorf("this is not media")
	}

	//qwe
	r := bytes.NewReader(file)

	var fileName string

	if allWapi.User.ID == 165 {
		extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
		fileName = uuid.NewString() + "." + extension
	} else {
		if len(evt.FileName) > 0 {
			fileName = evt.FileName
		} else {
			extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
			fileName = uuid.NewString() + "." + extension
		}
	}

	enable := true
	input := &s3.PutObjectInput{
		Bucket:           aws.String("wapi-uploads7d"),
		Key:              aws.String(allWapi.Profile.UUID + "/" + fileName),
		Body:             r,
		BucketKeyEnabled: &enable,
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads7d"),
		Key:    aws.String(allWapi.Profile.UUID + "/" + fileName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 7 * 24 * time.Hour
	})
	if err != nil {
		return "", "", err
	}

	return presignedReq.URL, fileName, nil

}

func (s *YandexS3) Put2DayLinkFilename(ctx context.Context, evt *wapi.IncomingOutgoingMessage, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	if evt.Type == "chat" {
		return "", "", fmt.Errorf("this is not media")
	}

	r := bytes.NewReader(file)

	var fileName string

	if allWapi.User.ID == 165 {
		extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
		fileName = uuid.NewString() + "." + extension
	} else {
		if len(evt.FileName) > 0 {
			fileName = evt.FileName
		} else {
			extension := mimechecker.GetExtensionFromMime(evt.Mimetype)
			fileName = uuid.NewString() + "." + extension
		}
	}

	enable := true
	input := &s3.PutObjectInput{
		Bucket:             aws.String("wapi-uploads7d"),
		Key:                aws.String(allWapi.Profile.UUID + "/" + fileName),
		Body:               r,
		BucketKeyEnabled:   &enable,
		ContentDisposition: aws.String(`attachment; filename*=UTF-8''` + url.PathEscape(fileName)),
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads7d"),
		Key:    aws.String(allWapi.Profile.UUID + "/" + fileName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 2 * 24 * time.Hour
	})
	if err != nil {
		return "", "", err
	}

	return presignedReq.URL, fileName, nil

}

func (s *YandexS3) Put3DayLinkFilename(ctx context.Context, fileName string, mime string, file []byte, allWapi *wapi.Wapi) (string, string, error) {

	if file == nil {
		return "", "", fmt.Errorf("file is nil")
	}

	r := bytes.NewReader(file)

	var newFileName string

	if len(fileName) > 0 {
		newFileName = fileName
	} else {
		if mime != "" {
			extension := mimechecker.GetExtensionFromMime(mime)
			newFileName = uuid.NewString() + "." + extension
		} else {
			newFileName = uuid.NewString()
		}
	}

	enable := true
	input := &s3.PutObjectInput{
		Bucket:             aws.String("wapi-uploads3d"),
		Key:                aws.String(allWapi.Profile.UUID + "/" + newFileName),
		Body:               r,
		BucketKeyEnabled:   &enable,
		ContentDisposition: aws.String(`attachment; filename*=UTF-8''` + url.PathEscape(newFileName)),
	}

	if _, err := s.s3Client.PutObject(ctx, input); err != nil {
		return "", "", err
	}

	presignClient := s3.NewPresignClient(s.s3Client)
	params := &s3.GetObjectInput{
		Bucket: aws.String("wapi-uploads3d"),
		Key:    aws.String(allWapi.Profile.UUID + "/" + newFileName),
	}

	presignedReq, err := presignClient.PresignGetObject(ctx, params, func(options *s3.PresignOptions) {
		options.Expires = 3 * 24 * time.Hour
	})
	if err != nil {
		return "", "", err
	}

	return presignedReq.URL, newFileName, nil

}
