package sheets_client

type SheetsEvent struct {
	Event         string  `json:"event"`
	MessageID     string  `json:"message_id"`
	MassPostingID *string `json:"mass_posting_id"`
	ProfileUUID   string  `json:"profile_uuid"`
}
