package sheets_client

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type SheetsClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewSheetsClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *SheetsClient {
	return &SheetsClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *SheetsClient) SendMessage(msg *SheetsEvent) error {

	jmsg, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
