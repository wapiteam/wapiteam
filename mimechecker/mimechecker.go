package mimechecker

import (
	"archive/zip"
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/h2non/filetype"
	"gitlab.com/wapiteam/wapiteam/wapi"
	"gitlab.com/wapiteam/wapiteam/wapihelpers"
	"gitlab.com/wapiteam/wapiteam/wapilog"
	"golang.org/x/net/html/charset"
)

var log wapilog.Logger

func init() {
	log = wapilog.NewWapiLogger("INFO", "")
}

func GetExtensionFromMime(mime string) string {
	extensions := map[string]string{
		"image/jpeg":             "jpg",
		"image/png":              "png",
		"audio/aac":              "aac",
		"audio/mp4":              "mp4",
		"video/quicktime":        "mov",
		"audio/amr":              "amr",
		"audio/mpeg":             "mpeg",
		"audio/ogg":              "ogg",
		"audio/ogg; codecs=opus": "ogg",
		"application/json":       "json",
		"application/pdf":        "pdf",
		"application/zip":        "zip",
		"application/gzip":       "gz",
		"application/xml":        "xml",
		"audio/midi":             "mid",
		"text/plain":             "txt",
		"text/css":               "css",
		"text/html":              "html",
		"text/php":               "php",
		"text/xml":               "xml",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document":   "docx",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation": "pptx",
		"application/vnd.ms-powerpoint":                                             "ppt",
		"application/vnd.ms-excel.sheet.macroEnabled.12":                            "xlsm",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":         "xlsx",
		"application/vnd.ms-excel":                                                  "xls",
		"application/vnd.oasis.opendocument.graphics":                               "odg",
		"application/vnd.oasis.opendocument.presentation":                           "odp",
		"application/vnd.oasis.opendocument.spreadsheet":                            "ods",
		"application/vnd.oasis.opendocument.text":                                   "odt",
		"video/mp4":                         "mp4",
		"video/3gpp":                        "3gp",
		"application/msword":                "doc",
		"application/octet-stream":          "bin",
		"application/ogg":                   "ogg",
		"application/postscript":            "ps",
		"application/x-7z-compressed":       "7z",
		"application/x-gzip":                "gz",
		"application/x-rar-compressed":      "rar",
		"application/x-tar":                 "tar",
		"application/x-www-form-urlencoded": "php",
		"audio/flac":                        "flac",
		"audio/wav":                         "wav",
		"audio/webm":                        "webm",
		"font/woff":                         "woff",
		"font/woff2":                        "woff2",
		"image/bmp":                         "bmp",
		"image/heic":                        "heic",
		"image/heif":                        "heif",
		"image/svg+xml":                     "svg",
		"image/tiff":                        "tiff",
		"image/webp":                        "webp",
		"message/rfc822":                    "eml",
		"model/gltf+json":                   "gltf",
		"model/vnd.collada+xml":             "dae",
		"model/x3d+xml":                     "x3d",
		"multipart/form-data":               "php",
		"text/calendar":                     "ics",
		"text/csv":                          "csv",
		"text/markdown":                     "md",
		"text/vcard":                        "vcf",
		"video/avi":                         "avi",
		"video/mpeg":                        "mpeg",
		"video/ogg":                         "ogv",
		"video/webm":                        "webm",
		"video/x-flv":                       "flv",
		"video/x-m4v":                       "m4v",
		"video/x-matroska":                  "mkv",
		"video/x-msvideo":                   "avi",
		"video/x-ms-wmv":                    "wmv",
		"video/x-ms-asf":                    "asf",
		"application/vnd.android.package-archive": "apk",
		"application/java-archive":                "jar",
		"application/x-apple-diskimage":           "dmg",
		"application/x-bzip2":                     "bz2",
		"application/x-java-jnlp-file":            "jnlp",
		"application/x-pkcs12":                    "p12",
		"application/x-pkcs7-certificates":        "p7b",
		"application/x-pkcs7-certreqresp":         "p7r",
	}

	for k, v := range extensions {
		if strings.EqualFold(k, mime) {
			return v
		}
	}

	return ""
}

func GetMimeFromExtension(extension string) string {
	mimeTypes := map[string]string{
		"jpeg":  "image/jpeg",
		"jpg":   "image/jpeg",
		"png":   "image/png",
		"aac":   "audio/aac",
		"mov":   "video/quicktime",
		"amr":   "audio/amr",
		"ogg":   "audio/ogg;codecs=opus",
		"json":  "application/json",
		"pdf":   "application/pdf",
		"zip":   "application/zip",
		"gz":    "application/gzip",
		"mid":   "audio/midi",
		"txt":   "text/plain",
		"css":   "text/css",
		"html":  "text/html",
		"php":   "text/php",
		"xml":   "text/xml",
		"docx":  "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"pptx":  "application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"ppt":   "application/vnd.ms-powerpoint",
		"xlsm":  "application/vnd.ms-excel.sheet.macroEnabled.12",
		"xlsx":  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		"xls":   "application/vnd.ms-excel",
		"odg":   "application/vnd.oasis.opendocument.graphics",
		"odp":   "application/vnd.oasis.opendocument.presentation",
		"ods":   "application/vnd.oasis.opendocument.spreadsheet",
		"odt":   "application/vnd.oasis.opendocument.text",
		"mp4":   "video/mp4",
		"3gp":   "video/3gpp",
		"doc":   "application/msword",
		"bin":   "application/octet-stream",
		"ps":    "application/postscript",
		"7z":    "application/x-7z-compressed",
		"rar":   "application/x-rar-compressed",
		"tar":   "application/x-tar",
		"flac":  "audio/flac",
		"wav":   "audio/wav",
		"woff":  "font/woff",
		"woff2": "font/woff2",
		"bmp":   "image/bmp",
		"heic":  "image/heic",
		"heif":  "image/heif",
		"svg":   "image/svg+xml",
		"tiff":  "image/tiff",
		"webp":  "image/webp",
		"eml":   "message/rfc822",
		"gltf":  "model/gltf+json",
		"dae":   "model/vnd.collada+xml",
		"x3d":   "model/x3d+xml",
		"ics":   "text/calendar",
		"csv":   "text/csv",
		"md":    "text/markdown",
		"vcf":   "text/vcard",
		"avi":   "video/avi",
		"mpeg":  "video/mpeg",
		"ogv":   "video/ogg",
		"webm":  "video/webm",
		"flv":   "video/x-flv",
		"m4v":   "video/x-m4v",
		"mkv":   "video/x-matroska",
		"wmv":   "video/x-ms-wmv",
		"asf":   "video/x-ms-asf",
		"apk":   "application/vnd.android.package-archive",
		"jar":   "application/java-archive",
		"dmg":   "application/x-apple-diskimage",
		"bz2":   "application/x-bzip2",
		"jnlp":  "application/x-java-jnlp-file",
		"p12":   "application/x-pkcs12",
		"p7b":   "application/x-pkcs7-certificates",
		"p7r":   "application/x-pkcs7-certreqresp",
	}

	ext := strings.ToLower(extension)
	mime, found := mimeTypes[ext]
	if found {
		return mime
	}

	return "not_supported"
}

func GetFileExtensionFromURL2(url string) (string, error) {
	extension := filepath.Ext(url)
	if extension == "" {
		return "", fmt.Errorf("file extension not found")
	}
	return extension[1:], nil
}

func RetryDownload(url string) ([]byte, error) {
	var err error
	url = wapihelpers.YaDiskSupport(url)
	var decoded []byte

	retries := []time.Duration{
		time.Second * 5,
		time.Second * 10,
		time.Second * 20,
	}

	for _, delay := range retries {
		decoded, err = getFileBytesFromURL(url)
		if err == nil {
			return decoded, nil
		}
		log.Errorf("download error, retry after: %v", delay)
		time.Sleep(delay)
	}

	return getFileBytesFromURL(url)
}

func GetFileNameFromURL(urlStr string) (string, error) {
	parsedURL, err := url.Parse(urlStr)
	if err != nil {
		return "", err
	}

	decodedPath, err := url.PathUnescape(parsedURL.Path)
	if err != nil {
		return "", err
	}
	return path.Base(decodedPath), nil
}

func RetryDownloadWithTimeOUT(url string, timeOUTs []time.Duration) ([]byte, error) {
	var err error
	url = wapihelpers.YaDiskSupport(url)

	var decoded []byte

	for _, delay := range timeOUTs {
		decoded, err = getFileBytesFromURL(url)
		if err == nil {
			return decoded, nil
		}
		log.Errorf("download error, retry after: %v", delay)
		time.Sleep(delay)
	}

	return getFileBytesFromURL(url)
}

func RetryDownloadWithTimeOUTWithProxy(url string, timeOUTs []time.Duration) ([]byte, *http.Response, error) {
	var err error
	url = wapihelpers.YaDiskSupport(url)

	var decoded []byte
	var r *http.Response

	for _, delay := range timeOUTs {
		decoded, r, err = getFileBytesFromURLWithProxy(url)
		if err == nil {
			return decoded, r, nil
		}
		log.Errorf("download error, retry after: %v", delay)
		time.Sleep(delay)
	}

	return getFileBytesFromURLWithProxy(url)
}

func getFileBytesFromURL(url string) ([]byte, error) {

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	userAgent := "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("DNT", "1")

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	bytes, err := io.ReadAll(response.Body)
	if err != nil {
		log.Errorf("reading file error: %s, link: %s", err.Error(), url)
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		log.Errorf("download error, response code: %d, link: %s", response.StatusCode, url)
		return nil, fmt.Errorf("download error, response code: %d, link: %s", response.StatusCode, url)
	} else {
		log.Infof("download success, response code: %s, len: %d", response.StatusCode, len(bytes))
	}

	return bytes, nil
}

func getFileBytesFromURLWithProxy(fileURL string) ([]byte, *http.Response, error) {

	proxyURL := "http://wapi:qweqweasdasdzxczxc123123@212.109.223.105:3128"

	proxyFunc := func(proxyURL string) (*http.Client, error) {
		proxy, err := url.Parse(proxyURL)
		if err != nil {
			return nil, fmt.Errorf("invalid proxy URL: %v", err)
		}
		transport := &http.Transport{
			Proxy: http.ProxyURL(proxy),
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
		return &http.Client{Transport: transport}, nil
	}

	defaultClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	client := defaultClient

	proxyClient, err := proxyFunc(proxyURL)
	if err != nil {
		log.Errorf("Failed to create proxy client: %v", err)
	} else {
		client = proxyClient
	}

	userAgent := "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
	headers := map[string]string{
		"User-Agent": userAgent,
	}

	makeRequest := func(client *http.Client, url string) ([]byte, *http.Response, error) {
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return nil, nil, err
		}
		for key, value := range headers {
			req.Header.Set(key, value)
		}

		response, err := client.Do(req)
		if err != nil {
			return nil, nil, err
		}
		defer response.Body.Close()

		if response.StatusCode != http.StatusOK {
			return nil, response, fmt.Errorf("download error, response code: %d, link: %s", response.StatusCode, fileURL)
		}

		bytes, err := io.ReadAll(response.Body)
		if err != nil {
			return nil, nil, fmt.Errorf("reading file error: %v", err)
		}

		return bytes, response, nil
	}

	data, r, err := makeRequest(client, fileURL)
	if err != nil {
		log.Errorf("Failed to download using proxy: %v. Retrying without proxy...", err)
		data, r, err = makeRequest(defaultClient, fileURL)
		if err != nil {
			return nil, r, fmt.Errorf("failed to download file without proxy: %v", err)
		}
	}

	return data, r, nil
}

func getMimeTypeFromXlsx(data []byte) string {
	readerAt := bytes.NewReader(data)
	zipReader, err := zip.NewReader(readerAt, int64(len(data)))
	if err != nil {
		log.Errorf("Error:", err)
		return ""
	}
	for _, f := range zipReader.File {
		if f.Name == "[Content_Types].xml" {
			fileReader, err := f.Open()
			if err != nil {
				log.Errorf("Error:", err)
				return ""
			}
			defer fileReader.Close()

			contentTypes, err := io.ReadAll(fileReader)
			if err != nil {
				log.Errorf("Error:", err)
				return ""
			}

			if strings.Contains(string(contentTypes), "spreadsheetml.sheet.main+xml") {
				return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
			}
			break
		}
	}
	return "application/zip"
}

func GetMimeTypeFromByte(decoded []byte) (string, error) {
	var mime string
	kind, err := filetype.Match(decoded)
	if err != nil {
		log.Errorf("getMimeTypeFromByte 547", err.Error())
		return "", fmt.Errorf("could not determine file type")
	}

	if kind.MIME.Value == "application/zip" {
		mime = getMimeTypeFromXlsx(decoded)
	} else {
		mime = kind.MIME.Value
	}

	if kind.MIME.Value != "application/octet-stream" {
		return mime, nil
	}

	return "", fmt.Errorf("could not determine file type")
}

func ValidatekMediaMime(mime string) string {

	supported_image := []string{
		"image/jpeg",
		"image/png",
	}

	supported_audio := []string{
		"audio/aac",
		"audio/mp4",
		"audio/amr",
		"audio/mpeg",
		"audio/ogg",
	}

	supported_document := []string{"application/json",
		"application/pdf",
		"application/zip",
		"application/gzip",
		"application/xml",
		"application/msword",
		"text/plain",
		"text/css",
		"text/html",
		"text/php",
		"text/xml",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"application/vnd.ms-powerpoint",
		"application/vnd.ms-excel.sheet.macroEnabled.12",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		"application/vnd.ms-excel",
		"application/vnd.oasis.opendocument.graphics",
		"application/vnd.oasis.opendocument.presentation",
		"application/vnd.oasis.opendocument.spreadsheet",
		"application/vnd.oasis.opendocument.text",
		"image/webp",
		"application/vnd.rar",
		"application/rar",
	}

	supported_video := []string{"video/mp4",
		"video/3gpp",
		"video/quicktime",
	}

	if wapi.ArrContains(supported_image, mime) {
		return "image"
	} else if wapi.ArrContains(supported_audio, mime) {
		return "audio"
	} else if wapi.ArrContains(supported_document, mime) {
		return "document"
	} else if wapi.ArrContains(supported_video, mime) {
		return "video"
	} else {
		return "not_supported"
	}

}

func GetMimeTypeFromB64(b64File string) (string, []byte, error) {
	decoded, err := base64.StdEncoding.DecodeString(b64File)
	if err != nil {
		return "", []byte{}, err
	}

	var mime string

	kind, err := filetype.Match(decoded)
	if err != nil {
		log.Errorf(err.Error())
	}

	if kind.MIME.Value == "application/zip" {
		mime = getMimeTypeFromXlsx(decoded)
	} else {
		mime = kind.MIME.Value
	}

	if kind.MIME.Value != "application/octet-stream" {
		return mime, decoded, nil
	}

	return "", []byte{}, fmt.Errorf("could not determine file type")
}

func GetFileExtensionFromURL(fileURL string) (string, error) {
	parsed, err := url.Parse(fileURL)
	if err != nil {
		return "", fmt.Errorf("can't parse url: %v", err)
	}

	path := parsed.Path
	extension := filepath.Ext(path)
	if extension == "" {
		return "", fmt.Errorf("file extension not found")
	}

	return extension[1:], nil
}

func GetMetaInfoFromURL(urlStr string) (*wapi.MetaInformationFromSite, error) {
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Timeout: 4350 * time.Millisecond,
	}
	userAgent := "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

	req, err := http.NewRequest("GET", urlStr, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", userAgent)

	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	// Чтение тела ответа
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	// Определяем кодировку и преобразуем текст в UTF-8, если это необходимо
	utf8Body, err := charset.NewReader(bytes.NewReader(body), response.Header.Get("Content-Type"))
	if err != nil {
		return nil, err
	}

	// Читаем преобразованный текст в UTF-8 формате
	utf8Doc, err := io.ReadAll(utf8Body)
	if err != nil {
		return nil, err
	}

	// Парсим HTML документ
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(utf8Doc))
	if err != nil {
		return nil, err
	}

	// Инициализируем мета-информацию
	metaInfo := &wapi.MetaInformationFromSite{}
	metaInfo.Title = doc.Find("title").Text()

	if title, exists := doc.Find(`meta[property="og:title"]`).Attr("content"); exists {
		metaInfo.Title = title
	}

	if description, exists := doc.Find(`meta[property="og:description"]`).Attr("content"); exists {
		metaInfo.Description = description
	}

	if imageURL, exists := doc.Find(`meta[property="og:image"]`).Attr("content"); exists {
		metaInfo.Image = &imageURL
	} else if imageURL, exists := doc.Find(`meta[property="vk:image"]`).Attr("content"); exists {
		metaInfo.Image = &imageURL
	}

	return metaInfo, nil
}
