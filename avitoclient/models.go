package avitoclient

import "time"

type Booking struct {
	AvitoBookingID int64  `json:"avito_booking_id"`
	BasePrice      int    `json:"base_price"`
	CheckIn        string `json:"check_in"`
	CheckOut       string `json:"check_out"`
	Contact        struct {
		Email string `json:"email"`
		Name  string `json:"name"`
		Phone string `json:"phone"`
	} `json:"contact"`
	GuestCount  int `json:"guest_count"`
	Nights      int `json:"nights"`
	SafeDeposit struct {
		OwnerAmount int `json:"owner_amount"`
		Tax         int `json:"tax"`
		TotalAmount int `json:"total_amount"`
	} `json:"safe_deposit"`
	Status     string `json:"status"`
	BookingUrl string `json:"booking_url,omitempty"`
}

type Bookings struct {
	Bookings []Booking `json:"bookings"`
}

type Coordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Salary struct {
	From int `json:"from"`
	To   int `json:"to"`
}

type Params struct {
	Address                string      `json:"address"`
	AgePreferences         []string    `json:"age_preferences"`
	Bonuses                []string    `json:"bonuses"`
	BusinessArea           string      `json:"business_area"`
	Change                 []string    `json:"change"`
	ConstructionWorkType   []string    `json:"construction_work_type"`
	Coordinates            Coordinates `json:"coordinates"`
	Cuisine                []string    `json:"cuisine"`
	DeliveryMethod         []string    `json:"delivery_method"`
	DrivingExperience      string      `json:"driving_experience"`
	DrivingLicenseCategory []string    `json:"driving_license_category"`
	EateryType             []string    `json:"eatery_type"`
	Experience             string      `json:"experience"`
	FacilityType           []string    `json:"facility_type"`
	FoodProductionShopType []string    `json:"food_production_shop_type"`
	IndustryType           []string    `json:"industry_type"`
	IsCompanyCar           string      `json:"is_company_car"`
	ManufacturingType      []string    `json:"manufacturing_type"`
	MedicalBook            string      `json:"medical_book"`
	PaidPeriod             string      `json:"paid_period"`
	PayoutFrequency        string      `json:"payout_frequency"`
	PieceworkFlag          string      `json:"piecework_flag"`
	Profession             string      `json:"profession"`
	Programs               []string    `json:"programs"`
	RegistrationMethod     []string    `json:"registration_method"`
	RetailEquipmentType    []string    `json:"retail_equipment_type"`
	RetailShopType         []string    `json:"retail_shop_type"`
	Salary                 Salary      `json:"salary"`
	Schedule               string      `json:"schedule"`
	ToolsAvailability      string      `json:"tools_availability"`
	WarehouseFunctionality []string    `json:"warehouse_functionality"`
	WhereToWork            string      `json:"where_to_work"`
	WorkerClass            []string    `json:"worker_class"`
}

type Vacancy struct {
	Description string    `json:"description"`
	ID          int       `json:"id"`
	IsActive    bool      `json:"is_active"`
	Params      Params    `json:"params"`
	Salary      int       `json:"salary"`
	StartTime   time.Time `json:"start_time"`
	Title       string    `json:"title"`
	UpdateTime  time.Time `json:"update_time"`
	URL         string    `json:"url"`
}
