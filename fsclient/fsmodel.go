package fsclient

import (
	"fmt"
	"os"

	"gitlab.com/wapiteam/wapiteam/wapilog"
)

type FileAssembled struct {
	FullPath  string `json:"fullPath,omitempty"`
	Name      string `json:"name,omitempty"`
	Extension string `json:"extension,omitempty"`
	Path      string `json:"path,omitempty"`
	Data      []byte `json:"data,omitempty"`
	// /MimeType  string `json:"mime_type,omitempty"`
	Status      string `json:"status,omitempty"`
	URL         string `json:"url,omitempty"`
	LocalPrefix string `json:"localPrefix,omitempty"`
}

type FileResponse struct {
	URL    string `json:"url,omitempty"`
	Status string `json:"status,omitempty"`
}

type FileRequest struct {
	Data    []byte `json:"data,omitempty"`
	BaseUrl string `json:"baseUrl,omitempty"`
}

func (file *FileAssembled) Save(l wapilog.Logger) error {
	err := os.MkdirAll(file.LocalPrefix+file.Path, os.ModePerm)
	if err != nil {
		return fmt.Errorf("error creating directory: %w", err)
	}
	l.Infof("directories %v are ok", file.Path)

	err = os.WriteFile(file.LocalPrefix+file.FullPath, file.Data, 0644)
	if err != nil {
		return fmt.Errorf("error writing file: %w", err)
	}
	l.Resultf("file has been saved")
	return nil
}

func (file *FileAssembled) Delete(l wapilog.Logger) error {
	l.Infof("attempting to delete file...")
	err := os.Remove(file.LocalPrefix + file.FullPath)
	if err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("file not found: %v", err)
		}
		return fmt.Errorf("error deleting file: %v", err)
	}
	l.Resultf("file deleted successfully")
	return nil
}

func (file *FileAssembled) DeleteDir(l wapilog.Logger) error {
	l.Infof("attempting to delete directory...")
	err := os.RemoveAll(file.LocalPrefix + file.Path)
	if err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("directory not found: %v", err)
		}
		return fmt.Errorf("error deleting directory: %v", err)
	}
	l.Resultf("directory deleted successfully")
	return nil
}

func (file *FileAssembled) Read(l wapilog.Logger) error {
	l.Infof("attempting to read file...")
	data, err := os.ReadFile(file.LocalPrefix + file.FullPath)
	if err != nil {
		if os.IsNotExist(err) {
			l.Warnf("file does not exist")
			return fmt.Errorf("file not found: %w", err)
		}
		return fmt.Errorf("error reading file: %w", err)
	}
	file.Data = data
	return nil
}

func (file *FileAssembled) Check(l wapilog.Logger) error {
	l.Infof("attempting to check file...")
	_, err := os.ReadFile(file.LocalPrefix + file.FullPath)
	if err != nil {
		if os.IsNotExist(err) {
			l.Warnf("file does not exist")
			return fmt.Errorf("file not found: %w", err)
		}
		return fmt.Errorf("error reading file: %w", err)
	}
	return nil
}
