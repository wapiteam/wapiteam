package fsclient

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type FSClient struct {
	url   string
	token string
	mode  string
}

var fsclient *FSClient = nil

func GetClient(url, token string, mode string) *FSClient {
	if fsclient == nil {
		fsclient = &FSClient{
			url:   url,
			token: token,
			mode:  mode,
		}
	}
	return fsclient
}

func (fsc *FSClient) SetUrl(url string) {
	if fsc.mode == "dev" {
		fsc.url = url
	} else {
		fsc.url = "https://fs.wappi.pro"
	}
}

func (fsc *FSClient) SetToken(token string) {
	fsc.token = token
}

func (fsc *FSClient) UploadFile(fullPath string, data []byte) (*FileResponse, error) {
	url := fsc.url + "/fs/uploadFile" + fullPath
	req := FileRequest{
		Data:    data,
		BaseUrl: fsc.url,
	}
	payload, _ := json.Marshal(req)
	resp, status, err := sendRequest(url, payload, "POST", fsc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error uploading file: %v, status: %v, error: %v", fullPath, status, err)
	}
	var answer FileResponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v", err)
	}
	if fsc.mode == "dev" {
		answer.URL = fsc.url + "/fs/downloadFile" + fullPath
	} else {
		answer.URL = "https://fs.wappi.pro" + "/fs/downloadFile" + fullPath
	}
	return &answer, nil
}

func (fsc *FSClient) DownloadFile(fullPath string) ([]byte, error) {
	url := fsc.url + "/fs/downloadFile" + fullPath
	req := FileRequest{
		BaseUrl: fsc.url,
	}
	payload, _ := json.Marshal(req)
	resp, status, err := sendRequest(url, payload, "GET", fsc.token)
	if err != nil {
		return nil, fmt.Errorf("error downloading file: %v, status: %v, error: %v", fullPath, status, err)
	}
	return resp, err
}

func (fsc *FSClient) CheckFile(fullPath string) (*FileAssembled, error) {
	url := fsc.url + "/fs/checkFile" + fullPath
	req := FileRequest{
		BaseUrl: fsc.url,
	}
	payload, _ := json.Marshal(req)
	resp, status, err := sendRequest(url, payload, "GET", fsc.token)
	if err != nil {
		return nil, fmt.Errorf("error downloading file: %v, status: %v, error: %v", fullPath, status, err)
	}
	var answer FileAssembled
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v", err)
	}
	if fsc.mode == "dev" {
		answer.URL = fsc.url + "/fs/downloadFile" + fullPath
	} else {
		answer.URL = "https://fs.wappi.pro" + "/fs/downloadFile" + fullPath
	}
	return &answer, nil
}

func (fsc *FSClient) DeleteFile(fullPath string) (*FileResponse, error) {
	url := fsc.url + "/fs/deleteFile" + fullPath
	req := FileRequest{
		BaseUrl: fsc.url,
	}
	payload, _ := json.Marshal(req)
	resp, status, err := sendRequest(url, payload, "DELETE", fsc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error delete file: %v, status: %v, error: %v", fullPath, status, err)
	}
	var answer FileResponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v", err)
	}

	return &answer, nil
}

func (fsc *FSClient) DeleteDir(fullPath string) (*FileResponse, error) {
	url := fsc.url + "/fs/deleteDir" + fullPath
	req := FileRequest{
		BaseUrl: fsc.url,
	}
	payload, _ := json.Marshal(req)
	resp, status, err := sendRequest(url, payload, "DELETE", fsc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error delete file: %v, status: %v, error: %v", fullPath, status, err)
	}
	var answer FileResponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v", err)
	}

	return &answer, nil
}

func sendRequest(url string, payload []byte, reqType, token string) ([]byte, int, error) {

	req, err := http.NewRequest(reqType, url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, 0, fmt.Errorf("error creating request:%v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	client := &http.Client{
		Timeout: 5 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, fmt.Errorf("error sending request:%v", err)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, fmt.Errorf("error reading body:%v", err)
	}

	defer resp.Body.Close()

	return body, resp.StatusCode, nil
}
