package bitrixclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type BitrixClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewBitrixClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *BitrixClient {
	return &BitrixClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *BitrixClient) SendMessage(bitrix_msg *OutBitrixMessage) error {

	jmsg, err := json.Marshal(bitrix_msg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
