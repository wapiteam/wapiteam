package bitrixclient

import (
	"gitlab.com/wapiteam/wapiteam/avitoclient"
	"gitlab.com/wapiteam/wapiteam/utmGRPC"
)

type OutBitrixMessage struct {
	User          BitrixUser           `json:"user,omitempty"`
	Message       BitrixMessage        `json:"message,omitempty"`
	Chat          BitrixChat           `json:"chat,omitempty"`
	AvitoApply    *AvitoApply          `json:"avito_apply,omitempty"`
	AvitoVacancy  *avitoclient.Vacancy `json:"avito_vacancy,omitempty"`
	AvitoOrder    *AvitoOrder          `json:"avito_order,omitempty"`
	AvitoBooking  *avitoclient.Booking `json:"avito_booking,omitempty"`
	StatusMessage *StatusMessage       `json:"status_message,omitempty"`
	Event         string               `json:"event,omitempty"`
	ReplyMessage  BitrixMessage        `json:"reply_message,omitempty"`
	ProfileUUID   string               `json:"profile_uuid,omitempty"`
	FromWhere     string               `json:"from_where,omitempty"`
	ChatType      string               `json:"chat_type,omitempty"`
	WapiBotID     string               `json:"wapi_bot_id,omitempty"`
	Phone         string               `json:"phone,omitempty"`
	UTM           *utmGRPC.UTM         `json:"utm,omitempty"`
}

type AvitoOrder struct {
	ID               string       `json:"id"`
	CreatedAt        string       `json:"createdAt"`
	UpdatedAt        string       `json:"updatedAt"`
	Status           string       `json:"status"`
	Delivery         Delivery     `json:"delivery"`
	BuyerInfo        BuyerInfo    `json:"buyerInfo"`
	Items            []OrderItem  `json:"items"`
	MarketplaceID    string       `json:"marketplaceId"`
	Prices           Prices       `json:"prices"`
	ReturnPolicy     ReturnPolicy `json:"returnPolicy"`
	Schedules        Schedules    `json:"schedules"`
	AvailableActions []Action     `json:"availableActions"`
}

type Delivery struct {
	ServiceName    string        `json:"serviceName"`
	ServiceType    string        `json:"serviceType"`
	TrackingNumber string        `json:"trackingNumber"`
	CourierInfo    *CourierInfo  `json:"courierInfo"`
	TerminalInfo   *TerminalInfo `json:"terminalInfo"`
}

type CourierInfo struct {
	Address string `json:"address"`
	Comment string `json:"comment"`
}

type TerminalInfo struct {
	Address string `json:"address"`
	Code    string `json:"code"`
}

type BuyerInfo struct {
	FullName    string `json:"fullName"`
	PhoneNumber string `json:"phoneNumber"`
}

type OrderItem struct {
	AvitoID  string `json:"avitoId"`
	ChatID   string `json:"chatId"`
	Count    int    `json:"count"`
	Prices   Prices `json:"prices"`
	Title    string `json:"title"`
	Location string `json:"location"`
	Url      string `json:"url"`
}

type Prices struct {
	Commission float64 `json:"commission"`
	Delivery   float64 `json:"delivery"`
	Discount   float64 `json:"discount"`
	Price      float64 `json:"price"`
	Total      float64 `json:"total"`
}

type ReturnPolicy struct {
	ReturnStatus   string `json:"returnStatus"`
	TrackingNumber string `json:"trackingNumber"`
}

type Schedules struct {
	ConfirmTill           string `json:"confirmTill"`
	DeliveryDate          string `json:"deliveryDate"`
	DeliveryDateMax       string `json:"deliveryDateMax"`
	DeliveryDateMin       string `json:"deliveryDateMin"`
	SetTermsTill          string `json:"setTermsTill"`
	SetTrackingNumberTill string `json:"setTrackingNumberTill"`
	ShipTill              string `json:"shipTill"`
}

type Action struct {
	Name     string `json:"name"`
	Required bool   `json:"required"`
}

type StatusMessage struct {
	Status    string `json:"status,omitempty"`
	MessageID string `json:"message_id,omitempty"`
	ChatID    string `json:"chat_id,omitempty"`
}

type BitrixUser struct {
	ID                string        `json:"id,omitempty"`
	LastName          string        `json:"last_name,omitempty"`
	Name              string        `json:"name,omitempty"`
	PictureURL        BitrixPicture `json:"picture,omitempty"`
	URL               string        `json:"url,omitempty"`
	Sex               string        `json:"sex,omitempty"`
	Email             string        `json:"email,omitempty"`
	Phone             string        `json:"phone,omitempty"`
	SkipPhoneValidate string        `json:"skip_phone_validate,omitempty"`
}

type BitrixPicture struct {
	Url string `json:"url,omitempty"`
}

type BitrixFileUrl struct {
	URL string `json:"url,omitempty"`
}

type BitrixMessage struct {
	ID         string           `json:"id,omitempty"`
	Date       int64            `json:"date,omitempty"`
	DisableCRM string           `json:"disable_crm,omitempty"`
	Text       string           `json:"text,omitempty"`
	Files      []*BitrixFileUrl `json:"files,omitempty"`
	IsFromMe   bool             `json:"is_from_me,omitempty"`
	IsSystem   bool             `json:"is_system,omitempty"`
}

type BitrixChat struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Url  string `json:"url,omitempty"`
}

type BitrixFile struct {
	FileName string
	File     []byte
}

type AvitoApply struct {
	ID            string `json:"id" bson:"id"`
	NegotiationID int    `json:"negotiation_id" bson:"negotiation_id"`
	Type          string `json:"type" bson:"type"`
	CreatedAt     string `json:"created_at" bson:"created_at"`
	UpdatedAt     string `json:"updated_at" bson:"updated_at"`
	ChatID        string `json:"chat_id" bson:"chat_id"`
	Applicant     struct {
		ID   string `json:"id" bson:"id"`
		Data struct {
			Name string `json:"name" bson:"name"`
		} `json:"data" bson:"data"`
		ResumeID string `json:"resume_id" bson:"resume_id"`
	} `json:"applicant" bson:"applicant"`
	Contacts struct {
		Chat struct {
			Value string `json:"value" bson:"value"`
		} `json:"chat" bson:"chat"`
		Phones []struct {
			Value  string      `json:"value" bson:"value"`
			Status interface{} `json:"status" bson:"status"`
		} `json:"phones" bson:"phones"`
	} `json:"contacts" bson:"contacts"`
	VacancyID          int64 `json:"vacancy_id" bson:"vacancy_id"`
	EmployeeID         int64 `json:"employee_id" bson:"employee_id"`
	EnrichedProperties struct {
		Status      string `json:"status" bson:"status"`
		Citizenship struct {
			Value          string `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"citizenship" bson:"citizenship"`
		Experience struct {
			Value          string `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"experience" bson:"experience"`
		Phone struct {
			Value          string `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"phone" bson:"phone"`
		Gender struct {
			Value          string `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"gender" bson:"gender"`
		Age struct {
			Value          int    `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"age" bson:"age"`
		FullName struct {
			Value          string `json:"value" bson:"value"`
			MatchingStatus string `json:"matching_status" bson:"matching_status"`
		} `json:"full_name" bson:"full_name"`
	} `json:"enriched_properties" bson:"enriched_properties"`

	Resume Resume `json:"resume" bson:"resume"`
}

type Resume struct {
	ID          int64  `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	URL         string `json:"url"`
	IsActive    bool   `json:"is_active"`
	IsPurchased bool   `json:"is_purchased"`
	Salary      int    `json:"salary"`
	StartTime   string `json:"start_time"`
	UpdateTime  string `json:"update_time"`
	Params      Params `json:"params"`
}

type Params struct {
	AbilityToBusinessTrip string       `json:"ability_to_business_trip"`
	Address               string       `json:"address"`
	Age                   int          `json:"age"`
	BusinessArea          string       `json:"business_area"`
	Education             string       `json:"education"`
	Experience            int          `json:"experience"`
	ExperienceList        []Experience `json:"experience_list"`
	LanguageList          []Language   `json:"language_list"`
	Moving                string       `json:"moving"`
	Nationality           string       `json:"nationality"`
	Pol                   string       `json:"pol"`
	Schedule              string       `json:"schedule"`
}

type Experience struct {
	Company          string `json:"company"`
	Position         string `json:"position"`
	Responsibilities string `json:"responsibilities"`
	WorkStart        string `json:"work_start"`
}

type Language struct {
	Language      string `json:"language"`
	LanguageLevel string `json:"language_level"`
}
