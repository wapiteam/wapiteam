package botclient

import "time"

type WapiEventMessage struct {
	Client  *Client       `json:"client,omitempty"`
	Message *MessageToBot `json:"message,omitempty"`
}

type MessageToBot struct {
	Type       string `json:"type,omitempty"`
	Body       string `json:"body,omitempty"`
	File       []byte `json:"file,omitempty"`
	IsFromMe   bool   `json:"is_from_me,omitempty"`
	MessasgeID string `json:"message_id"`
}

type WapiEvent struct {
	Type             string            `json:"type,omitempty"`
	UUID             string            `json:"uuid,omitempty"`
	WapiEventMessage *WapiEventMessage `json:"wapi_event_message,omitempty"`
	Platform         string            `json:"platform,omitempty"`
	User             uint32            `json:"user,omitempty"`
}

type Client struct {
	Phone         string    `json:"phone,omitempty" bson:"phone"`
	Name          string    `json:"name,omitempty" bson:"name"`
	LastExecution time.Time `json:"last_execution,omitempty" bson:"last_execution"`
	Chat          string    `json:"chat,omitempty" bson:"chat"`
}
