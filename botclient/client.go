package botclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type BotClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewBotClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *BotClient {
	return &BotClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *BotClient) SendEvent(bitrix_msg *WapiEvent) error {

	jmsg, err := json.Marshal(bitrix_msg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
