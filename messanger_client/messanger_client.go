package messanger_client

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapi"
	"gitlab.com/wapiteam/wapiteam/wapilog"
	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

var log wapilog.Logger

type MessangerClient struct {
	exchangeName string
	queueName    string
	broker       *wapirabbit.AmqpBroker
}

func init() {
	log = wapilog.NewWapiLogger("INFO", "")
}

type Service struct {
	ProfileId string                        `json:"profile_id"`
	Type      string                        `json:"type,omitempty"`
	MessageID string                        `json:"message_id,omitempty"`
	Message   *wapi.IncomingOutgoingMessage `json:"message,omitempty"`
}

type SendMessage struct {
	Event    string                        `json:"event"`
	Message  *wapi.IncomingOutgoingMessage `json:"message,omitempty"`
	Status   *wapi.AckWH                   `json:"status,omitempty"`
	Call     *wapi.CallHookWH              `json:"call,omitempty"`
	Service  *Service                      `json:"service,omitempty"`
	UserID   int                           `json:"user_id"`
	Platform string                        `json:"platform"`
}

func NewMessangerClient(broker *wapirabbit.AmqpBroker, exchangeName string, queueName string) *MessangerClient {
	return &MessangerClient{
		exchangeName: exchangeName,
		queueName:    queueName,
		broker:       broker,
	}
}

// asdasdad
func (hc *MessangerClient) SendMessageEvent(ctx context.Context, allWapi *wapi.Wapi, message *wapi.IncomingOutgoingMessage) error {

	if message == nil {
		log.Errorf("MessangerClient SendMessage message is nil")
		return nil
	}

	msg := SendMessage{
		Event:    "message_event",
		Message:  message,
		UserID:   allWapi.User.ID,
		Platform: allWapi.Profile.Platform,
	}

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		log.Errorf("Ошибка Marshal MessangerClient SendMessageEvent message: %s", err.Error())
		return err
	}

	if err := hc.broker.Publish(ctx, hc.exchangeName, hc.queueName, jsonMsg); err != nil {
		log.Errorf("error MessangerClient SendMessageEvent Publish: %s", err.Error())
		return err
	}

	log.Resultf("MessangerClient SendMessage Publish succes msg_type: %s, profile.uuid: %s, ChatId: %s", message.Type, message.ProfileId, message.ChatId)
	return nil
}

func (hc *MessangerClient) SendStatusMessageEvent(ctx context.Context, allWapi *wapi.Wapi, message *wapi.AckWH) error {

	if message == nil {
		log.Errorf("MessangerClient SendMessage message is nil")
		return nil
	}

	msg := SendMessage{
		Event:    "status_event",
		Status:   message,
		UserID:   allWapi.User.ID,
		Platform: allWapi.Profile.Platform,
	}

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		log.Errorf("Ошибка Marshal MessangerClient SendStatusMessageEvent message: %s", err.Error())
		return err
	}

	if err := hc.broker.Publish(ctx, hc.exchangeName, hc.queueName, jsonMsg); err != nil {
		log.Errorf("error MessangerClient SendStatusMessageEvent Publish: %s", err.Error())
		return err
	}

	log.Resultf("MessangerClient SendStatusMessage Publish succes message.ID: %s, profile.uuid: %s, ChatId: %s, Status: %s", message.ID, message.ProfileId, message.ChatId, message.Status)
	return nil
}

func (hc *MessangerClient) SendCallEvent(ctx context.Context, allWapi *wapi.Wapi, message *wapi.CallHookWH) error {
	if message == nil {
		log.Errorf("MessangerClient SendMessage message is nil")
		return nil
	}

	msg := SendMessage{
		Event:    message.Type,
		Call:     message,
		UserID:   allWapi.User.ID,
		Platform: allWapi.Profile.Platform,
	}

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		log.Errorf("Ошибка Marshal MessangerClient SendCallEvent message: %s", err.Error())
		return err
	}

	if err := hc.broker.Publish(ctx, hc.exchangeName, hc.queueName, jsonMsg); err != nil {
		log.Errorf("error MessangerClient SendCallEvent Publish: %s", err.Error())
		return err
	}

	log.Resultf("MessangerClient SendCallEvent Publish succes profile.uuid: %s, Number: %s, Type: %s", message.ProfileId, message.Number, message.Type)
	return nil
}

func (hc *MessangerClient) SendServiceEvent(ctx context.Context, allWapi *wapi.Wapi, message *Service) error {
	if message == nil {
		log.Errorf("MessangerClient SendMessage message is nil")
		return nil
	}

	msg := SendMessage{
		Event:    "service_event",
		Service:  message,
		UserID:   allWapi.User.ID,
		Platform: allWapi.Profile.Platform,
	}

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		log.Errorf("Ошибка Marshal MessangerClient SendCallEvent message: %s", err.Error())
		return err
	}

	if err := hc.broker.Publish(ctx, hc.exchangeName, hc.queueName, jsonMsg); err != nil {
		log.Errorf("error MessangerClient SendCallEvent Publish: %s", err.Error())
		return err
	}

	log.Resultf("MessangerClient SendServiceEvent Publish succes profile.uuid: %s, MessageID: %s, ServiceType: %s", message.ProfileId, message.MessageID, message.Type)
	return nil
}
