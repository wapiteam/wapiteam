package config

import (
	"github.com/caarlos0/env"
)

type WapiConfig struct {
	PROFILE_ID        int    `env:"WBW_PROFILE_ID"`
	APP_ID            int    `env:"APP_ID" envDefault:"94575"`
	APP_HASH          string `env:"APP_HASH" envDefault:"a3406de8d171bb422bb6ddf3bbd800e2"`
	DB_HOST           string `env:"DB_HOST,required"`
	DB_USER           string `env:"DB_USER,required"`
	DB_PASSWORD       string `env:"DB_PASSWORD,required"`
	DB_NAME           string `env:"DB_NAME,required"`
	DB_PORT           string `env:"DB_PORT,required"`
	MONGO_FROM_DB     string `json:"MONGO_FROM_DB,omitempty"`
	AMQP_USER         string `env:"AMQP_USER"`
	MONGO_URL         string `env:"MONGO_URL"`
	AMQP_PASSWORD     string `env:"AMQP_PASSWORD"`
	AMQP_HOST         string `env:"AMQP_HOST"`
	AMQP_PORT         string `env:"AMQP_PORT"`
	TG_PHONE          string `env:"TG_PHONE"`
	LOG_LEVEL         string `env:"LOG_LEVEL" envDefault:"INFO"`
	AMO_CLIENT_SECRET string `env:"AMO_CLIENT_SECRET"`
	CONVERTER_URL     string `env:"CONVERTER_URL" envDefault:"http://10.10.10.59:8082/convert"`
	AMO_REDIRECT_URI  string `env:"AMO_REDIRECT_URI" envDefault:"https://wappi.pro/amocrm/webhook"`
	AMO_WEBHOOK       string `env:"AMO_WEBHOOK" envDefault:"https://wappi.pro/amocrm/install"`
	BOT_TOKEN         string `env:"BOT_TOKEN"`
	WAPPI_CHANNEL_ID  string `env:"WAPPI_CHANNEL_ID"`
	TASK_HAPROXY      string `env:"TASK_HAPROXY" envDefault:"10.10.10.90:8800"`
}

func NewWapiConfig() (*WapiConfig, error) {
	conf := &WapiConfig{}
	if err := env.Parse(conf); err != nil {
		return nil, err
	}
	return conf, nil
}
