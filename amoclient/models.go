package amoclient

import (
	"gitlab.com/wapiteam/wapiteam/avitoclient"
	"gitlab.com/wapiteam/wapiteam/bitrixclient"
	"gitlab.com/wapiteam/wapiteam/utmGRPC"
	"gitlab.com/wapiteam/wapiteam/wapi"
)

// anal type
type AmoOutput struct {
	ProfileUUID    string                   `json:"profile_uuid,omitempty"`
	AccountID      string                   `json:"account_id,omitempty"`
	Payload        Payload                  `json:"payload,omitempty"`
	EventType      string                   `json:"event_type,omitempty"`
	FromWhere      string                   `json:"from_where,omitempty"`
	ChatType       string                   `json:"chat_type,omitempty"`
	IsFromMe       bool                     `json:"is_from_me,omitempty"`
	Platform       string                   `json:"platform,omitempty"`
	WappiMessageID *string                  `json:"wappi_message_id,omitempty"`
	WappiBotID     string                   `json:"wappi_bot_id,omitempty"`
	TGContact      *wapi.TGContact          `json:"tg_contact,omitempty"`
	AvitoOrder     *bitrixclient.AvitoOrder `json:"avito_order,omitempty"`
	AvitoResume    *bitrixclient.Resume     `json:"avito_resume,omitempty"`
	AvitoApply     *bitrixclient.AvitoApply `json:"avito_apply,omitempty"`
	AvitoVacancy   *avitoclient.Vacancy     `json:"avito_vacancy,omitempty"`
	AvitoBooking   *avitoclient.Booking     `json:"avito_booking,omitempty"`
	UTM            *utmGRPC.UTM             `json:"utm,omitempty"`
	AnalType       *string                  `json:"anal_type,omitempty"`
}

type Payload struct {
	Timestamp         int64           `json:"timestamp"`
	Msgid             string          `json:"msgid"`
	ConversationID    string          `json:"conversation_id"`
	Sender            *SenderReceiver `json:"sender"`
	Receiver          *SenderReceiver `json:"receiver,omitempty"`
	Message           MessageDetail   `json:"message"`
	ReplyTo           *Reply          `json:"reply_to,omitempty"`
	Silent            bool            `json:"silent"`
	Source            Source          `json:"source"`
	ConversationRefID *string         `json:"conversation_ref_id,omitempty"`
}

type SenderReceiver struct {
	ID      string   `json:"id,omitempty"`
	Name    string   `json:"name,omitempty"`
	Profile *Profile `json:"profile,omitempty"`
	Phone   string   `json:"phone,omitempty"`
	RefID   string   `json:"ref_id,omitempty"`
	Avatar  string   `json:"avatar,omitempty"`
}

type Reply struct {
	Message EmbeddedMessageDetail `json:"message"`
}

type Source struct {
	ExternalID string `json:"external_id"`
}

type Profile struct {
	Phone string `json:"phone"`
	Email string `json:"email"`
}

type Conversation struct {
	ID       string `json:"id"`
	ClientID string `json:"client_id"`
}

type Location struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type Contact struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type EmbeddedUser struct {
	ID    *string `json:"id"`
	RefID *string `json:"ref_id"`
	Namne *string `json:"name"`
}

type EmbeddedMessageDetail struct {
	ID            *string       `json:"id"`
	MsgID         *string       `json:"msgid"`
	Type          string        `json:"type"`
	Text          string        `json:"text"`
	Sender        *EmbeddedUser `json:"sender"`
	Media         *string       `json:"media"`
	FileName      string        `json:"file_name"`
	FileSize      int           `json:"file_size"`
	MediaDuration int           `json:"media_duration"`
	Location      Location      `json:"location"`
	Contact       Contact       `json:"contact"`
	Timestamp     *int64        `json:"timestamp"`
	MsecTimestamp *int64        `json:"msec_timestamp"`
}

type MessageDetail struct {
	ID            string   `json:"id"`
	Type          string   `json:"type"`
	Text          string   `json:"text"`
	Markup        *string  `json:"markup"`
	Tag           string   `json:"tag"`
	Media         string   `json:"media"`
	Thumbnail     string   `json:"thumbnail"`
	FileName      string   `json:"file_name"`
	FileSize      int      `json:"file_size"`
	MediaDuration int      `json:"media_duration"`
	Location      Location `json:"location"`
	Contact       Contact  `json:"contact"`
}
