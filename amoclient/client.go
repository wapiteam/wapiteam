package amoclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type AmoClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewAmoClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *AmoClient {
	return &AmoClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *AmoClient) SendMessage(msg *AmoOutput) error {

	jmsg, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
