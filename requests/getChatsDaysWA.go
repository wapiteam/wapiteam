package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetChatsDaysWA(profile wapi.WapiProfile, offset, limit string) ([]wapi.Chat, error) {

	queryParams := map[string]string{
		"profile_id": profile.UUID,
		"offset":     offset,
		"limit":      limit,
	}

	response, err := HTTPSend[wapi.WapiChatResponse](r.Token, r.WappiURL+"/api/sync/chats/days/get", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	return response.Dialogs, nil
}
