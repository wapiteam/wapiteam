package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetChatsDaysTG(profile wapi.WapiProfile, offset, limit string) ([]wapi.CustomTGChat, error) {

	queryParams := map[string]string{
		"profile_id": profile.UUID,
		"offset":     offset,
		"limit":      limit,
	}

	response, err := HTTPSend[wapi.WapiTGChatsResponse](r.Token, r.WappiURL+"/tapi/sync/chats/days/get", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	return response.Chats, nil
}
