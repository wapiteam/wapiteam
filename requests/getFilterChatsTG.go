package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetChatsFilterTG(profile wapi.WapiProfile, filter string) ([]wapi.CustomTGChat, error) {

	queryParams := map[string]string{
		"profile_id":  profile.UUID,
		"client_name": filter,
	}

	response, err := HTTPSend[wapi.WapiTGChatsResponse](r.Token, r.WappiURL+"/tapi/sync/chats/filter", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	// log.Debugf(response.Dialogs)

	return response.Chats, nil
}
