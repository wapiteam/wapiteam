package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetAllChatsTG(profile wapi.WapiProfile) ([]wapi.CustomTGChat, error) {

	queryParams := map[string]string{
		"profile_id": profile.UUID,
		"show_all":   "true",
	}

	response, err := HTTPSend[wapi.WapiTGChatsResponse](r.Token, r.WappiURL+"/tapi/sync/chats/get", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	return response.Chats, nil
}
