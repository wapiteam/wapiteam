package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetAllChatsWA(profile wapi.WapiProfile) ([]wapi.Chat, error) {

	queryParams := map[string]string{
		"profile_id": profile.UUID,
		"show_all":   "true",
	}

	response, err := HTTPSend[wapi.WapiChatResponse](r.Token, r.WappiURL+"/api/sync/chats/get", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	return response.Dialogs, nil
}
