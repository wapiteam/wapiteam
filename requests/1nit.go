package requests

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/wapiteam/wapiteam/wapilog"
)

var log = wapilog.NewWapiLogger("INFO", "")

type WappiRequests struct {
	WappiURL string
	Token    string
}

func NewWappiRequests(token string, wappi_url string) *WappiRequests {
	requests := &WappiRequests{
		Token:    token,
		WappiURL: wappi_url,
	}

	return requests
}

func HTTPSend[T any](token string, url string, method string, body []byte, queryParams map[string]string) (*T, error) {
	// Создание запроса
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		log.Errorf("request creation error: %s", err.Error())
		return nil, err
	}

	// Добавление заголовков
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", "application/json")

	// Добавление query параметров, если они есть
	if len(queryParams) > 0 {
		q := req.URL.Query()
		for key, value := range queryParams {
			q.Add(key, value)
		}
		req.URL.RawQuery = q.Encode()
	}

	// Отправка запроса
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("request send error: %s", err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	// Чтение ответа
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("response read error: %s", err.Error())
		return nil, err
	}

	if resp.StatusCode > 200 {
		log.Errorf("response error: %s", string(respBody))
	}
	// log.Infof("response: %s", string(respBody))

	// Создаем переменную типа T для хранения результата
	var responseData T

	err = json.Unmarshal(respBody, &responseData)
	if err != nil {
		log.Errorf("json unmarshal error: %s", err.Error())
		return nil, err
	}

	return &responseData, nil
}
