package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) Logout(platform, profile_uuid string) (*wapi.WapiResponse, error) {

	var pre_url string

	switch platform {
	case "tg":
		pre_url = "/tapi"
	case "wz":
		pre_url = "/api"
	case "av":
		pre_url = "/avito"
	default:
		pre_url = "/api"
	}

	queryParams := map[string]string{
		"profile_id": profile_uuid,
	}

	response, err := HTTPSend[wapi.WapiResponse](r.Token, r.WappiURL+pre_url+"/profile/logout", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	return response, nil
}
