package requests

import (
	"encoding/json"
	"fmt"

	"gitlab.com/wapiteam/wapiteam/wapi"
)

func (r *WappiRequests) SendMessage(url, profile_uuid, bot_id, logTag string, body *wapi.JsonBody) (*wapi.WapiStringResponse, error) {

	queryParams := map[string]string{
		"profile_id": profile_uuid,
	}

	if bot_id != "" {
		queryParams["bot_id"] = bot_id
	}

	bodyBytes, err := json.Marshal(body)
	if err != nil {
		return nil, fmt.Errorf(logTag+"не сформировал bodyBytes: %v", err)
	}

	response, err := HTTPSend[wapi.WapiStringResponse](r.Token, r.WappiURL+url, "POST", bodyBytes, queryParams)
	if err != nil {
		log.Errorf(logTag+"Error: %s", err)
		return nil, err
	}

	return response, nil
}
