package requests

import "gitlab.com/wapiteam/wapiteam/wapi"

func (r *WappiRequests) GetChatsFilterWA(profile wapi.WapiProfile, filter string) ([]wapi.Chat, error) {

	queryParams := map[string]string{
		"profile_id":  profile.UUID,
		"client_name": filter,
	}

	response, err := HTTPSend[wapi.WapiChatResponse](r.Token, r.WappiURL+"/api/sync/chats/filter", "GET", nil, queryParams)
	if err != nil {
		log.Errorf("Error: %s", err)
		return nil, err
	}

	// log.Debugf(response.Dialogs)

	return response.Dialogs, nil
}
