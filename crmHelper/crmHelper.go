package crmHelper

import (
	"bytes"
	"compress/gzip"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"math"
	"math/rand/v2"
	"net/http"
	"sync"
	"time"
)

type CrmReqSender struct {
	RequestFunc               func(*QRequest, map[string]string, ...interface{}) (any, error)
	ClientList                map[string]*Client
	delayBetweenRequests      float64
	RequestTickerInterval     time.Duration //how rapid to check async answer from crm in milliseconds
	RequestTimeout            time.Duration //time to wait of async answer from crm in seconds
	DefaultDelayBeforeRequest int           //default delay before request for all clients in seconds
	Ctx                       context.Context
	mutex                     sync.Mutex
}

type Client struct {
	id                   string
	DelayBeforeRequest   int //seconds to wait before request
	HeadersList          map[string]string
	requestToSendStack   []QRequest
	requestFinishedStack map[uint64]*QRequest
	crmReqSender         *CrmReqSender
	mutex                sync.Mutex
}

type QRequest struct {
	id         uint64
	Method     string
	Url        string
	Payload    []byte
	WhenToSend *time.Time //should be used with SkipAnswer == true
	SkipAnswer bool
	answer     any
	err        error
	client     *Client
}

var reqHelper *CrmReqSender = nil

func GetReqHelper() *CrmReqSender {
	if reqHelper == nil {
		defaultNextRequest := func(q *QRequest, headersList map[string]string, params ...any) (any, error) {
			if q == nil {
				return nil, fmt.Errorf("no requests at query")
			}
			req, err := http.NewRequest(q.Method, q.Url, bytes.NewBuffer(q.Payload))
			if err != nil {
				return nil, fmt.Errorf("error creating request: %v", err)
			}
			for h, v := range headersList {
				req.Header.Set(h, v)
			}
			client := &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
			}
			resp, err := client.Do(req)
			if err != nil {
				return nil, fmt.Errorf("error sending request: %v", err)
			}

			var reader io.ReadCloser
			switch resp.Header.Get("Content-Encoding") {
			case "gzip":
				reader, err = gzip.NewReader(resp.Body)
				if err != nil {
					return nil, fmt.Errorf("error creating gzip reader:%v", err)
				}
				defer reader.Close()
			default:
				reader = resp.Body
			}

			body, err := io.ReadAll(reader)
			if err != nil {
				return nil, fmt.Errorf("error reading body: %v", err)
			}
			defer resp.Body.Close()
			return body, nil
		}

		reqHelper = &CrmReqSender{
			RequestFunc:           defaultNextRequest,
			ClientList:            map[string]*Client{},
			RequestTickerInterval: 100,
			RequestTimeout:        120,
		}
	}
	return reqHelper
}

func (rh *CrmReqSender) SetRequestsPerSecond(requestsPerSecond float64) {
	var delay float64
	if requestsPerSecond != 0 {
		delay = 1 / requestsPerSecond
	}
	rh.delayBetweenRequests = math.Round(delay * 1000)
}

// returns client and bool if it was newly created
func (rh *CrmReqSender) GetClient(clientID string, headersList map[string]string) (*Client, bool) {
	rh.mutex.Lock()
	defer rh.mutex.Unlock()
	if rh.ClientList[clientID] == nil {
		cl := Client{
			id:                   clientID,
			requestToSendStack:   make([]QRequest, 0),
			requestFinishedStack: make(map[uint64]*QRequest, 0),
			HeadersList:          headersList,
			crmReqSender:         rh,
			DelayBeforeRequest:   rh.DefaultDelayBeforeRequest,
		}
		rh.ClientList[clientID] = &cl
		go rh.ClientList[clientID].run()
		return rh.ClientList[clientID], true
	}
	rh.ClientList[clientID].HeadersList = headersList
	return rh.ClientList[clientID], false
}

func (c *Client) Push(q *QRequest) uint64 {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if q != nil {
		if q.id == 0 {
			q.id = rand.Uint64()
		}
		q.answer = nil
		q.err = nil
		q.client = c
		c.requestToSendStack = append(c.requestToSendStack, *q)
		return q.id
	}
	return 0
}

func (c *Client) Pop() *QRequest {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if len(c.requestToSendStack) > 0 {
		r := c.requestToSendStack[len(c.requestToSendStack)-1]
		c.requestToSendStack = c.requestToSendStack[:len(c.requestToSendStack)-1]
		return &r
	}
	return nil
}

func (c *Client) SendNow(q QRequest) (any, error) {
	return c.crmReqSender.RequestFunc(&q, c.HeadersList)
}

func (c *Client) asyncSendRequest() (uint64, error) {
	q := c.Pop()
	// c.mutex.Lock()
	// defer c.mutex.Unlock()
	if q == nil {
		return 0, fmt.Errorf("no requests at query")
	}
	go func() {
		if q.WhenToSend == nil || q.WhenToSend.After(time.Now()) {
			if c.DelayBeforeRequest != 0 {
				time.Sleep(time.Duration(c.DelayBeforeRequest) * time.Second)
			}
			q.answer, q.err = c.crmReqSender.RequestFunc(q, c.HeadersList)
			if !q.SkipAnswer {
				c.requestFinishedStack[q.id] = q
			}
		} else {
			c.Push(q) //push back delayed request as latest
		}

	}()
	return q.id, nil
}

func (c *Client) AsyncWaitAnswer(id uint64) (any, error) {
	ticker := time.NewTicker(c.crmReqSender.RequestTickerInterval * time.Millisecond)
	timeoutChan := time.After(c.crmReqSender.RequestTimeout * time.Second)
	defer func() {
		c.mutex.Lock()
		if c.requestFinishedStack[id] != nil {
			delete(c.requestFinishedStack, id)
		}
		c.mutex.Unlock()
		ticker.Stop()
	}()

	for {
		select {
		case <-ticker.C:
			c.mutex.Lock()
			if _, ok := c.requestFinishedStack[id]; ok {
				answer := c.requestFinishedStack[id].answer
				err := c.requestFinishedStack[id].err
				c.mutex.Unlock()
				return answer, err
			}
			c.mutex.Unlock()
		case <-timeoutChan:
			return nil, fmt.Errorf("request with id %d timed out", id)
		}
	}
}

func (c *Client) run() {
	ctx := c.crmReqSender.Ctx

	ticker := time.NewTicker(time.Duration(c.crmReqSender.delayBetweenRequests) * time.Millisecond)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if len(c.requestToSendStack) > 0 {
				go func() {
					id, err := c.asyncSendRequest()
					if err != nil {
						fmt.Print(fmt.Errorf("request with id %d error: %v", id, err))
					}
				}()
			}
		case <-ctx.Done():
			return
		}
	}
}
