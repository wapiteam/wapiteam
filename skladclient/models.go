package skladclient

import "gitlab.com/wapiteam/wapiteam/utmGRPC"

// sklad
type SkladMessage struct {
	Event       string       `json:"event"`
	ProfileUUID string       `json:"profile_uuid"`
	Originator  string       `json:"originator"`
	Recipient   string       `json:"recipient"`
	Txt         string       `json:"txt"`
	IsOutgoing  bool         `json:"is_outgoing"`
	State       string       `json:"state"` //recieved, sent, read, failed
	ErrorText   string       `json:"error_text"`
	MediaUrl    string       `json:"media_url"`
	FileData    []byte       `json:"file_data,omitempty"`
	FileName    string       `json:"file_name,omitempty"`
	FileType    string       `json:"file_type,omitempty"`
	Time        string       `json:"time"`
	MediaType   string       `json:"media_type"` //image\document
	ChatID      string       `json:"chat_id"`
	ID          string       `json:"id"`
	FromWhere   string       `json:"from_where"`
	TaskID      string       `json:"task_id"`
	UTM         *utmGRPC.UTM `json:"utm,omitempty"`
	SenderName  string       `json:"sender_name"` //имя отправителя (может быть сам профиль)
	ChatName    string       `json:"chat_name"`   //имя клиента клиента (по ту сторону профиля)
	ChatType    string       `json:"chatType"`
}
