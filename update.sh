# Читаем сообщение коммита из консоли
echo "Enter the commit message: "
read message

# Получаем текущую версию релиза из docker-compose.yml
releaseVersion=$(grep 'version=' version.txt | cut -d'=' -f2 | tr -d 'v')
echo "Last release version $releaseVersion"

# Запрашиваем новую версию
echo "Set new version (without 'v' prefix):"
read version

# Записываем новую версию в файл
echo "version=v$version" > version.txt

# Добавляем изменения в git
git add .

# Коммитим изменения
git commit -m "$message"

#Добвляем тег
git tag -a "v$version" -m "$message"

#Пушим изменения
git push origin main --tags

echo "Чтоб спулить новую версию необходимо ввести на сервере go get -u gitlab.com/wapiteam/wapiteam@v$version"
