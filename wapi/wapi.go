package wapi

import (
	"errors"
	"fmt"
	"strings"

	"github.com/lib/pq"
	"gitlab.com/wapiteam/wapiteam/fsclient"
	"gitlab.com/wapiteam/wapiteam/wapilog"
	"gorm.io/gorm"
)

var log wapilog.Logger

func init() {
	log = wapilog.NewWapiLogger("INFO", "ALL WAPPI")
}

type Wapi struct {
	User            *WapiUser           `json:"user,omitempty"`
	Profile         *WapiProfile        `json:"profile,omitempty"`
	Token           *WapiAuthtokenToken `json:"token,omitempty"`
	Container       *WapiContainer      `json:"container,omitempty"`
	Payments        []WapiPayment       `json:"payments,omitempty"`
	Proxy           *WapiProxy          `json:"proxy,omitempty"`
	ProxyResrve     *WapiProxy          `json:"proxy,omitempty"`
	Authorization   *WapiAuthorization  `json:"authorization,omitempty"`
	TGApp           *TGApp              `json:"tgapp,omitempty"`
	Response        Response            `json:"response,omitempty"`
	Command         Command             `json:"command,omitempty"`
	YaBackUp        WapiBackUp          `json:"ya_back_up,omitempty"`
	WapiBackUp      WapiBackUp          `json:"wapi_back_up,omitempty"`
	Hurrican        Hurricane           `json:"hurrican,omitempty"`
	ExcludeContacts *ExcludeContacts    `json:"exclude_contacts,omitempty"`
	FSClient        *fsclient.FSClient  `json:"fsclient,omitempty"`
	db              *gorm.DB
}

func NewWapi(db *gorm.DB, profileId int) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Token:           &WapiAuthtokenToken{},
		Payments:        []WapiPayment{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	}

	if err := db.Table("wapi_profile").Where("id = ?", profileId).First(&wapi.Profile).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("authtoken_token").Where("user_id = ?", wapi.Profile.UserID).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_user").Where("id = ?", wapi.Profile.UserID).First(&wapi.User).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if wapi.Profile.SelfProxy != nil {
		wapi.Proxy.Proxy = *wapi.Profile.SelfProxy
	} else if wapi.Profile.ProxyID != nil && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_proxy").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_proxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_telegramproxy").Where("id = ?", wapi.Profile.TgProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_telegramproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.SpamProxyID.Valid && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_spamproxy").Where("id = ?", wapi.Profile.SpamProxyID.Int16).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_spamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgSpamProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_tgspamproxy").Where("id = ?", wapi.Profile.TgSpamProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_tgspamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	}

	if wapi.Profile.Platform == "wz" {
		if err := db.Table("wapi_proxyreserve").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.ProxyResrve).Error; err != nil {
			log.Errorf("ошибка резервной прокси: %s, profileID: %d", err.Error, wapi.Profile.ID)
		}
	}

	if err := db.Table("wapi_container").Where("profile_id = ?", profileId).First(&wapi.Container).Error; err != nil {
		log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_payment").Where("profile_id = ? AND status = true", wapi.Profile.ID).Find(&wapi.Payments).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_excludecontacts").Where("profile_id = ?", wapi.Profile.ID).Find(&wapi.ExcludeContacts).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	if wapi.Container.NodeID != nil {
		if err := db.Table("wapi_node").Where("id = ?", wapi.Container.NodeID).First(&wapi.Container.Node).Error; err != nil {
			log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			return nil, err
		}
	}

	if err := db.Table("tg_apps").Where("user_id = ?", wapi.User.ID).Find(&wapi.TGApp).Error; err != nil {
		log.Errorf("tg_apps get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	return wapi, nil
}

func NewWapiFromUUID(db *gorm.DB, token string, uuid string) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Payments:        []WapiPayment{},
		Token:           &WapiAuthtokenToken{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	}

	if err := db.Table("authtoken_token").Where("key = ?", token).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("token error")
	}

	if wapi.Token.UserID == 0 {
		return nil, errors.New("authorization token incorrect")
	}

	if err := db.Table("wapi_profile").Where("uuid = ?", uuid).Where("user_id = ?", wapi.Token.UserID).First(&wapi.Profile).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("profile_id error")
	}

	if err := db.Table("wapi_user").Where("id = ?", wapi.Profile.UserID).First(&wapi.User).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("auth token error")
	}

	if wapi.Profile.SelfProxy != nil {
		wapi.Proxy.Proxy = *wapi.Profile.SelfProxy
	} else if wapi.Profile.ProxyID != nil && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_proxy").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_proxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_telegramproxy").Where("id = ?", wapi.Profile.TgProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_telegramproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.SpamProxyID.Valid && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_spamproxy").Where("id = ?", wapi.Profile.SpamProxyID.Int16).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_spamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgSpamProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_tgspamproxy").Where("id = ?", wapi.Profile.TgSpamProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_tgspamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	}

	if wapi.Profile.Platform == "wz" {
		if err := db.Table("wapi_proxyreserve").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.ProxyResrve).Error; err != nil {
			log.Errorf("ошибка резервной прокси: %s, profileID: %d", err.Error, wapi.Profile.ID)
		}
	}

	if err := db.Table("wapi_container").Where("profile_id = ?", wapi.Profile.ID).First(&wapi.Container).Error; err != nil {
		log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_payment").Where("profile_id = ? AND status = true", wapi.Profile.ID).Find(&wapi.Payments).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_excludecontacts").Where("profile_id = ?", wapi.Profile.ID).Find(&wapi.ExcludeContacts).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	if wapi.Container.NodeID != nil {
		if err := db.Table("wapi_node").Where("id = ?", wapi.Container.NodeID).First(&wapi.Container.Node).Error; err != nil {
			log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			return nil, err
		}
	}

	if err := db.Table("tg_apps").Where("user_id = ?", wapi.User.ID).Find(&wapi.TGApp).Error; err != nil {
		log.Errorf("tg_apps get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	return wapi, nil
}

// for vk
func NewWapiByUUIDWithoutToken(db *gorm.DB, uuid string) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Payments:        []WapiPayment{},
		Token:           &WapiAuthtokenToken{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	}

	if err := db.Table("wapi_profile").Where("uuid = ?", uuid).First(&wapi.Profile).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("profile_id error")
	}

	if err := db.Table("wapi_user").Where("id = ?", wapi.Profile.UserID).First(&wapi.User).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("auth token error")
	}

	if err := db.Table("authtoken_token").Where("user_id = ?", wapi.Profile.UserID).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("token error")
	}

	if wapi.Token.Key == "" {
		return nil, errors.New("authorization token incorrect")
	}

	if wapi.Profile.SelfProxy != nil {
		wapi.Proxy.Proxy = *wapi.Profile.SelfProxy
	} else if wapi.Profile.ProxyID != nil && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_proxy").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_proxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_telegramproxy").Where("id = ?", wapi.Profile.TgProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_telegramproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.SpamProxyID.Valid && wapi.Profile.Platform != "tg" {
		err := db.Table("wapi_spamproxy").Where("id = ?", wapi.Profile.SpamProxyID.Int16).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_spamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	} else if wapi.Profile.TgSpamProxyID != nil && wapi.Profile.Platform == "tg" {
		err := db.Table("wapi_tgspamproxy").Where("id = ?", wapi.Profile.TgSpamProxyID).First(&wapi.Proxy).Error
		if err != nil {
			log.Errorf("wapi_tgspamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
			return nil, err
		}
	}

	if wapi.Profile.Platform == "wz" {
		if err := db.Table("wapi_proxyreserve").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.ProxyResrve).Error; err != nil {
			log.Errorf("ошибка резервной прокси: %s, profileID: %d", err.Error, wapi.Profile.ID)
		}
	}

	if err := db.Table("wapi_container").Where("profile_id = ?", wapi.Profile.ID).First(&wapi.Container).Error; err != nil {
		log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_payment").Where("profile_id = ? AND status = true", wapi.Profile.ID).Find(&wapi.Payments).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, err
	}

	if err := db.Table("wapi_excludecontacts").Where("profile_id = ?", wapi.Profile.ID).Find(&wapi.ExcludeContacts).Error; err != nil {
		log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	if wapi.Container.NodeID != nil {
		if err := db.Table("wapi_node").Where("id = ?", wapi.Container.NodeID).First(&wapi.Container.Node).Error; err != nil {
			log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			return nil, err
		}
	}

	if err := db.Table("tg_apps").Where("user_id = ?", wapi.User.ID).Find(&wapi.TGApp).Error; err != nil {
		log.Errorf("tg_apps get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	return wapi, nil
}

func NewWapiFromToken(db *gorm.DB, token string) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Payments:        []WapiPayment{},
		Token:           &WapiAuthtokenToken{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	} // TODO: add other fields

	if err := db.Table("authtoken_token").Where("key = ?", token).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("token error")
	}

	if wapi.Token.UserID == 0 {
		return nil, errors.New("authorization token incorrect")
	}

	if err := db.Table("wapi_user").Where("id = ?", wapi.Token.UserID).First(&wapi.User).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("auth token error")
	}

	if wapi.User.ID == 0 {
		return nil, errors.New("authorization token incorrect")
	}

	if err := db.Table("tg_apps").Where("user_id = ?", wapi.User.ID).Find(&wapi.TGApp).Error; err != nil {
		log.Errorf("tg_apps get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
	}

	return wapi, nil
}

//	func parsePgsqlArray(str string) []string {
//		if len(str) > 2 {
//			str = str[:len(str)-1]
//			str = str[1:]
//			return strings.Split(str, ",")
//		} else {
//			return []string{}
//		}
//	}

// user only... at least now
func NewWapiFromEmail(db *gorm.DB, email string) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Payments:        []WapiPayment{},
		Token:           &WapiAuthtokenToken{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	}

	err := db.Table("wapi_user").Where("email = ?", email).First(wapi.User).Error
	if err != nil {
		return nil, fmt.Errorf("no user found by email [%v]:%v", email, err)
	}

	return wapi, nil
}

func ArrContains(array []string, needle string) bool {
	for _, n := range array {
		if needle == n {
			return true
		}
	}
	return false
}

func RemovePlusPrefix(input string) string {
	if len(input) > 0 && input[0] == '+' {
		return input[1:]
	}
	return input
}

func RemovePlusMinusAndSpaces(input string) string {
	result := strings.ReplaceAll(input, "+", "")
	result = strings.ReplaceAll(result, "-", "")
	result = strings.ReplaceAll(result, " ", "")
	return result
}

func ArrContainsWithRemovePlusMinusAndSpaces(array []string, needle string) bool {
	for _, n := range array {
		if needle == RemovePlusMinusAndSpaces(n) {
			return true
		}
	}
	return false
}

func SQLARRContains(array pq.StringArray, needle string) bool {
	for _, n := range array {
		if needle == n {
			return true
		}
	}
	return false
}

func GetWapiUserWithTokenByID(db *gorm.DB, id int) (*Wapi, error) {
	wapi := &Wapi{
		User:            &WapiUser{},
		Profile:         &WapiProfile{},
		Payments:        []WapiPayment{},
		Token:           &WapiAuthtokenToken{},
		Container:       &WapiContainer{},
		Proxy:           &WapiProxy{},
		ProxyResrve:     &WapiProxy{},
		Authorization:   &WapiAuthorization{},
		ExcludeContacts: &ExcludeContacts{},
		TGApp:           &TGApp{},
		Response:        nil,
		Command:         nil,
		YaBackUp:        nil,
		WapiBackUp:      nil,
		Hurrican:        nil,
		db:              db,
	}

	err := db.Table("wapi_user").Where("id = ?", id).First(wapi.User).Error
	if err != nil {
		return nil, fmt.Errorf("no user found by id [%v]:%v", id, err)
	}
	err = db.Table("authtoken_token").Where("user_id = ?", id).First(wapi.Token).Error
	if err != nil {
		return wapi, fmt.Errorf("no token found by id [%v]:%v (but user is)", id, err)
	}

	return wapi, nil
}

// do not touch!
func AllWapiByUUID(db *gorm.DB, token string, uuid string) (*Wapi, error) {
	wapi := &Wapi{
		// User:            &WapiUser{},
		// Profile:         &WapiProfile{},
		// Payments:        []WapiPayment{},
		// Token:           nil,
		// Container:       nil,
		// Proxy:           &WapiProxy{},
		// ProxyResrve:     &WapiProxy{},
		// Authorization:   &WapiAuthorization{},
		// ExcludeContacts: &ExcludeContacts{},
		// TGApp:           &TGApp{},
		// Response:        nil,
		// Command:         nil,
		// YaBackUp:        nil,
		// WapiBackUp:      nil,
		// Hurrican:        nil,
		db: db,
	}
	var errr error

	if err := db.Table("authtoken_token").Where("key = ?", token).First(&wapi.Token).Error; err != nil {
		log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		return nil, errors.New("token error")
	}

	if wapi.Token.UserID == 0 {
		errr = errors.New("authorization token incorrect")
	}

	if wapi.Token != nil {
		if err := db.Table("wapi_profile").Where("uuid = ?", uuid).Where("user_id = ?", wapi.Token.UserID).First(&wapi.Profile).Error; err != nil {
			log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			errr = errors.New("profile_id error")
		}
	}

	if wapi.Profile != nil {
		if err := db.Table("wapi_user").Where("id = ?", wapi.Profile.UserID).First(&wapi.User).Error; err != nil {
			log.Errorf("WapiAuthtokenToken get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			return nil, errors.New("auth token error")
		}

		if wapi.Profile.SelfProxy != nil {
			wapi.Proxy.Proxy = *wapi.Profile.SelfProxy
		} else if wapi.Profile.ProxyID != nil && wapi.Profile.Platform != "tg" {
			err := db.Table("wapi_proxy").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.Proxy).Error
			if err != nil {
				log.Errorf("wapi_proxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
				errr = err
			}
		} else if wapi.Profile.TgProxyID != nil && wapi.Profile.Platform == "tg" {
			err := db.Table("wapi_telegramproxy").Where("id = ?", wapi.Profile.TgProxyID).First(&wapi.Proxy).Error
			if err != nil {
				log.Errorf("wapi_telegramproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
				errr = err
			}
		} else if wapi.Profile.SpamProxyID.Valid && wapi.Profile.Platform != "tg" {
			err := db.Table("wapi_spamproxy").Where("id = ?", wapi.Profile.SpamProxyID.Int16).First(&wapi.Proxy).Error
			if err != nil {
				log.Errorf("wapi_spamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
				errr = err
			}
		} else if wapi.Profile.TgSpamProxyID != nil && wapi.Profile.Platform == "tg" {
			err := db.Table("wapi_tgspamproxy").Where("id = ?", wapi.Profile.TgSpamProxyID).First(&wapi.Proxy).Error
			if err != nil {
				log.Errorf("wapi_tgspamproxy get error: %s, profileID: %d", err.Error(), wapi.Profile.ID)
				errr = err
			}
		}

		if wapi.Profile.Platform == "wz" {
			if err := db.Table("wapi_proxyreserve").Where("id = ?", wapi.Profile.ProxyID).First(&wapi.ProxyResrve).Error; err != nil {
				log.Errorf("ошибка резервной прокси: %s, profileID: %d", err.Error, wapi.Profile.ID)
			}
		}

		if err := db.Table("wapi_container").Where("profile_id = ?", wapi.Profile.ID).First(&wapi.Container).Error; err != nil {
			log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			errr = err
		}

		if err := db.Table("wapi_payment").Where("profile_id = ? AND status = true", wapi.Profile.ID).Find(&wapi.Payments).Error; err != nil {
			log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
			errr = err
		}

		if err := db.Table("wapi_excludecontacts").Where("profile_id = ?", wapi.Profile.ID).Find(&wapi.ExcludeContacts).Error; err != nil {
			log.Errorf("wapi_payment get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		}

		if wapi.Container != nil && wapi.Container.NodeID != nil {
			if err := db.Table("wapi_node").Where("id = ?", wapi.Container.NodeID).First(&wapi.Container.Node).Error; err != nil {
				log.Errorf("wapi_container get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
				errr = err
			}
		}

		if err := db.Table("tg_apps").Where("user_id = ?", wapi.User.ID).Find(&wapi.TGApp).Error; err != nil {
			log.Errorf("tg_apps get error: %s, profileID: %d", err.Error, wapi.Profile.ID)
		}
	}

	return wapi, errr
}
