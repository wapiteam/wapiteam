package wapi

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type WapiResponse struct {
	Status       string    `json:"status"`
	Timestamp    int64     `json:"timestamp"`
	Time         string    `json:"time"`
	Detail       string    `json:"detail"`
	TaskID       string    `json:"task_id"`
	CommandStart time.Time `json:"command_start"`
	CommandEnd   time.Time `json:"command_end"`
	UUID         string    `json:"uuid"`
	Error        string    `json:"error,omitempty"`
}

type WapiTGChatsResponse struct {
	WapiResponse
	Chats      []CustomTGChat `json:"dialogs,omitempty"`
	TotalCount int64          `json:"total_count,omitempty"`
}

type WapiFetchMessage struct {
	WapiResponse
	Message *WapiMessage `json:"message"`
}

type WapiFetchMessages struct {
	WapiResponse
	Messages   []WapiMessage `json:"messages"`
	TotalCount int64         `json:"total_count"`
}

type WapiStringResponse struct {
	WapiResponse
	MessageId      string `json:"message_id,omitempty"`
	Pts            int    `json:"pts,omitempty"`
	PtsCount       int    `json:"pts_count,omitempty"`
	StoryID        string `json:"story_id,omitempty"`
	DeliveryStatus string `json:"delivery_status,omitempty"`
}

type WapiIsRegistredResponse struct {
	WapiResponse
	Phone      string `json:"phone"`
	OnWhatsapp bool   `json:"on_whatsapp"`
}

type WapiDownloadFileResponse struct {
	WapiResponse
	FileB64         string `json:"file_b64,omitempty"`
	FileName        string `json:"file_name,omitempty"`
	MediaType       string `json:"media_type,omitempty"`
	MimeType        string `json:"mime_type,omitempty"`
	FileLink        string `json:"file_link,omitempty"`
	FlileLinkExpire int64  `json:"file_link_expire,omitempty"`
}

type WapiQrResponse struct {
	WapiResponse
	QRCode *string `json:"qrCode,omitempty"`
}

type WapiChatResponse struct {
	WapiResponse
	Dialogs    []Chat `json:"dialogs,omitempty"`
	TotalCount int64  `json:"total_count"`
}

type WapiTGVeiwsResponse struct {
	WapiResponse
	TotalViews int         `json:"views,omitempty"`
	Offset     any         `json:"offset,omitempty"`
	Users      []TGContact `json:"users,omitempty"`
}

type WapiInterfaceResponse struct {
	WapiResponse
	Result  []interface{} `json:"result,omitempty"`
	IResult []interface{} `json:"interface_result,omitempty"`
}

type WapiContactsResponse struct {
	WapiResponse
	Contacts []*TGContact `json:"contacts"`
}

type WapiContactResponse struct {
	WapiResponse
	Contact *TGContact `json:"contact"`
}

func (wr *WapiResponse) SendWapiResponse(ctx context.Context, jmsg []byte, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	if cmd == nil {
		return fmt.Errorf("cmd is nil")
	}

	if broker == nil {
		return fmt.Errorf("broker is nil")
	}

	if err := broker.Publish(ctx, cmd.UUID, cmd.UUID+"_"+cmd.TaskID, jmsg); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiInterfaceResponse to wapi success {command: %s, %s} {profile.uuid: %s}", cmd.Command, cmd.TaskID, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiStringResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	if taskQueue != nil {
		if err := broker.Publish(ctx, "amq.direct", *taskQueue, jmsg); err != nil {
			log.Errorf("WapiStringResponse Publish to tasks error %s", err.Error())
			return err
		}
	}
	log.Resultf("WapiStringResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiContactsResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiContactsResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiContactResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiContactResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiInterfaceResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiInterfaceResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiFetchMessage) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiFetchMessage to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiFetchMessages) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiInterfaceResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiTGChatsResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiInterfaceResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiTGVeiwsResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiTGVeiwsResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}

func (wr *WapiDownloadFileResponse) SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error {
	wr.CommandEnd = time.Now()
	wr.UUID = cmd.UUID
	wr.CommandStart = cmd.CommandStart
	wr.TaskID = cmd.TaskID

	if len(wr.Error) > 0 {
		wr.Status = "error"
		log.Errorf("error: %s", wr.Detail)
	} else {
		wr.Status = "done"
		wr.Error = ""
	}

	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("WapiStringResponse Marshal error %s", err.Error())
		return err
	}

	if err := wr.WapiResponse.SendWapiResponse(ctx, jmsg, broker, cmd, taskQueue); err != nil {
		log.Errorf("WapiStringResponse Publish error %s", err.Error())
		return err
	}

	log.Resultf("WapiInterfaceResponse to wapi success {command: %s}	{profile.uuid: %s}", cmd.Command, cmd.UUID)
	wr = nil
	return nil
}
