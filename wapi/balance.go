package wapi

import (
	"math"
	"time"

	"gorm.io/gorm"
)

var (
	balanceTypes = map[string]func(amount float64) (float64, bool){
		"add_balance":      makePositiveFalse,
		"hand_add_balance": makePositiveTrue,
		"add_days":         makeNegativeTrue,
		"payment_request":  makeNegativeFalse,
		"gpt_refound":      makeNegativeTrue,
		"waba_refound":     makeNegativeTrue,
		"referral":         makePositiveTrue,
	}
)

type WapiBalance struct {
	ID          uint32         `json:"id,omitempty"`
	UserId      uint           `json:"user_id,omitempty"`
	Balance     float64        `json:"balance,omitempty"`
	Amount      float64        `json:"amount,omitempty"`
	Type        string         `json:"type,omitempty"`
	ProfileID   *int           `json:"profile_id,omitempty"`
	Status      bool           `json:"status,omitempty"`
	CreatedAt   time.Time      `json:"created_at,omitempty"`
	CreatedTime string         `json:"created_time,omitempty" gorm:"-"`
	Profile     WapiProfile    `json:"profile,omitempty" gorm:"foreignKey:ProfileID"`
	Location    *time.Location `json:"location,omitempty" gorm:"-"`
}

func makeNegativeTrue(value float64) (float64, bool) {
	if value > 0 {
		return -value, true
	}
	return value, true
}

func makeNegativeFalse(value float64) (float64, bool) {
	if value > 0 {
		return -value, false
	}
	return value, false
}

func makePositiveFalse(value float64) (float64, bool) {
	if value < 0 {
		return -value, false
	}
	return value, false
}

func makePositiveTrue(value float64) (float64, bool) {
	if value < 0 {
		return -value, true
	}
	return value, true
}

type BalanceOption func(*WapiBalance)

func BalanceWithLocation(location *time.Location) BalanceOption {
	return func(b *WapiBalance) {
		b.CreatedAt = time.Now().In(location)
		b.Location = location
	}
}

func (w *Wapi) CreateBalance(amount float64, balanceType string, profileId *int, opts ...BalanceOption) (*WapiBalance, error) {
	var status bool
	amount, status = balanceTypes[balanceType](amount)
	amount = math.Round(float64(amount)*100) / 100

	balance := &WapiBalance{
		UserId:    uint(w.User.ID),
		Amount:    amount,
		Type:      balanceType,
		ProfileID: profileId,
		Status:    status,
		CreatedAt: time.Now(),
	}

	if balance.Type == "payment_request" {
		status = true
		balance.Status = true
	}

	for _, opt := range opts {
		opt(balance)
	}

	if status {
		old_balance, err := w.GetBalance()
		if err != nil {
			log.Errorf("Error getting balance: %v", err)
			return nil, err
		}

		balance.Balance = math.Round((old_balance+amount)*100) / 100

		w.User.Balance = balance.Balance
		if err := w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("balance", balance.Balance).Error; err != nil {
			log.Errorf("Error updating balance: %v", err)
			return nil, err
		}
	}

	if balance.Type == "gpt_refound" {
		var existingBalance WapiBalance
		var today string
		if balance.Location != nil {
			today = time.Now().In(balance.Location).Format("2006-01-02")
		} else {
			today = time.Now().Format("2006-01-02")
		}

		err := w.db.Table("wapi_balancehistory").Where("user_id = ? AND type = ? AND DATE(created_at) = ?", balance.UserId, balance.Type, today).First(&existingBalance).Error
		if err == nil {

			balanceSummRound := math.Round((existingBalance.Balance+balance.Amount)*100) / 100
			amountSummRound := math.Round((existingBalance.Amount+balance.Amount)*100) / 100

			err = w.db.Table("wapi_balancehistory").
				Where("id = ?", existingBalance.ID).
				Update("balance", balanceSummRound).
				Update("amount", amountSummRound).Error
			if err != nil {
				log.Errorf("Ошибка при обновлении баланса у полбзователя ID: %d, error: %v", w.User.ID, err)
			} else {
				log.Warnf("Баланс UUID: %s, UserID: %d обновлён", w.Profile.UUID, w.User.ID)
			}
		} else if err == gorm.ErrRecordNotFound {
			if err := w.db.Table("wapi_balancehistory").Create(balance).Error; err != nil {
				log.Errorf("Error creating balance history: %v", err)
				return nil, err
			}
		}
	} else {
		if err := w.db.Table("wapi_balancehistory").Create(balance).Error; err != nil {
			log.Errorf("Error creating balance history: %v", err)
			return nil, err
		}
	}
	return balance, nil
}

func (w *Wapi) GetBalance() (float64, error) {
	var balance float64
	if err := w.db.Table("wapi_balancehistory").Where("user_id = ? AND status = ?", w.User.ID, true).Select("COALESCE(SUM(amount), 0)").Scan(&balance).Error; err != nil {
		return 0, err
	}

	return balance, nil
}

func (w *Wapi) GetBalanceHistory(limit, offset int) ([]WapiBalance, int64, error) {
	var balance []WapiBalance
	var totalRows int64

	if err := w.db.Preload("Profile").Table("wapi_balancehistory").Where("user_id = ? AND status = ?", w.User.ID, true).Order("created_at desc").Limit(limit).Offset(offset).Find(&balance).Error; err != nil {
		return nil, totalRows, err
	}

	if err := w.db.Table("wapi_balancehistory").Where("user_id = ? AND status = ?", w.User.ID, true).Order("created_at desc").
		Count(&totalRows).Error; err != nil {
		return nil, totalRows, err
	}

	for i, b := range balance {
		balance[i].CreatedTime = b.CreatedAt.Format("02.01.2006 15:04:05")
	}

	return balance, totalRows, nil
}

func (w *Wapi) GetBalanceHistoryByType(limit, offset int, balanceType string) ([]WapiBalance, int64, error) {
	var balance []WapiBalance
	var totalRows int64

	if err := w.db.Preload("Profile").Table("wapi_balancehistory").Where("user_id = ? AND type = ?", w.User.ID, balanceType).Order("created_at desc").Limit(limit).Offset(offset).Find(&balance).Error; err != nil {
		return nil, totalRows, err
	}

	if err := w.db.Table("wapi_balancehistory").Where("user_id = ? AND type = ?", w.User.ID, balanceType).Order("created_at desc").
		Count(&totalRows).Error; err != nil {
		return nil, totalRows, err
	}

	for i, b := range balance {
		balance[i].CreatedTime = b.CreatedAt.Format("02.01.2006 15:04:05")
	}

	return balance, totalRows, nil
}

func (w *Wapi) GetBalanceHistoryByProfile(limit, offset int, profileId int) ([]WapiBalance, int64, error) {
	var balance []WapiBalance
	var totalRows int64

	if err := w.db.Preload("Profile").Table("wapi_balancehistory").Where("user_id = ? AND profile_id = ? AND status = ?", w.User.ID, profileId, true).Order("created_at desc").Limit(limit).Offset(offset).Find(&balance).Error; err != nil {
		return nil, totalRows, err
	}

	if err := w.db.Table("wapi_balancehistory").Where("user_id = ? AND profile_id = ? AND status = ?", w.User.ID, profileId, true).
		Count(&totalRows).Error; err != nil {
		return nil, totalRows, err
	}

	for i, b := range balance {
		balance[i].CreatedTime = b.CreatedAt.Format("02.01.2006 15:04:05")
	}
	return balance, totalRows, nil
}

func (w *Wapi) GetBalanceByID(paymentID uint) (*WapiBalance, error) {
	balance := &WapiBalance{}
	if err := w.db.Table("wapi_balancehistory").Where("id = ?", paymentID).First(balance).Error; err != nil {
		return nil, err
	}
	return balance, nil
}

func (w *Wapi) UpdateBalanceStatus(paymentID uint, status bool, balanceType string) error {
	if status {
		var balance WapiBalance
		if err := w.db.Table("wapi_balancehistory").Where("id = ?", paymentID).First(&balance).Error; err != nil {
			log.Errorf("Error getting balance: %v", err)
			return err
		}

		amount, typeStatus := balanceTypes[balanceType](balance.Amount)

		if !typeStatus {
			old_balance, err := w.GetBalance()
			if err != nil {
				log.Errorf("Error getting balance: %v", err)
				return err
			}
			result_balance := old_balance + amount
			if err := w.db.Table("wapi_balancehistory").Where("id = ?", paymentID).UpdateColumn("balance", result_balance).Error; err != nil {
				log.Errorf("Error updating balance: %v", err)
				return err
			}
			if err := w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("balance", result_balance).Error; err != nil {
				log.Errorf("Error updating balance: %v", err)
				return err
			}
		}

		if balanceType == "add_balance" {
			var msterToken WapiAuthtokenToken
			if w.User.RefID != nil {
				log.Infof("RefID: %d", w.User.RefID)
				if err := w.db.Table("authtoken_token").Where("user_id = ?", w.User.RefID).First(&msterToken).Error; err != nil {
					return err
				}

				masterWapi, err := NewWapiFromToken(w.db, msterToken.Key)
				if err != nil {
					log.Errorf("Error getting master wapi: %v", err)
					return err
				}

				var profileId *int
				if balance.ProfileID != nil {
					profileId = balance.ProfileID
				}

				amount := float64(amount) * 0.1
				if _, err := masterWapi.CreateBalance(amount, "referral", profileId); err != nil {
					log.Errorf("Error creating balance: %v", err)
					return err
				}
			}
		}
	}

	if err := w.db.Table("wapi_balancehistory").Where("id = ?", paymentID).UpdateColumn("status", status).Error; err != nil {
		log.Errorf("Error updating balance status: %v", err)
		return err
	}

	return nil
}
