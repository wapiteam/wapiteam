package wapi

type WAContact struct {
	ID       string `json:"id"`
	Number   string `json:"number"`
	Pushname string `json:"pushname"`
	IsMe     bool   `json:"isMe"`
}
