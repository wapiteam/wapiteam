package wapi

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type WapiPayment struct {
	ID            int        `json:"id,omitempty"`
	Status        bool       `json:"status,omitempty"`
	ExpiredAt     *time.Time `json:"expired_at,omitempty"`
	PaymentAt     *time.Time `json:"payment_at,omitempty"`
	CreatedAt     time.Time  `json:"created_at,omitempty"`
	ProfileId     int        `json:"profile_id,omitempty"`
	TariffId      uint32     `json:"tariff_id,omitempty"`
	PaymentId     string     `json:"payment_id,omitempty"`
	IsAutoPayment bool       `json:"is_auto_payment,omitempty"`
	AutoPaymentId *uint32    `json:"auto_payment_id,omitempty"`
	PromoId       *uint32    `json:"promo_id,omitempty"`
	Hand          bool       `json:"hand,omitempty"`
	UserID        int        `json:"user_id,omitempty"`
	UUID          string     `json:"uuid,omitempty"`
}

func (w *Wapi) GetLatestPayment() (*WapiPayment, error) {
	var latestPayment *WapiPayment
	if w.User.WithPayments {
		for _, payment := range w.Payments {
			if payment.Status {
				if latestPayment == nil {
					latestPayment = &payment
				} else if payment.PaymentAt != nil && latestPayment.PaymentAt != nil {
					if payment.PaymentAt.After(*latestPayment.PaymentAt) {
						latestPayment = &payment
					}
				}
			}
		}

		if latestPayment == nil {
			return nil, fmt.Errorf("профиль %s, не оплачен", w.Profile.UUID)
		}
	}

	return latestPayment, nil
}

func (w *Wapi) GetLastPaymentFromDb() (*WapiPayment, error) {

	var payment *WapiPayment
	if err := w.db.Table("wapi_payment").Where("profile_id = ? AND status = true", w.Profile.ID).Order("expired_at desc").First(&payment).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, w.Profile.ID)
		return nil, err
	}

	return payment, nil
}

func (w *Wapi) CreatePayment(tarifID uint32, isSubscribe bool, promoID *uint32) (*WapiPayment, error) {

	paymentID := uuid.New().String()

	payment := &WapiPayment{
		Status:    false,
		CreatedAt: time.Now(),
		ProfileId: w.Profile.ID,
		TariffId:  tarifID,
		PaymentId: paymentID,
		PromoId:   promoID,
		UserID:    w.User.ID,
		UUID:      w.Profile.UUID,
	}

	if err := w.db.Table("wapi_payment").Create(&payment).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, w.Profile.ID)
		return nil, err
	}

	return payment, nil
}

func (w *Wapi) UpdatePayment(payment *WapiPayment) error {
	if err := w.db.Table("wapi_payment").Save(&payment).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, profileID: %d", err.Error, w.Profile.ID)
		return err
	}

	return nil
}

func (w *Wapi) PaymentSucces(tariffID uint32, paymentID string, isAutoPayment bool, promoCode *string, isHand bool) error {
	var lastPayment *WapiPayment
	var currentPayment *WapiPayment
	currentTime := time.Now()

	container := &WapiContainer{}

	err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).First(container).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	} else if errors.Is(err, gorm.ErrRecordNotFound) || container.ContainerID == nil || (container.ContainerID != nil && *container.ContainerID == "deleted") {

		if errors.Is(err, gorm.ErrRecordNotFound) {
			container = &WapiContainer{
				ProfileID:     w.Profile.ID,
				WaStatus:      "close",
				ContainerName: proto.String(strconv.Itoa(w.Profile.ID)),
			}
			err = w.db.Table("wapi_container").Create(container).Error
			if err != nil {
				return err
			}
		}

		w.ProxyNodes(w.Profile, container, true)
	}

	err = w.db.Table("wapi_payment").Where("payment_id = ? AND status = ?", paymentID, false).First(&currentPayment).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	} else if errors.Is(err, gorm.ErrRecordNotFound) {
		return errors.New("payment not found")
	}

	err = w.db.Table("wapi_payment").Where("profile_id = ? AND status = ?", w.Profile.ID, true).Order("expired_at desc").First(&lastPayment).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	var newExpiredAt time.Time
	var monthCount int
	var dayCount int
	switch tariffID {
	case 1:
		monthCount = 1
	case 2:
		monthCount = 3
	case 3:
		monthCount = 6
	case 4:
		monthCount = 12
	case 5:
		dayCount = 5
	default:
		return errors.New("tariffID not found")
	}

	if lastPayment != nil && lastPayment.ExpiredAt != nil {
		if lastPayment.ExpiredAt.After(currentTime) || lastPayment.ExpiredAt.Equal(currentTime) {
			newExpiredAt = lastPayment.ExpiredAt.AddDate(0, monthCount, dayCount)
		} else {
			newExpiredAt = currentTime.AddDate(0, monthCount, dayCount)
		}
	} else {
		newExpiredAt = currentTime.AddDate(0, monthCount, dayCount)
	}

	currentPayment.Status = true
	currentPayment.ExpiredAt = &newExpiredAt
	currentPayment.PaymentAt = &currentTime
	currentPayment.ProfileId = w.Profile.ID
	currentPayment.TariffId = tariffID
	currentPayment.PaymentId = paymentID
	currentPayment.IsAutoPayment = isAutoPayment
	currentPayment.UserID = w.User.ID
	currentPayment.UUID = w.Profile.UUID
	currentPayment.Hand = isHand

	if promoCode != nil {
		var promo WapiPromo
		if err = w.db.Table("wapi_promo").Where("code = ?", &promoCode).First(&promo).Error; err != nil {
			if !errors.Is(err, gorm.ErrRecordNotFound) {
				log.Errorf("promo get error: %s, promoCode: %s", err.Error, promoCode)
			}
		} else {
			currentPayment.PromoId = &promo.ID
			if err := w.db.Table("wapi_promo").Where("code = ?", &promoCode).UpdateColumn("uses", gorm.Expr("uses + ?", 1)).Error; err != nil {
				log.Errorf("инкремент промокода error: %s, promoCode: %s", err.Error, promoCode)
			}
		}
	}

	if err = w.db.Table("wapi_payment").Where("payment_id = ?", paymentID).Save(&currentPayment).Error; err != nil {
		return err
	}

	log.Infof("Payment success: %s", paymentID)

	if tariffID == 5 {
		if w.Profile.Platform == "tg" {
			if err = w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("use_test_tg", true).Error; err != nil {
				return err
			}
		} else if w.Profile.Platform == "wz" {
			if err = w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("use_test", true).Error; err != nil {
				return err
			}
		} else if w.Profile.Platform == "av" {
			if err = w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("use_test_av", true).Error; err != nil {
				return err
			}
		} else if w.Profile.Platform == "tgbot" {
			if err = w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("use_test_tgbot", true).Error; err != nil {
				return err
			}
		} else {
			return errors.New("platform not found")
		}
	}
	return nil

}

func (w *Wapi) CreateProfileMonthPayment(monthCount int, tariffID uint32) (string, error) {
	var lastPayment *WapiPayment
	currentTime := time.Now()

	container := &WapiContainer{}

	err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).First(container).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return "", err
	} else if errors.Is(err, gorm.ErrRecordNotFound) || container.ContainerID == nil || (container.ContainerID != nil && *container.ContainerID == "deleted") {

		if errors.Is(err, gorm.ErrRecordNotFound) {
			container = &WapiContainer{
				ProfileID:     w.Profile.ID,
				WaStatus:      "close",
				ContainerName: proto.String(strconv.Itoa(w.Profile.ID)),
			}
			err = w.db.Table("wapi_container").Create(container).Error
			if err != nil {
				return "", err
			}
		}

		w.ProxyNodes(w.Profile, container, true)
	}

	err = w.db.Table("wapi_payment").Where("profile_id = ? AND status = ?", w.Profile.ID, true).Order("expired_at desc").First(&lastPayment).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return "", err
	}
	var newExpiredAt time.Time

	if lastPayment != nil && lastPayment.ExpiredAt != nil {
		if lastPayment.ExpiredAt.After(currentTime) || lastPayment.ExpiredAt.Equal(currentTime) {
			newExpiredAt = lastPayment.ExpiredAt.AddDate(0, monthCount, 0)
		} else {
			newExpiredAt = currentTime.AddDate(0, monthCount, 0)
		}
	} else {
		newExpiredAt = currentTime.AddDate(0, monthCount, 0)
	}

	paymentID := uuid.New().String()

	newPayment := WapiPayment{
		Status:        true,
		ExpiredAt:     &newExpiredAt,
		CreatedAt:     currentTime,
		ProfileId:     w.Profile.ID,
		TariffId:      tariffID,
		PaymentId:     paymentID,
		IsAutoPayment: false,
		Hand:          false,
		UserID:        int(w.Profile.UserID),
		UUID:          w.Profile.UUID,
	}

	err = w.db.Table("wapi_payment").Create(&newPayment).Error
	if err != nil {
		return "", err
	}

	return paymentID, nil

}

func (w *Wapi) CreateProfileDayPayment(dayCount int, tariffID uint32) (string, error) {
	var lastPayment *WapiPayment
	currentTime := time.Now()

	//asdasd
	container := &WapiContainer{}

	err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).First(container).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return "", err
	} else if errors.Is(err, gorm.ErrRecordNotFound) || container.ContainerID == nil || (container.ContainerID != nil && *container.ContainerID == "deleted") {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			container = &WapiContainer{
				ProfileID:     w.Profile.ID,
				WaStatus:      "close",
				ContainerName: proto.String(strconv.Itoa(w.Profile.ID)),
			}
			err = w.db.Table("wapi_container").Create(container).Error
			if err != nil {
				return "", err
			}
		}

		w.ProxyNodes(w.Profile, container, true)
	}

	err = w.db.Table("wapi_payment").Where("profile_id = ? AND status = ?", w.Profile.ID, true).Order("expired_at desc").First(&lastPayment).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return "", err
	}

	var newExpiredAt time.Time

	if lastPayment != nil && lastPayment.ExpiredAt != nil {
		if lastPayment.ExpiredAt.After(currentTime) || lastPayment.ExpiredAt.Equal(currentTime) {
			newExpiredAt = lastPayment.ExpiredAt.AddDate(0, 0, dayCount)
		} else {
			newExpiredAt = currentTime.AddDate(0, 0, dayCount)
		}
	} else {
		newExpiredAt = currentTime.AddDate(0, 0, dayCount)
	}

	paymentID := uuid.New().String()

	newPayment := WapiPayment{
		Status:        true,
		ExpiredAt:     &newExpiredAt,
		CreatedAt:     currentTime,
		ProfileId:     w.Profile.ID,
		TariffId:      tariffID,
		PaymentId:     paymentID,
		IsAutoPayment: false,
		Hand:          false,
		UserID:        int(w.Profile.UserID),
		UUID:          w.Profile.UUID,
	}

	err = w.db.Table("wapi_payment").Create(&newPayment).Error
	if err != nil {
		return "", err
	}

	return paymentID, nil

}

// payment by expired date for sklad with truncation
func (w *Wapi) AddProfilePaymentByExiredDate(expires *time.Time, tariffID uint32) (string, error) {
	var lastPayment *WapiPayment
	location := time.FixedZone("GMT+3", 3*60*60)
	currentTime := time.Now().In(location)

	if expires == nil {
		return "", fmt.Errorf("no apropriate expiry time")
	}

	err := w.db.Table("wapi_payment").Where("profile_id = ? AND status = ?", w.Profile.ID, true).Order("expired_at desc").First(&lastPayment).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return "", err
	}

	paymentID := uuid.New().String()

	//utcTime := expires.UTC()
	*expires = expires.Truncate(24 * time.Hour).Add(24 * time.Hour)

	newPayment := WapiPayment{
		Status:        true,
		ExpiredAt:     expires,
		CreatedAt:     currentTime,
		ProfileId:     w.Profile.ID,
		TariffId:      tariffID,
		PaymentId:     paymentID,
		IsAutoPayment: false,
		Hand:          false,
		UserID:        int(w.Profile.UserID),
		UUID:          w.Profile.UUID,
		PaymentAt:     &currentTime,
	}

	err = w.db.Table("wapi_payment").Create(&newPayment).Error
	if err != nil {
		return "", err
	}

	return paymentID, nil
}

func (w *Wapi) UpdateProfilePaymentExpDate(expires *time.Time, paymentID string) error {
	var payment *WapiPayment
	err := w.db.Table("wapi_payment").Where("profile_id = ? AND payment_id = ?", w.Profile.ID, paymentID).Order("expired_at desc").First(&payment).Error
	if err != nil {
		return fmt.Errorf("can't update wapi payment for %v with paymentID %v to %v: %v", w.Profile.ID, paymentID, expires, err)
	}
	payment.ExpiredAt = expires
	err = w.db.Table("wapi_payment").Save(&payment).Error
	if err != nil {
		return fmt.Errorf("can't save updated payment : %v", err)
	}
	return nil
}

func (w *Wapi) CheckAndCreateContainer() error {
	container := &WapiContainer{}
	err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).First(container).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	} else if errors.Is(err, gorm.ErrRecordNotFound) || container.ContainerID == nil || (container.ContainerID != nil && *container.ContainerID == "deleted") {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			container = &WapiContainer{
				ProfileID:     w.Profile.ID,
				WaStatus:      "close",
				ContainerName: proto.String(strconv.Itoa(w.Profile.ID)),
			}
			err = w.db.Table("wapi_container").Create(container).Error
			if err != nil {
				return err
			}
		}
		w.ProxyNodes(w.Profile, container, true)
	}
	return nil
}
