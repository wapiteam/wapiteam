package wapi

// 11
func (w *Wapi) UpdateAnalType(analType *string) error {
	wapiProfile := WapiProfile{}
	if err := w.db.Table("wapi_profile").Where("uuid = ?", w.Profile.UUID).First(&wapiProfile).Error; err != nil {
		log.Errorf("WapiProfile get error: %s, wapiUUID: %d", err.Error, w.Profile.UUID)
		return err
	}
	wapiProfile.AnalType = analType
	return w.db.Save(wapiProfile).Error
}
