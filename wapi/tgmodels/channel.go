package tgmodels

import (
	"github.com/gotd/td/tg"
)

type CustomChannel struct {
	Creator             bool
	Left                bool
	Broadcast           bool
	Verified            bool
	Megagroup           bool
	Restricted          bool
	Signatures          bool
	Min                 bool
	Scam                bool
	HasLink             bool
	HasGeo              bool
	SlowmodeEnabled     bool
	CallActive          bool
	CallNotEmpty        bool
	Fake                bool
	Gigagroup           bool
	Noforwards          bool
	JoinToSend          bool
	JoinRequest         bool
	Forum               bool
	StoriesHidden       bool
	StoriesHiddenMin    bool
	StoriesUnavailable  bool
	ID                  int64
	AccessHash          int64
	Title               string
	Username            string
	Photo               *CustomChatPhoto
	Date                int
	RestrictionReason   []tg.RestrictionReason
	AdminRights         tg.ChatAdminRights
	BannedRights        tg.ChatBannedRights
	DefaultBannedRights tg.ChatBannedRights
	ParticipantsCount   int
	Usernames           []tg.Username
	StoriesMaxID        int
}

func (chn *CustomChannel) ConvertCustomChannelToTGChannel() *tg.Channel {
	if chn != nil {
		channel := &tg.Channel{
			Creator:             chn.Creator,
			Left:                chn.Left,
			Broadcast:           chn.Broadcast,
			Verified:            chn.Verified,
			Megagroup:           chn.Megagroup,
			Restricted:          chn.Restricted,
			Signatures:          chn.Signatures,
			Min:                 chn.Min,
			Scam:                chn.Scam,
			HasLink:             chn.HasLink,
			HasGeo:              chn.HasGeo,
			SlowmodeEnabled:     chn.SlowmodeEnabled,
			CallActive:          chn.CallActive,
			CallNotEmpty:        chn.CallNotEmpty,
			Fake:                chn.Fake,
			Gigagroup:           chn.Gigagroup,
			Noforwards:          chn.Noforwards,
			JoinToSend:          chn.JoinToSend,
			JoinRequest:         chn.JoinRequest,
			Forum:               chn.Forum,
			StoriesHidden:       chn.StoriesHidden,
			StoriesHiddenMin:    chn.StoriesHiddenMin,
			StoriesUnavailable:  chn.StoriesUnavailable,
			ID:                  chn.ID,
			AccessHash:          chn.AccessHash,
			Title:               chn.Title,
			Username:            chn.Username,
			Date:                chn.Date,
			RestrictionReason:   []tg.RestrictionReason{},
			AdminRights:         tg.ChatAdminRights{},
			BannedRights:        tg.ChatBannedRights{},
			DefaultBannedRights: tg.ChatBannedRights{},
			ParticipantsCount:   chn.ParticipantsCount,
			Usernames:           []tg.Username{},
			StoriesMaxID:        chn.StoriesMaxID,
		}
		if chn.Photo != nil {
			channel.Photo = &tg.ChatPhoto{
				HasVideo:      chn.Photo.HasVideo,
				PhotoID:       chn.Photo.PhotoID,
				StrippedThumb: chn.Photo.StrippedThumb,
				DCID:          chn.Photo.DCID,
			}
		} else {
			channel.Photo = &tg.ChatPhotoEmpty{}
		}
		return channel
	} else {
		return nil
	}
}
