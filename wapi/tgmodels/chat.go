package tgmodels

import "github.com/gotd/td/tg"

type CustomChat struct {
	Creator           bool
	Left              bool
	Deactivated       bool
	CallActive        bool
	CallNotEmpty      bool
	Noforwards        bool
	ID                int64
	Title             string
	Photo             *CustomChatPhoto
	ParticipantsCount int
	Date              int
	Version           int
}

type CustomChatPhoto struct {
	HasVideo      bool
	PhotoID       int64
	StrippedThumb []byte
	DCID          int
}

func (ch *CustomChat) ConvertCustomChatToTGChat() *tg.Chat {
	if ch != nil {
		chat := &tg.Chat{
			Creator:           ch.Creator,
			Left:              ch.Left,
			Deactivated:       ch.Deactivated,
			CallActive:        ch.CallActive,
			CallNotEmpty:      ch.CallNotEmpty,
			Noforwards:        ch.Noforwards,
			ID:                ch.ID,
			Title:             ch.Title,
			ParticipantsCount: ch.ParticipantsCount,
			Date:              ch.Date,
			Version:           ch.Version,
		}
		if ch.Photo != nil {
			chat.Photo = &tg.ChatPhoto{
				HasVideo:      ch.Photo.HasVideo,
				PhotoID:       ch.Photo.PhotoID,
				StrippedThumb: ch.Photo.StrippedThumb,
				DCID:          ch.Photo.DCID,
			}
		} else {
			chat.Photo = &tg.ChatPhotoEmpty{}
		}
		return chat
	} else {
		return nil
	}
}
