package tgmodels

import (
	"github.com/gotd/td/tg"
)

type CustomUser struct {
	Self                 bool
	Contact              bool
	MutualContact        bool
	Deleted              bool
	Bot                  bool
	BotChatHistory       bool
	BotNochats           bool
	Verified             bool
	Restricted           bool
	Min                  bool
	BotInlineGeo         bool
	Support              bool
	Scam                 bool
	ApplyMinPhoto        bool
	Fake                 bool
	BotAttachMenu        bool
	Premium              bool
	AttachMenuEnabled    bool
	BotCanEdit           bool
	CloseFriend          bool
	StoriesHidden        bool
	StoriesUnavailable   bool
	ID                   int64
	AccessHash           int64
	FirstName            string
	LastName             string
	Username             string
	Phone                string
	Photo                *CustomUserProfilePhoto
	Status               interface{}
	BotInfoVersion       int
	BotInlinePlaceholder string
	LangCode             string
	Usernames            []CustomUsername
	StoriesMaxID         int
}

type CustomUsername struct {
	Editable bool
	Active   bool
	Username string
}

type CustomUserProfilePhoto struct {
	HasVideo      bool
	Personal      bool
	PhotoID       int64
	StrippedThumb []byte
	DCID          int
}

func (cu *CustomUser) ConvertCustomUserToTGUser() *tg.User {
	if cu != nil {
		user := &tg.User{
			Self:               cu.Self,
			Contact:            cu.Contact,
			MutualContact:      cu.MutualContact,
			Deleted:            cu.Deleted,
			Bot:                cu.Bot,
			BotChatHistory:     cu.BotChatHistory,
			BotNochats:         cu.BotNochats,
			Verified:           cu.Verified,
			Restricted:         cu.Restricted,
			Min:                cu.Min,
			BotInlineGeo:       cu.BotInlineGeo,
			Support:            cu.Support,
			Scam:               cu.Scam,
			ApplyMinPhoto:      cu.ApplyMinPhoto,
			Fake:               cu.Fake,
			BotAttachMenu:      cu.BotAttachMenu,
			Premium:            cu.Premium,
			AttachMenuEnabled:  cu.AttachMenuEnabled,
			BotCanEdit:         cu.BotCanEdit,
			CloseFriend:        cu.CloseFriend,
			StoriesHidden:      cu.StoriesHidden,
			StoriesUnavailable: cu.StoriesUnavailable,
			ID:                 cu.ID,
			AccessHash:         cu.AccessHash,
			FirstName:          cu.FirstName,
			LastName:           cu.LastName,
			Username:           cu.Username,
			Phone:              cu.Phone,

			BotInfoVersion:       cu.BotInfoVersion,
			BotInlinePlaceholder: cu.BotInlinePlaceholder,
			LangCode:             cu.LangCode,
			StoriesMaxID:         cu.StoriesMaxID,
		}
		if cu.Photo != nil {
			user.Photo = &tg.UserProfilePhoto{
				HasVideo:      cu.Photo.HasVideo,
				Personal:      cu.Photo.Personal,
				PhotoID:       cu.Photo.PhotoID,
				StrippedThumb: cu.Photo.StrippedThumb,
				DCID:          cu.Photo.DCID,
			}
		} else {
			user.Photo = &tg.UserProfilePhotoEmpty{}
		}

		return user
	} else {
		return nil
	}
}
