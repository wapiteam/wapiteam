package wapi

import "time"

type WapiMessage struct {
	ID              string              `json:"id"`
	Type            string              `json:"type"`
	From            string              `json:"from"`
	To              string              `json:"to"`
	FromMe          bool                `json:"fromMe"`
	SenderName      string              `json:"senderName"`
	ContactName     string              `json:"contact_name"`
	Username        string              `json:"username"`
	Phone           string              `json:"phone"`
	Time            int64               `json:"time"`
	Body            any                 `json:"body"`
	StanzaId        string              `json:"stanzaId"` // ReplyMessage ForwardedMessage PollMessage parent!!!
	ChatId          string              `json:"chatId"`
	IsForwarded     bool                `json:"isForwarded"`
	IsReply         bool                `json:"isReply"`
	Caption         string              `json:"caption"`
	FileName        string              `json:"file_name"`
	IsRead          bool                `json:"isRead" bson:"isRead"`
	DeliveryStatus  string              `json:"delivery_status" bson:"delivery_status"`
	S3Info          S3Info              `json:"s3Info"`
	PollVotes       map[string]PollVote `json:"poll_votes" bson:"poll_votes"`
	PollOptions     []string            `json:"poll_options,omitempty"`
	PollSelectCount uint32              `json:"poll_select_count,omitempty"`
	Mimetype        string              `json:"mimetype"`
	PTS             int                 `json:"pts,omitempty"`
	FromWhere       string              `json:"from_where"`
	ForwardMessage  *ReplyMessage       `json:"forward_message,omitempty"`
	ReplyMessage    *ReplyMessage       `json:"reply_message,omitempty"`
	IsEdited        bool                `json:"isEdited"`
	IsFromAPI       bool                `json:"isFromAPI"`
	IsDeleted       bool                `json:"isDeleted"`
	Location        *Location           `json:"location"`
	IsPinned        bool                `json:"isPinned"`
	Reactions       []Reaction          `json:"reactions"`
	Attachments     *[]Attachment       `json:"attachments,omitempty"`
}
type Attachment struct {
	Type       string `json:"type" bson:"type"`
	Date       int64  `json:"date" bson:"date"`
	Ext        string `json:"ext" bson:"ext"`
	ID         string `json:"id" bson:"id"`
	IsLicensed bool   `json:"isLicensed" bson:"isLicensed"`
	OwnerID    string `json:"ownerId" bson:"ownerId"`
	Size       int64  `json:"size" bson:"size"`
	Title      string `json:"title" bson:"title"`
	TypeCode   int    `json:"typeCode" bson:"typeCode"`
	Url        string `json:"url" bson:"url"`
}

type S3Info struct {
	Url             string     `json:"url,omitempty" bson:"url"`
	Expire          *time.Time `json:"expire,omitempty" bson:"expire"`
	ExpireTimestamp int64      `json:"expire_timestamp,omitempty" bson:"expire_timestamp"`
}

type PollVote struct {
	Vote      []string   `json:"votes,omitempty" bson:"votes"`
	Timestamp *time.Time `json:"timestamp,omitempty" bson:"timestamp"`
}

type Reaction struct {
	Reaction    string `json:"reaction"`
	Count       int    `json:"count"`
	UserID      string `json:"user_id"`
	ContactName string `json:"contact_name"`
}
