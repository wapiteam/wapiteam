package wapi

import "github.com/lib/pq"

type ExcludeContacts struct {
	ProfileID int            `json:"profile_id"`
	Contacts  pq.StringArray `json:"contacts,omitempty" gorm:"type:varchar(180)[]"`
}

func (e *ExcludeContacts) IsExclude(id string) bool {
	if len(e.Contacts) == 0 {
		return false
	}

	if ArrContains(e.Contacts, id) {
		return true
	}
	return false
}

func (e *ExcludeContacts) IsExcludeWithRemovePlusMinusAndSpaces(id string) bool {
	if len(e.Contacts) == 0 {
		return false
	}

	if ArrContainsWithRemovePlusMinusAndSpaces(e.Contacts, id) {
		return true
	}
	return false
}
