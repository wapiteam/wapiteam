package wapi

import (
	"context"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

// WapiBackUp итнрефейс
type WapiBackUp interface {
	GetBackUp(ctx context.Context, backUpName string, bucket string) ([]byte, error)
	PutBackUp(ctx context.Context, backUpName string, data []byte, bucket string) error
	DeleteBackUp(ctx context.Context, backUpName string, bucket string) error
	IsBackUpExist(ctx context.Context, backUpName string, bucket string) (bool, error)
	PutBackUp7DaysHWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error)
	PutBackUpBitrix24HWithLink(ctx context.Context, backUpName string, data []byte) (string, error)
	Put7DayLinkFilename(ctx context.Context, evt *IncomingOutgoingMessage, file []byte, allWapi *Wapi) (string, string, error)
	PutAudioConverter(ctx context.Context, file []byte) (string, string, error)
	PutBackUp180DaysWithLink(ctx context.Context, backUpName string, uuid string, data []byte) (string, error)
	Put2DayLinkFilename(ctx context.Context, evt *IncomingOutgoingMessage, file []byte, allWapi *Wapi) (string, string, error)
	Put3DayLinkFilename(ctx context.Context, fileName string, mime string, file []byte, allWapi *Wapi) (string, string, error)
}

// Hurricane итнрефейс
type Hurricane interface {

	/*
		Send - послать сообщение в хурикан

		доступные типы сообщений:

		wa_status_open

		wa_status_connecting

		wa_status_close

		update_auth_false

		update_auth_true

		need_restore_false

		need_restore_true

		send_qr_code

		restart_container

		retry_webhook

		stop_and_start_continer

		synced_false

		update_auth_false_without_docker

		synced_true
	*/
	Send(ctx context.Context, allWapi *Wapi, event string) error
}

// Response итнрефейс
type Response interface {
	SendResponse(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error
}

// // WapiResponse итнрефейс
// type AbstractResponse interface {
// 	SendWapiResponse(ctx context.Context, jmsg []byte, broker *wapirabbit.AmqpBroker, cmd *WapiCommand, taskQueue *string) error
// }

// Command итнрефейс
type Command interface {
	SendCommand(ctx context.Context, broker *wapirabbit.AmqpBroker, cmd *WapiCommand) error
}

// Container итнрефейс
type Container interface {
	Restart(ctx context.Context) error
	Stop(ctx context.Context) error
	Start(ctx context.Context) error
	DockerCreate(ctx context.Context, platform PlatformType, binds []string, image string) (string, error)
}

// Webhook итнрефейс
type Webhook interface {
	SendWebhook(ctx context.Context, allWapi *Wapi) error
}

type WapiResponder interface {
	GetWapiResponse() *WapiResponse
}
