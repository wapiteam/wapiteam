package wapi

import (
	"fmt"
	"time"
)

type PlatformType string

const (
	Whatsapp    PlatformType = "wz"
	Telegram    PlatformType = "tg"
	TelegramBot PlatformType = "tgbot"
)

type WapiConfig struct {
	Param      string
	IntValue   int64
	StrValue   string
	JsonValue  []byte
	ArrayValue []interface{}
}

type WapiNode struct {
	ID      uint32 `json:"id,omitempty"`
	Name    string `json:"name,omitempty"`
	MongoIp string `json:"mongo_ip,omitempty"`
	Tech    string `json:"tech,omitempty"`
}

func (WapiNode) TableName() string {
	return "wapi_node"
}

type WapiWh struct {
	Types         []string
	Url           *string
	Authorization *string
	Intervals     []time.Duration
	Webhook       Webhook
}

type WapiProxy struct {
	ID    uint   `json:"id"`
	Proxy string `json:"proxy"`
}

type WapiBackup struct {
	ID         int       `json:"id,omitempty"`
	ProfileID  int       `json:"profile_id,omitempty"`
	BackupedAt time.Time `json:"backuped_at,omitempty"`
	Path       string    `json:"path,omitempty"`
	BackupType string    `json:"backup_type,omitempty"`
}

type WapiUser struct {
	ID               int      `json:"id"`
	Debt             int      `json:"debt,omitempty"`
	PaySystem        string   `json:"pay_system"`
	IsSuperuser      bool     `json:"is_superuser,omitempty"`
	Tech             string   `json:"tech,omitempty"`
	Spamer           bool     `json:"spamer,omitempty"`
	TariffId         *uint32  `json:"tariff_id,omitempty"`
	WithPayments     bool     `json:"with_payments"`
	Email            string   `json:"email,omitempty"`
	RequestTimeout   *float64 `json:"request_timeout,omitempty"`
	S3Save           bool     `json:"s3_save,omitempty"`
	LimitNewProfiles *uint32  `json:"limit_new_profiles,omitempty"`
	RefID            *int     `json:"ref_id,omitempty"`
	RefCode          *string  `json:"ref_code,omitempty"`
	Balance          float64  `json:"balance,omitempty" gorm:"default:0"`
	Hello            bool     `json:"hello,omitempty" gorm:"default:true"`
	Moysklad         string   `json:"moysklad,omitempty"`
	MoyskladToken    string   `json:"moysklad_token,omitempty"`
	Password         string   `json:"password,omitempty"`
}

type WapiAuthtokenToken struct {
	Key    string
	UserID uint
}

type Contact struct {
	DisplayName string `json:"display_name,omitempty"`
	Phone       string `json:"phone,omitempty"`
	Vcard       string `json:"vcard,omitempty"`
}

func (c Contact) String() string {
	return fmt.Sprintf("Contact { DisplayName: %s, Phone: %s, Vcard: %s }", c.DisplayName, c.Phone, c.Vcard)
}

type Location struct {
	Latitude         float64 `json:"latitude"`
	Longitude        float64 `json:"longitude"`
	Description      string  `json:"description"`
	DegreesLatitude  float64 `json:"degreesLatitude"`
	DegreesLongitude float64 `json:"degreesLongitude"`
	Name             string  `json:"name"`
	JPEGThumbnail    *string `json:"jpegThumbnail,omitempty"`
}

func (l Location) String() string {
	var name, desc, thumbnail string

	name = l.Name
	desc = l.Description

	if l.JPEGThumbnail != nil {
		thumbnail = *l.JPEGThumbnail
	}
	return fmt.Sprintf(
		"Location { Name: %s, Latitude: %.6f, Longitude: %.6f, DegreesLatitude: %.6f, DegreesLongitude: %.6f, Description: %s, JPEGThumbnail: %s }",
		name, l.Latitude, l.Longitude, l.DegreesLatitude, l.DegreesLongitude, desc, thumbnail,
	)
}

type WapiTaskEvent struct {
	Event       string `json:"event"`
	ProfileUUID string `json:"profile_uuid"`
	MessageID   string `json:"message_id"`
	TaskID      string `json:"task_id"`
}

type MetaInformationFromSite struct {
	Title       string  `json:"title,omitempty"`
	Description string  `json:"description,omitempty"`
	Image       *string `json:"image,omitempty"`
	Link        string  `json:"link,omitempty"`
}
