package wapi

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/wapiteam/wapiteam/utmGRPC"
	"go.mau.fi/whatsmeow/types"
)

type IncomingOutgoingMessage struct {
	ID                  string        `json:"id"`
	ProfileId           string        `json:"profile_id"`
	WhType              string        `json:"wh_type"`
	Timestamp           time.Time     `json:"timestamp,omitempty"`
	Time                int64         `json:"time,omitempty"`
	Body                string        `json:"body"`
	Type                string        `json:"type"`
	From                string        `json:"from"`
	To                  string        `json:"to"`
	Star                string        `json:"star,omitempty"`
	SenderName          string        `json:"senderName"`
	ChatId              string        `json:"chatId"`
	Caption             *string       `protobuf:"bytes,3,opt,name=caption" json:"caption"`
	Title               *string       `json:"title,omitempty"`
	From_where          string        `json:"from_where"`
	Contact             *Contact      `json:"contact,omitempty"`
	Contacts            []Contact     `json:"contacts,omitempty"`
	FileName            string        `json:"file_name,omitempty"`
	Location            *Location     `json:"location,omitempty"`
	Mimetype            string        `json:"mimetype,omitempty"`
	ContactName         string        `json:"contact_name"`
	ContactPhone        string        `json:"contact_phone"`
	ContactUserName     string        `json:"contact_username"`
	Username            string        `json:"username,omitempty"`
	Phone               string        `json:"phone,omitempty"`
	ListResponse        bool          `json:"list_response,omitempty"`
	SelectedListId      *string       `json:"selected_list_id,omitempty"`
	IsForwarded         bool          `json:"is_forwarded"`
	IsReply             bool          `json:"isReply"`
	IsEdited            bool          `json:"is_edited"`
	StanzaId            string        `json:"stanza_id"`
	PollOptions         []string      `json:"poll_options,omitempty"`
	SelectedPollVariant []string      `json:"selected_poll_variant,omitempty"`
	IsMe                bool          `json:"is_me"`
	ChatType            string        `json:"chat_type,omitempty"`
	TaskID              string        `json:"task_id,omitempty"`
	Thumbnail           string        `json:"thumbnail"`
	Picture             string        `json:"picture"`
	WapiBotID           string        `json:"wappi_bot_id"`
	IsDeleted           bool          `json:"is_deleted"`
	IsBot               bool          `json:"is_bot"`
	ReplyMessage        *ReplyMessage `json:"reply_message,omitempty"`
	ForwardMessage      *ReplyMessage `json:"forward_message,omitempty"`
	UTM                 *utmGRPC.UTM  `json:"utm,omitempty"`
	Order               *Order        `json:"order,omitempty"`
	LengthSeconds       int           `json:"length_seconds,omitempty"`
	FileLink            string        `json:"file_link,omitempty"`
	FlileLinkExpire     int64         `json:"file_link_expire,omitempty"`
}

type Order struct {
	OrderStatus  string `json:"order_status"`
	OrderID      string `json:"order_id"`
	TotalAmount  int    `json:"total_amount"`
	Thumbnail    string `json:"thumbnail"`
	TtemCount    int    `json:"item_count"`
	OrderTitle   string `json:"order_title"`
	CurrencyCode string `json:"currency_code"`
}

type ReplyMessage struct {
	ID              string `json:"id"`
	Body            string `json:"body"`
	Type            string `json:"type"`
	ChatId          string `json:"chatId"`
	Timestamp       int64  `json:"timestamp"`
	Caption         string `protobuf:"bytes,3,opt,name=caption" json:"caption"`
	FileName        string `json:"file_name,omitempty"`
	Mimetype        string `json:"mimetype,omitempty"`
	ContactName     string `json:"contact_name"`
	Username        string `json:"username,omitempty"`
	Phone           string `json:"phone,omitempty"`
	FlileLinkExpire int64  `json:"file_link_expire,omitempty"`
	ChatType        string `json:"chat_type,omitempty"`
	Seconds         int    `json:"seconds,omitempty"`
}

type CallHookWH struct {
	WhType    string            `json:"wh_type,omitempty"`
	Type      string            `json:"type,omitempty"`
	ProfileId string            `json:"profile_id,omitempty"`
	Number    string            `json:"number,omitempty"`
	Timestamp time.Time         `json:"timestamp,omitempty"`
	Time      int64             `json:"time,omitempty"`
	Contact   types.ContactInfo `json:"contact"`
}

type PresenceUpdates struct {
	WhType    string     `json:"wh_type,omitempty"`
	ProfileId string     `json:"profile_id,omitempty"`
	Number    string     `json:"number,omitempty"`
	Status    string     `json:"status,omitempty"`
	LastSeen  *time.Time `json:"last_seen,omitempty"`
	Timestamp time.Time  `json:"timestamp,omitempty"`
	Time      int64      `json:"time,omitempty"`
}

type AckWH struct {
	ID                 string    `json:"id"`
	ProfileId          string    `json:"profile_id"`
	WhType             string    `json:"wh_type"`
	Timestamp          time.Time `json:"timestamp,omitempty"`
	Time               int64     `json:"time,omitempty"`
	Type               string    `json:"type,omitempty"`
	From               string    `json:"from,omitempty"`
	To                 string    `json:"to,omitempty"`
	ChatId             string    `json:"chat_id,omitempty"`
	IdGroupParticipant string    `json:"id_group_participant,omitempty"`
	Status             string    `json:"status,omitempty"`
	TaskID             string    `json:"task_id,omitempty"`
	Detail             string    `json:"detail,omitempty"`
	Phone              string    `json:"phone,omitempty"`
	UserName           string    `json:"user_name,omitempty"`
}

type JoinLeaveGroupWH struct {
	WhType    string `json:"wh_type,omitempty"`
	ProfileId string `json:"profile_id,omitempty"`
	Type      string `json:"type,omitempty"`
	// Group     *types.GroupInfo `json:"group,omitempty"`
	// Join      []types.JID      `json:"join,omitempty"`
	// Leave     []types.JID      `json:"leave,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
	Time      int64     `json:"time,omitempty"`
}

type AuthStatusWH struct {
	WhType    string    `json:"wh_type,omitempty"`
	ProfileId string    `json:"profile_id,omitempty"`
	Status    string    `json:"status,omitempty"`
	Reason    string    `json:"reason,omitempty"`
	Details   string    `json:"details,omitempty"`
	Phone     string    `json:"phone,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
	Time      int64     `json:"time,omitempty"`
}

type WarningWH struct {
	WhType    string    `json:"wh_type,omitempty"`
	ProfileId string    `json:"profile_id,omitempty"`
	Message   string    `json:"reason,omitempty"`
	TaskID    string    `json:"task_id,omitempty"`
	Phone     string    `json:"phone,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
	Time      int64     `json:"time,omitempty"`
}

type AppStatusWH struct {
	WhType    string    `json:"wh_type,omitempty"`
	ProfileId string    `json:"profile_id,omitempty"`
	Status    string    `json:"status,omitempty"`
	Phone     string    `json:"phone,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
	Time      int64     `json:"time,omitempty"`
}

func SendWhWithRetry(msg []byte, retryIntervals []time.Duration, url string, webhookAuth *string) (int, error) {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	http_cli := &http.Client{Timeout: 90 * time.Second, Transport: transport}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(msg))
	if err != nil {
		return 0, err
	}

	req.Header.Set("User-Agent", "WappiWH")
	req.Header.Set("Content-Type", "application/json")

	if webhookAuth != nil {
		req.Header.Set("Authorization", *webhookAuth)
	}

	for _, interval := range retryIntervals {
		resp, err := http_cli.Do(req)
		if err != nil {
			log.Errorf("server return error %s, sending for retry", err.Error())
			time.Sleep(interval)
			continue
		}

		if resp.StatusCode != http.StatusOK {
			log.Errorf("server return StatusCode %d, sending for retry", resp.StatusCode)
			time.Sleep(interval)
			continue
		}

		return resp.StatusCode, nil
	}

	return 0, fmt.Errorf("used all attempts, sending canceled")
}

func (m *IncomingOutgoingMessage) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}

	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	// if !SQLARRContains(allWapi.Profile.WebhookTypes, "incoming_message") {
	// 	return fmt.Errorf("has not incoming_message wh type in profile")
	// }

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{10 * time.Second, 60 * time.Second, 180 * time.Second}
	}

	var data2 []*IncomingOutgoingMessage
	m.ProfileId = allWapi.Profile.UUID
	if !m.IsMe {
		if SQLARRContains(allWapi.Profile.WebhookTypes, "incoming_message") {
			if m.Type != "chat" && isWazapa {
				return fmt.Errorf("this is wazapa not sending")
			}
			m.WhType = "incoming_message"
			data2 = append(data2, m)
			messages := make(map[string]interface{})
			messages["messages"] = &data2
			msg, err := json.Marshal(messages)
			if err != nil {
				log.Errorf("json marshal errr: %s", err.Error())
				return fmt.Errorf("json marshal errr: %s", err.Error())
			}
			log.Resultf("sending a webhook, type: %s, ID: %s, TaskID: %s", m.WhType, m.ID, m.TaskID)
			go func() {
				code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
				if err != nil {
					log.Errorf("sending webhook error: %s, type: %s, ID: %s, TaskID: %s", err.Error(), m.WhType, m.ID, m.TaskID)
					return
				}
				log.Resultf("sending webhook success, type: %s, ID: %s, code: %d, TaskID: %s", m.WhType, m.ID, code, m.TaskID)
			}()

		}
		return nil
	} else {
		if SQLARRContains(allWapi.Profile.WebhookTypes, "outgoing_message_phone") && m.From_where == "phone" {
			if isWazapa {
				return fmt.Errorf("this is wazapa not sending")
			}
			m.WhType = "outgoing_message_phone"
			messages := make(map[string]interface{})
			data2 = append(data2, m)
			messages["messages"] = &data2
			msg, err := json.Marshal(messages)
			if err != nil {
				log.Errorf("json marshal errr: %s", err.Error())
				return fmt.Errorf("json marshal errr: %s", err.Error())
			}

			log.Resultf("sending a webhook, type: %s, ID: %s, TaskID: %s", m.WhType, m.ID, m.TaskID)
			go func() {
				code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
				if err != nil {
					log.Errorf("sending webhook error: %s, type: %s, ID: %s, TaskID: %s", err.Error(), m.WhType, m.ID, m.TaskID)
					return
				}
				log.Resultf("sending webhook success, type: %s, ID: %s, code: %d, TaskID: %s", m.WhType, m.ID, code, m.TaskID)
			}()

		} else if SQLARRContains(allWapi.Profile.WebhookTypes, "outgoing_message_api") && m.From_where == "api" {
			if isWazapa {
				return fmt.Errorf("this is wazapa not sending")
			}
			m.WhType = "outgoing_message_api"
			messages := make(map[string]interface{})
			data2 = append(data2, m)
			messages["messages"] = &data2
			msg, err := json.Marshal(messages)
			if err != nil {
				log.Errorf("json marshal errr: %s", err.Error())
				return fmt.Errorf("json marshal errr: %s", err.Error())
			}

			log.Resultf("sending a webhook, type: %s, ID: %s, TaskID: %s", m.WhType, m.ID, m.TaskID)
			go func() {
				code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
				if err != nil {
					log.Errorf("sending webhook error: %s, type: %s, ID: %s, TaskID: %s", err.Error(), m.WhType, m.ID, m.TaskID)
					return
				}
				log.Resultf("sending webhook success, type: %s, ID: %s, code: %d, TaskID: %s", m.WhType, m.ID, code, m.TaskID)
			}()

		}
		return nil
	}
}

func (m *AuthStatusWH) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}

	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	if isWazapa || !SQLARRContains(allWapi.Profile.WebhookTypes, "authorization_status") {
		return fmt.Errorf("has not authorization_status wh type in profile")
	}

	if m.Reason == "" {
		m.Reason = "logged out from another device"
	}

	if m.Status == "online" {
		m.Reason = ""
	}

	var data2 []*AuthStatusWH
	m.ProfileId = allWapi.Profile.UUID
	m.WhType = "authorization_status"
	m.Phone = allWapi.Profile.Phone
	m.Timestamp = time.Now()
	m.Time = time.Now().Unix()
	data2 = append(data2, m)
	messages := make(map[string]interface{})
	messages["messages"] = data2
	msg, err := json.Marshal(messages)
	if err != nil {
		log.Errorf("json marshal errr: %s", err.Error())
		return fmt.Errorf("json marshal errr: %s", err.Error())
	}

	wh_id := uuid.NewString()[:7]

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{1 * time.Second, 10 * time.Second, 30 * time.Second, 60 * time.Second, 120 * time.Second, 180 * time.Second}
	}

	log.Resultf("sending a webhook, type: %s, ID: %s", m.WhType, wh_id)

	go func() {
		code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
		if err != nil {
			log.Errorf("sending webhook error: %s, type: %s, ID: %s", err.Error(), m.WhType, wh_id)
			return
		}
		log.Resultf("sending webhook success, type: %s, ID: %s, code: %d", m.WhType, wh_id, code)
	}()

	return nil
}

func (m *AppStatusWH) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}

	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	if isWazapa || !SQLARRContains(allWapi.Profile.WebhookTypes, "application_status") {
		return fmt.Errorf("has not application_status wh type in profile")
	}

	var data2 []*AppStatusWH
	m.ProfileId = allWapi.Profile.UUID
	m.WhType = "application_status"
	m.Phone = allWapi.Profile.Phone
	m.Timestamp = time.Now()
	m.Time = time.Now().Unix()
	data2 = append(data2, m)
	messages := make(map[string]interface{})
	messages["messages"] = data2
	msg, err := json.Marshal(messages)
	if err != nil {
		log.Errorf("json marshal errr: %s", err.Error())
		return fmt.Errorf("json marshal errr: %s", err.Error())
	}

	wh_id := uuid.NewString()[:7]

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{1 * time.Second, 10 * time.Second, 30 * time.Second, 60 * time.Second, 120 * time.Second, 180 * time.Second}
	}

	log.Resultf("sending a webhook, type: %s, ID: %s", m.WhType, wh_id)

	go func() {
		code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
		if err != nil {
			log.Errorf("sending webhook error: %s, type: %s, ID: %s", err.Error(), m.WhType, wh_id)
			return
		}
		log.Resultf("sending webhook success, type: %s, ID: %s, code: %d", m.WhType, wh_id, code)
	}()

	return nil
}

func (m *AckWH) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}

	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	if isWazapa || !SQLARRContains(allWapi.Profile.WebhookTypes, "delivery_status") {
		return fmt.Errorf("has not delivery_status wh type in profile")
	}
	m.ProfileId = allWapi.Profile.UUID
	m.WhType = "delivery_status"
	messages := make(map[string]interface{})
	messages["messages"] = &m
	msg, err := json.Marshal(messages)
	if err != nil {
		log.Errorf("json marshal errr: %s", err.Error())
		return fmt.Errorf("json marshal errr: %s", err.Error())
	}

	wh_id := uuid.NewString()[:7]

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{1 * time.Second, 10 * time.Second, 30 * time.Second, 60 * time.Second, 120 * time.Second, 180 * time.Second}
	}

	log.Resultf("sending a webhook, type: %s, ID: %s", m.WhType, wh_id)

	go func() {
		code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
		if err != nil {
			log.Errorf("sending webhook error: %s, type: %s, ID: %s", err.Error(), m.WhType, wh_id)
			return
		}
		log.Resultf("sending webhook success, type: %s, ID: %s, code: %d", m.WhType, wh_id, code)
	}()

	return nil

}

func (m *WarningWH) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}

	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	if isWazapa || !SQLARRContains(allWapi.Profile.WebhookTypes, "delivery_status") {
		return fmt.Errorf("has not delivery_status wh type in profile")
	}
	m.ProfileId = allWapi.Profile.UUID
	m.WhType = "warning"
	messages := make(map[string]interface{})
	messages["messages"] = &m
	msg, err := json.Marshal(messages)
	if err != nil {
		log.Errorf("json marshal errr: %s", err.Error())
		return fmt.Errorf("json marshal errr: %s", err.Error())
	}

	wh_id := uuid.NewString()[:7]

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{1 * time.Second, 10 * time.Second, 30 * time.Second, 60 * time.Second, 120 * time.Second, 180 * time.Second}
	}

	log.Resultf("sending a webhook, type: %s, ID: %s", m.WhType, wh_id)

	go func() {
		code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
		if err != nil {
			log.Errorf("sending webhook error: %s, type: %s, ID: %s", err.Error(), m.WhType, wh_id)
			return
		}
		log.Resultf("sending webhook success, type: %s, ID: %s, code: %d", m.WhType, wh_id, code)
	}()

	return nil

}

func (m *CallHookWH) SendWebhook(ctx context.Context, allWapi *Wapi) error {

	if allWapi == nil {
		return fmt.Errorf("allWapi is nil")
	}

	if allWapi.Profile.WebhookUrl == nil || *allWapi.Profile.WebhookUrl == "" {
		return fmt.Errorf("webhook url is nil")
	}
	isWazapa := strings.Contains(*allWapi.Profile.WebhookUrl, "k.wazapa")
	if isWazapa || !SQLARRContains(allWapi.Profile.WebhookTypes, "incoming_call") {
		return fmt.Errorf("has not incoming_call wh type in profile")
	}
	m.ProfileId = allWapi.Profile.UUID
	m.WhType = "incoming_call"
	messages := make(map[string]interface{})
	messages["messages"] = &m
	msg, err := json.Marshal(messages)
	if err != nil {
		log.Errorf("json marshal errr: %s", err.Error())
		return fmt.Errorf("json marshal errr: %s", err.Error())
	}

	wh_id := uuid.NewString()[:7]

	var intevals []time.Duration
	if allWapi.User.WithPayments {
		intevals = []time.Duration{1 * time.Second, 30 * time.Second, 60 * time.Second}
	} else {
		intevals = []time.Duration{1 * time.Second, 10 * time.Second, 30 * time.Second, 60 * time.Second, 120 * time.Second, 180 * time.Second}
	}

	log.Resultf("sending a webhook, type: %s, ID: %s", m.WhType, wh_id)

	go func() {
		code, err := SendWhWithRetry(msg, intevals, *allWapi.Profile.WebhookUrl, allWapi.Profile.WebhookAuth)
		if err != nil {
			log.Errorf("sending webhook error: %s, type: %s, ID: %s", err.Error(), m.WhType, wh_id)
			return
		}
		log.Resultf("sending webhook success, type: %s, ID: %s, code: %d", m.WhType, wh_id, code)
	}()

	return nil

}

func (m *IncomingOutgoingMessage) AssertInWapiMessage() *WapiMessage {

	if m.Type == "image" || m.Type == "ptt" || m.Type == "video" || m.Type == "document" || m.Type == "audio" {
		m.Body = ""
	}

	wm := &WapiMessage{
		ID:           m.ID,
		Time:         m.Time,
		Body:         m.Body,
		Type:         m.Type,
		From:         m.From,
		To:           m.To,
		SenderName:   m.SenderName,
		ChatId:       m.ChatId,
		FileName:     m.FileName,
		Mimetype:     m.Mimetype,
		ContactName:  m.ContactName,
		Username:     m.Username,
		Phone:        m.Phone,
		IsForwarded:  m.IsForwarded,
		IsReply:      m.IsReply,
		IsEdited:     m.IsEdited,
		StanzaId:     m.StanzaId,
		PollOptions:  m.PollOptions,
		IsDeleted:    m.IsDeleted,
		ReplyMessage: m.ReplyMessage,
		FromMe:       m.IsMe,
		S3Info: S3Info{
			Url:             m.FileLink,
			ExpireTimestamp: m.FlileLinkExpire,
		},
		FromWhere: m.From_where,
	}

	if m.Location != nil {
		wm.Location = m.Location
		wm.Body = m.Location
	}

	if m.Contact != nil {
		wm.Body = m.Contact
	}

	if m.Caption != nil {
		wm.Caption = *m.Caption
	}

	if m.From_where == "api" {
		wm.IsFromAPI = true
	}

	if m.FlileLinkExpire != 0 {
		exp := time.Unix(m.FlileLinkExpire, 0)
		wm.S3Info.Expire = &exp
	}

	return wm
}
