package wamodels

type ContactInfo struct {
	Status      string    `json:"status,omitempty"`
	Timestamp   int64     `json:"timestamp,omitempty"`
	Time        string    `json:"time,omitempty"`
	WAProfile   WAProfile `json:"profile,omitempty"`
	TGProfile   TGProfile `json:"contact,omitempty"`
	UUID        string    `json:"uuid,omitempty"`
	ChannelType string
}

type TGProfile struct {
	ID        string `json:"id,omitempty"`
	Type      string `json:"type,omitempty"`
	Number    string `json:"number,omitempty"`
	PushName  string `json:"pushname,omitempty"`
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Picture   string `json:"picture,omitempty"`
	Thumbnail string `json:"thumbnail,omitempty"`
	Username  string `json:"username,omitempty"`
}

type WAProfile struct {
	IsOnWhatsapp bool      `json:"is_on_whatsapp,omitempty"`
	ID           string    `json:"id,omitempty"`
	User         WAUser    `json:"user,omitempty"`
	Contact      WAContact `json:"contact,omitempty"`
	Image        string    `json:"image,omitempty"`
	Thumbnail    string    `json:"thumbnail,omitempty"`
}

type WAUser struct {
	Details map[string]WAUserDetails `json:"user,omitempty"`
}

type WAUserDetails struct {
	VerifiedName *string  `json:"VerifiedName,omitempty"`
	Status       string   `json:"Status,omitempty"`
	PictureID    string   `json:"PictureID,omitempty"`
	Devices      []string `json:"Devices,omitempty"`
}

type WAContact struct {
	Found        bool   `json:"Found,omitempty"`
	FirstName    string `json:"FirstName,omitempty"`
	FullName     string `json:"FullName,omitempty"`
	PushName     string `json:"PushName,omitempty"`
	BusinessName string `json:"BusinessName,omitempty"`
}
