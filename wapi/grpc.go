package wapi

import (
	"context"

	"gitlab.com/wapiteam/wapiteam/utmGRPC"
	"gitlab.com/wapiteam/wapiteam/wapiGRPC"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// whatsapp
func (w *Wapi) MessagesGet(ctx context.Context, chatId, order string, limit, offset int32, time *int64, grpcHost string) (*wapiGRPC.ChatMessagesResponse, error) {

	conn, err := grpc.NewClient(grpcHost, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Errorf("fail to dial: %v", err)
		return nil, err
	}
	defer conn.Close()
	client := wapiGRPC.NewWapiGRPCClient(conn)

	chatMessages, err := client.GetMessagesFromChat(ctx, &wapiGRPC.GetMessagesRequest{
		ChatId:    chatId,
		ProfileId: w.Profile.UUID,
		Token:     w.Token.Key,
		Limit:     limit,
		Order:     order,
		Offset:    offset,
		Time:      time,
	})

	if err != nil {
		log.Errorf("fail to get messages: %v", err)
		return nil, err
	}
	return chatMessages, nil
}

func (w *Wapi) UTMGet(ctx context.Context, grpcHost string, utmID string) (*utmGRPC.UTMResponse, error) {

	conn, err := grpc.NewClient(grpcHost, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Errorf("fail to dial: %v", err)
		return nil, err
	}
	defer conn.Close()
	client := utmGRPC.NewUTMServiceClient(conn)

	utmResult, err := client.GetUTM(ctx, &utmGRPC.UTMRequest{
		Id: utmID,
	})

	if err != nil {
		log.Errorf("fail to get messages: %v", err)
		return nil, err
	}
	return utmResult, nil
}
