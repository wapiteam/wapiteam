package wapi

type WapiTariff struct {
	ID          int    `json:"id"`
	Discount    int    `json:"discount,omitempty"`
	Month       int    `json:"month,omitempty"`
	Price       int    `json:"price,omitempty"`
	PromoPeriod int    `json:"promo_period,omitempty"`
	Comments    string `json:"comments,omitempty"`
}

func (WapiTariff) TableName() string {
	return "wapi_tariff"
}

func (w *Wapi) GetTariff(id int) (*WapiTariff, error) {
	tariff := &WapiTariff{}
	if err := w.db.Where("id = ?", id).First(tariff).Error; err != nil {
		return nil, err
	}
	return tariff, nil
}
