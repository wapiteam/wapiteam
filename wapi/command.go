package wapi

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type WapiCommand struct {
	TaskID          string              `json:"task_id"`
	CommandStart    time.Time           `json:"command_start"`
	Command         string              `json:"command,omitempty"`
	Recipient       string              `json:"recipient,omitempty"`
	UUID            string              `json:"uuid,omitempty"`
	Body            string              `json:"body,omitempty"`
	ImageFormat     string              `json:"image_format,omitempty"`
	Mime            string              `json:"mime,omitempty"`
	Caption         string              `json:"caption,omitempty"`
	B64File         []byte              `json:"b64_file,omitempty"`
	FileName        string              `json:"file_name,omitempty"`
	Latitude        float64             `json:"latitude,omitempty"`
	Longitude       float64             `json:"longitude,omitempty"`
	Address         string              `json:"address,omitempty"`
	Phone           string              `json:"phone,omitempty"`
	StanzaId        string              `json:"stanza_id,omitempty"`
	GroupID         string              `json:"group_id,omitempty"`
	Participants    []string            `json:"participants,omitempty"`
	WhTypes         []string            `json:"wh_types,omitempty"`
	Limit           int64               `json:"limit,omitempty"`
	Offset          int64               `json:"offset,omitempty"`
	Date            *time.Time          `json:"date,omitempty"`
	ShowAll         bool                `json:"show_all,omitempty"`
	Title           string              `json:"title,omitempty"`
	Response        *WapiStringResponse `json:"response"`
	MassPostingID   *string             `json:"mass_posting_id,omitempty"`
	PollOptions     []string            `json:"poll_options,omitempty"`
	PollSelectCount uint32              `json:"poll_select_count,omitempty"`
	WebhookAuth     *string             `json:"webhook_auth,omitempty"`
	WebhookUrl      *string             `json:"webhook_url,omitempty"`
	MarkAll         bool                `json:"mark_all,omitempty"`
	Order           string              `json:"order,omitempty"`
	Url             string              `json:"url,omitempty"`
	Filter          Filter              `json:"filter,omitempty"`
	WappiBotID      string              `json:"wappi_bot_id,omitempty"`
	Source          string              `json:"source,omitempty"`

	// not gowapi
	IsSpam      bool `json:"is_spam,omitempty"`
	TimeoutFrom int  `json:"timeout_from,omitempty"`
	TimeoutTo   int  `json:"timeout_to,omitempty"`
}

type Filter struct {
	IDs []string `json:"ids,omitempty"`
}

func (wr *WapiCommand) SendCommand(ctx context.Context, broker *wapirabbit.AmqpBroker) error {
	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("SendWapiCommand Marshal error %s", err.Error())
		return err
	}
	if err := broker.Publish(ctx, wr.UUID, wr.UUID+"_"+wr.Command, jmsg); err != nil {
		log.Errorf("SendWapiCommand Publish error %s", err.Error())
		return err
	}

	log.Resultf("SendWapiCommand to wapi succes {command: %s}	{profile.uuid: %s}", wr.Command, wr.UUID)
	return nil
}

func (wr *WapiCommand) SendCommandRout(ctx context.Context, broker *wapirabbit.AmqpBroker, pubRoutingKey string) error {
	jmsg, err := json.Marshal(wr)
	if err != nil {
		log.Errorf("SendWapiCommand Marshal error %s", err.Error())
		return err
	}
	if err := broker.Publish(ctx, wr.UUID, pubRoutingKey, jmsg); err != nil {
		log.Errorf("SendWapiCommand Publish error %s", err.Error())
		return err
	}

	log.Resultf("SendWapiCommand to wapi succes {command: %s}	{profile.uuid: %s}", wr.Command, wr.UUID)
	return nil
}
