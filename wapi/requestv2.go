package wapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

// func (w *Wapi) SendFileUrlV2(recipinet, caption string, itemList *[]URLAtatchment, intervals int) (*WapiStringResponse, error) {
// 	var globalErr error
// 	jsonMSG := &JsonBody{
// 		Recipient: recipinet,
// 		Caption:   caption,
// 	}
// 	if itemList != nil {
// 		jsonMSG.URLAttachments = itemList
// 	}
// 	msg, err := json.Marshal(jsonMSG)
// 	if err != nil {
// 		return nil, fmt.Errorf("не сформировал реквест: %v", err)
// 	}

// 	var method = "/sync/message/file/url/send"
// 	var platform string

// 	if w.Profile.Platform == "tg" {
// 		platform = "tapi"
// 	} else if w.Profile.Platform == "wz" {
// 		platform = "api"
// 	} else if w.Profile.Platform == "av" {
// 		platform = "avito"
// 	} else if w.Profile.Platform == "vk" {
// 		platform = "vkapi"
// 	} else {
// 		platform = "api"
// 	}

// 	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
// 	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID, bytes.NewBuffer(msg))

// 	if err != nil {
// 		return nil, fmt.Errorf("не сформировал реквест: %v", err)
// 	}
// 	req.Header.Set("User-Agent", "WAPPI WH")
// 	req.Header.Set("Authorization", w.Token.Key)

// 	httpClient := &http.Client{}

// 	for attempt := 0; attempt <= intervals; attempt++ {
// 		if attempt > 0 {
// 			time.Sleep(time.Duration(attempt) * 2 * time.Second)
// 		}

// 		resp, err := httpClient.Do(req)
// 		if err != nil {
// 			globalErr = err
// 			log.Errorf("Ошибка запроса: %v", err)
// 			continue
// 		}

// 		responseBody, err := io.ReadAll(resp.Body)
// 		resp.Body.Close()
// 		if err != nil {
// 			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
// 			continue
// 		}

// 		var response WapiStringResponse
// 		if err := json.Unmarshal(responseBody, &response); err != nil {
// 			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
// 			var tt interface{}
// 			err := json.Unmarshal(responseBody, &tt)
// 			if err != nil {
// 				log.Errorf(err.Error())
// 			}
// 			log.Debugf(tt)
// 			continue
// 		}

// 		if resp.StatusCode != 200 {
// 			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
// 			if response.Detail != "" {
// 				log.Errorf("Ошибка: %s", response.Detail)
// 			}
// 			resp.Body.Close()
// 			continue
// 		}
// 		return &response, nil
// 	}
// 	return nil, globalErr
// }

func (w *Wapi) SyncSendMessageV2(jsonMSG *JsonBody, intervals int) (*WapiStringResponse, error) {
	var globalErr error

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/v2/sync/message/send"
	var platform string

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else if w.Profile.Platform == "vk" {
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
		wapi_url = "http://172.21.0.12:8686/"
		platform = "vkapi"
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
	} else {
		platform = "api"
	}

	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			var tt interface{}
			err := json.Unmarshal(responseBody, &tt)
			if err != nil {
				log.Errorf(err.Error())
			}
			log.Debugf(tt)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}
