package wapi

import "time"

type WapiPromo struct {
	ID        uint32    `json:"id,omitempty"`
	Code      string    `json:"code,omitempty"`
	AmountUse int       `json:"amount_use,omitempty"`
	Uses      int       `json:"uses,omitempty"`
	ExpiredAt time.Time `json:"expired_at,omitempty"`
	Discount  int       `json:"discount,omitempty"`
	// TariffIDs []int     `json:"tariff_ids,omitempty"`
	UserGenID int `json:"user_gen_id,omitempty"`
}

func (WapiPromo) TableName() string {
	return "wapi_promo"
}

func (w *Wapi) GetPromo(code string) (*WapiPromo, error) {
	promo := &WapiPromo{}
	if err := w.db.Table("wapi_promo").Where("code = ?", code).First(promo).Error; err != nil {
		return nil, err
	}
	return promo, nil
}
