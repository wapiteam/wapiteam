package wapi

import "time"

type WapiAuthorization struct {
	ID        uint      `gorm:"primaryKey" json:"id,omitempty"`
	Type      string    `gorm:"not null" json:"type,omitempty"`
	ProfileId int       `gorm:"unique;not null" json:"profile_id,omitempty"`
	UUID      string    `json:"uuid,omitempty"`
	Phone     *string   `json:"phone,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty"`
	Reason    string    `json:"reason,omitempty"`
	Details   string    `json:"details,omitempty" gorm:"-"`
}
