package wapi

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

type WapiContainer struct {
	ID            int        `json:"id,omitempty"`
	ProfileID     int        `json:"profile_id,omitempty"`
	ContainerID   *string    `json:"container_id,omitempty"`
	ContainerName *string    `json:"container_name,omitempty"`
	WaStatus      string     `json:"wa_status,omitempty"`
	NodeID        *uint32    `json:"node_id,omitempty"`
	Node          *WapiNode  `json:"node,omitempty"`
	WsConnection  bool       `json:"ws_connection,omitempty"`
	BackupedAt    *time.Time `json:"backuped_at,omitempty"`
	IsRebooting   bool       `json:"is_rebooting,omitempty"`
}

// TODO надо сделать КРУД по контам в базе

func (c *WapiContainer) Restart(ctx context.Context) error {
	dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"), client.WithHost("tcp://"+c.Node.Name+":4444"))
	if err != nil {
		return err
	}
	defer dockerClient.Close()

	if c.ContainerName == nil {
		return fmt.Errorf("container name is nil")
	}

	err = dockerClient.ContainerRestart(ctx, *c.ContainerName, container.StopOptions{})
	if err != nil {
		return err
	}

	return nil
}

func (c *WapiContainer) Stop(ctx context.Context) error {
	dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"), client.WithHost("tcp://"+c.Node.Name+":4444"))
	if err != nil {
		return err
	}
	defer dockerClient.Close()

	if c.ContainerName == nil {
		return fmt.Errorf("container name is nil")
	}

	err = dockerClient.ContainerStop(ctx, *c.ContainerName, container.StopOptions{})
	if err != nil {
		return err
	}

	return nil
}

func (c *WapiContainer) Start(ctx context.Context) error {
	dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"), client.WithHost("tcp://"+c.Node.Name+":4444"))
	if err != nil {
		return err
	}
	defer dockerClient.Close()

	if c.ContainerName == nil {
		return fmt.Errorf("container name is nil")
	}

	err = dockerClient.ContainerStart(ctx, *c.ContainerName, container.StartOptions{})
	if err != nil {
		return err
	}

	return nil
}

func (c *WapiContainer) Remove(ctx context.Context) error {
	dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"), client.WithHost("tcp://"+c.Node.Name+":4444"))
	if err != nil {
		return err
	}
	defer dockerClient.Close()

	if c.ContainerName == nil {
		return fmt.Errorf("container name is nil")
	}

	err = dockerClient.ContainerRemove(ctx, *c.ContainerName, container.RemoveOptions{
		Force: true,
	})

	if err != nil {
		return err
	}

	return nil
}

func (c *WapiContainer) DockerCreate(ctx context.Context, platform PlatformType, binds []string) (string, error) {
	dockerClient, err := client.NewClientWithOpts(client.WithVersion("1.40"), client.WithHost("tcp://"+c.Node.Name+":4444"))
	if err != nil {
		return "", err
	}
	defer dockerClient.Close()

	if c.ContainerName == nil {
		return "", fmt.Errorf("container name is nil")
	}

	if c.ProfileID == 0 {
		return "", fmt.Errorf("profile id is nil")
	}

	binds = append(binds, "/home/wapi/storage/hosts:/etc/hosts")

	var restartPolicy container.RestartPolicy

	var image string

	envs := []string{
		"TZ=Europe/Moscow",
		fmt.Sprintf("DB_HOST=%s", os.Getenv("DB_HOST")),
		fmt.Sprintf("DB_PASSWORD=%s", os.Getenv("DB_PASSWORD")),
		fmt.Sprintf("DB_USER=%s", os.Getenv("DB_USER")),
		fmt.Sprintf("DB_PORT=%s", os.Getenv("DB_PORT")),
		fmt.Sprintf("DB_NAME=%s", os.Getenv("DB_NAME")),
		fmt.Sprintf("AMQP_PORT=%s", os.Getenv("AMQP_PORT")),
		fmt.Sprintf("AMQP_HOST=%s", os.Getenv("AMQP_HOST")),
		fmt.Sprintf("AMQP_USER=%s", os.Getenv("AMQP_USER")),
		fmt.Sprintf("AMQP_PASSWORD=%s", os.Getenv("AMQP_PASSWORD")),
		fmt.Sprintf("WBW_PROFILE_ID=%d", c.ProfileID),
	}

	if platform == Telegram {
		image = "wapitg"

		envs = append(envs, []string{
			fmt.Sprintf("APP_ID=%s", os.Getenv("APP_ID")),
			fmt.Sprintf("APP_HASH=%s", os.Getenv("APP_HASH")),
		}...)

		restartPolicy = container.RestartPolicy{
			Name:              "unless-stopped",
			MaximumRetryCount: 0,
		}

		// restartPolicy.MaximumRetryCount = 50

		binds = append(binds, "/home/wapi/storage/wapitg/wapitg:/src/wapitg")

	} else if platform == Whatsapp {
		image = "gowapi"

		restartPolicy = container.RestartPolicy{
			Name:              "unless-stopped",
			MaximumRetryCount: 0,
		}
		binds = append(binds, "/home/wapi/storage/gowapi/gowapi:/src/gowapi")
	} else if platform == TelegramBot {
		image = "tgbot"

		envs = append(envs, []string{
			fmt.Sprintf("APP_ID=%s", os.Getenv("APP_ID")),
			fmt.Sprintf("APP_HASH=%s", os.Getenv("APP_HASH")),
		}...)

		restartPolicy = container.RestartPolicy{
			Name:              "unless-stopped",
			MaximumRetryCount: 0,
		}

		binds = append(binds, "/home/wapi/storage/tgbot/tgbot:/src/tgbot")
	}

	container, err := dockerClient.ContainerCreate(
		ctx,
		&container.Config{
			Image: image,
			Env:   envs,
		},
		&container.HostConfig{
			RestartPolicy: restartPolicy,
			Binds:         binds,
		},
		nil,
		nil,
		*c.ContainerName,
	)

	if err != nil {
		return "", err
	}

	c.ContainerID = &container.ID

	return container.ID, nil

}

func (c *Wapi) UpdateContainer(container *WapiContainer) error {
	if err := c.db.Table("wapi_container").Where("profile_id = ?", c.Profile.ID).Save(container).Error; err != nil {
		return err
	}
	return nil
}
