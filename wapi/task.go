package wapi

import (
	"context"
	"os"

	"gitlab.com/wapiteam/wapiteam/grpc_wapitask"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (w *Wapi) GetTask(ctx context.Context, messageID, ststus string) (*grpc_wapitask.GetTaskTaskResponse, error) {

	conn, err := grpc.NewClient(os.Getenv("TASK_HAPROXY"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Errorf("fail to dial: %v", err)
		return nil, err
	}
	defer conn.Close()
	client := grpc_wapitask.NewWappiGrpcClient(conn)

	var in grpc_wapitask.GetTaskMessage
	in.Uuid = w.Profile.UUID
	in.MessageId = messageID
	in.Status = ststus

	response, err := client.GetTask(ctx, &in)
	if err != nil {
		log.Errorf("Error when calling SayHello: %s", err)
	}

	return response, err
}
