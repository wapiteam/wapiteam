package wapi

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/wapiteam/wapiteam/wapihelpers"
)

type WapiProfile struct {
	ID                  int            `json:"id"`
	Name                string         `json:"name"`
	WebhookUrl          *string        `json:"webhook_url,omitempty"`
	Status              bool           `json:"status,omitempty"`
	Authorized          bool           `json:"authorized"`
	UUID                string         `json:"uuid,omitempty"`
	UserID              uint           `json:"user_id,omitempty"`
	ProxyID             *int           `json:"proxy_id,omitempty"`
	SpamProxyID         sql.NullInt16  `json:"spam_proxy_id,omitempty"`
	TgSpamProxyID       *int           `json:"tg_spam_proxy_id,omitempty"`
	SpamLevelID         *int32         `json:"spam_level_id,omitempty" gorm:"column:spam_level_id"`
	Warming             int            `json:"warming,omitempty"`
	SpamOpenChats       int            `json:"spam_open_chats,omitempty"`
	AuthorizedAt        *time.Time     `json:"authorized_at,omitempty"`
	LogoutedAt          *time.Time     `json:"logouted_at,omitempty"`
	CheckedAt           time.Time      `json:"checked_at,omitempty"`
	Phone               string         `json:"phone,omitempty"`
	WorkedDays          *int64         `json:"worked_days,omitempty"`
	NeedRestore         bool           `json:"need_restore,omitempty"`
	WebhookTypes        pq.StringArray `json:"webhook_types,omitempty" gorm:"type:varchar(180)[]"`
	WebhookAuth         *string        `json:"webhook_auth,omitempty"`
	MassPostingDisable  bool           `json:"mass_posting_disable,omitempty"`
	DeletedAt           *time.Time     `json:"deleted_at,omitempty"`
	IsSubscribe         bool           `json:"is_subscribe,omitempty"`
	LastActivity        float64        `json:"last_activity,omitempty"`
	MessageCount        uint32         `json:"message_count,omitempty"`
	PaymentNotification bool           `json:"payment_notification,omitempty"`
	ResolveConflict     bool           `json:"resolve_conflict,omitempty"`
	CheckCount          uint32         `json:"check_count,omitempty"`
	QR                  *string        `json:"qr,omitempty" gorm:"-"`
	SelfProxy           *string        `json:"self_proxy,omitempty"`
	TemporyBan          float64        `json:"tempory_ban,omitempty" gorm:"-"`
	CrmType             *string        `json:"crm_type,omitempty"`
	CrmID               *string        `json:"crm_id,omitempty"`
	BotEnable           bool           `json:"bot_enable,omitempty"`
	AsisstantEnable     bool           `json:"asisstant_enable,omitempty"`
	Synchronized        bool           `json:"synchronized"`
	Platform            string         `json:"platform"`
	Floodwait           *int           `json:"floodwait,omitempty"`
	UseragentID         uint32         `json:"useragent_id,omitempty"`
	TgProxyID           *int           `json:"tg_proxy_id"`
	Tech                string         `json:"tech,omitempty"`
	Nickname            *string        `json:"nickname,omitempty"`
	CheckLimitDisable   bool           `json:"check_limit_disable,omitempty"`
	ChatsEnable         bool           `json:"chats_enable,omitempty"`
	AuthNotification    bool           `json:"auth_notification,omitempty"`
	ProxyPool           string         `json:"proxy_pool"`
	AnalType            *string        `json:"anal_type,omitempty"`
	AiEnable            bool           `json:"ai_enable,omitempty"`
	BotToken            string         `json:"bot_token,omitempty"`
}

func (WapiProfile) TableName() string {
	return "wapi_profile"
}

func (w *Wapi) UpdateProfile(profile *WapiProfile) error {
	if err := w.db.Table("wapi_profile").Where("uuid = ?", w.Profile.UUID).Save(&profile).Error; err != nil {
		return err
	}
	return nil
}

func (w *Wapi) GetProfile() (*WapiProfile, error) {
	profile := &WapiProfile{}
	if err := w.db.Table("wapi_profile").Where("uuid = ?", w.Profile.UUID).First(&profile).Error; err != nil {
		return nil, err
	}
	return profile, nil
}

func (w *Wapi) ProfileCreate(platform, name, webhook_url string, panel bool) (*WapiProfile, error) {
	var spam_level *int32
	var err error
	var today_count int64
	today := time.Now().Format("2006-01-02")
	w.db.Table("wapi_profile").Where("user_id = ?", w.User.ID).Where("DATE(checked_at) = ?", today).Count(&today_count)
	log.Warnf("today_count created profiles by %s: %d", w.User.Email, today_count)
	if today_count >= 300 && !w.User.WithPayments {
		log.Warnf("ЮЗЕР %s ЗАЕБАЛ СОЗДАВАТЬ ПРОФИЛИ!!! УЖЕ СОЗДАНО %d", w.User.Email, today_count)
		wapihelpers.SendTgMsg(fmt.Sprintf("ЮЗЕР %s ПРЕВЫСИЛ ЛИМИТ НА СОЗДАНИЕ ПРОФИЛЕЙ!!! УЖЕ СОЗДАНО %d", w.User.Email, today_count), "zhlupek")
		return nil, errors.New("превышен лимит создания профилей")
	}

	location := time.FixedZone("GMT+3", 3*60*60)
	now := time.Now().In(location)

	profile := &WapiProfile{
		Name:                name,
		Platform:            platform,
		PaymentNotification: true,
		UserID:              uint(w.User.ID),
		UUID:                uuid.NewString()[0:13],
		UseragentID:         1,
		Tech:                w.User.Tech,
		WebhookUrl:          &webhook_url,
		Status:              true,
		ResolveConflict:     true,
		IsSubscribe:         false,
		CheckedAt:           now,
		WorkedDays:          proto.Int64(0),
		SpamLevelID:         spam_level,
		CheckLimitDisable:   false,
		WebhookTypes: []string{
			"incoming_message",
			"delivery_status",
			"authorization_status",
		},
	}

	err = w.db.Table("wapi_profile").Create(&profile).Error
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	ctName := strconv.Itoa(profile.ID)
	container := &WapiContainer{
		ProfileID:     profile.ID,
		WaStatus:      "close",
		ContainerName: &ctName,
	}

	err = w.db.Table("wapi_container").Create(&container).Error
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	if !w.User.WithPayments {
		// распределяем
		log.Infof("Распределяем для профиля %s, panel: %v", profile.UUID, panel)
		if panel {
			go w.ProxyNodes(profile, container, false)
		} else {
			w.ProxyNodes(profile, container, false)
		}

		var tariff_id uint32

		if w.User.TariffId != nil {
			tariff_id = *w.User.TariffId
		} else {
			tariff_id = 1
		}

		location := time.FixedZone("GMT+3", 3*60*60)
		now := time.Now().In(location)

		_, err = w.AddProfilePaymentByExiredDate(&now, tariff_id)
		if err != nil {
			log.Errorf(err.Error())
			return nil, err
		}

	}

	w.Profile = profile

	if w.User.ID == 2034 || w.User.ID == 3498 {
		if profile.WorkedDays != nil {
			*profile.WorkedDays++
		}
		log.Infof("Добавляю профилю %s 1 день работы", profile.UUID)
		if err = w.db.Table("wapi_profile").Where("id = ?", profile.ID).UpdateColumn("worked_days", profile.WorkedDays).Error; err != nil {
			log.Errorf(err.Error())
			return nil, err
		}

		var tariff WapiTariff
		if err = w.db.Table("wapi_tariff").First(&tariff, w.User.TariffId).Error; err != nil {
			log.Errorf(err.Error())
			return nil, err
		}

		w.User.Debt += tariff.Price
		if err = w.db.Table("wapi_user").Where("id = ?", w.User.ID).UpdateColumn("debt", w.User.Debt).Error; err != nil {
			log.Errorf(err.Error())
			return nil, err
		}
	}

	return profile, nil
}

func (w *Wapi) ProxyNodes(profile *WapiProfile, container *WapiContainer, createCont bool) {
	var err error
	w.Profile = profile
	if w.Profile.Platform == "wz" {
		var proxy_table string
		var proxy_relatation string
		if w.User.Spamer {
			proxy_table = "wapi_spamproxy"
			proxy_relatation = "spam_proxy_id"
		} else {
			proxy_table = "wapi_proxy"
			proxy_relatation = "proxy_id"
		}

		var proxies []WapiProxy

		if err = w.db.Table(proxy_table).Find(&proxies).Error; err != nil {
			log.Errorf(err.Error())
			return
		}

		if len(proxies) == 0 {
			log.Errorf("No active proxies available")
			return
		}

		proxy_map := make(map[uint]int)
		for _, p := range proxies {
			var count int64
			err = w.db.Table("wapi_profile").Where(proxy_relatation+" = ?", p.ID).Count(&count).Error
			if err != nil {
				log.Errorf(err.Error())
				return
			}
			proxy_map[p.ID] = int(count)
		}

		var mini_prxy_id uint
		mini_proxy_count := -1
		for id, count := range proxy_map {
			if mini_proxy_count == -1 || count < mini_proxy_count {
				mini_prxy_id = id
				mini_proxy_count = count
			}
		}

		proxx := int(mini_prxy_id)

		if err = w.db.Table("wapi_profile").Where("id = ?", profile.ID).UpdateColumn(proxy_relatation, proxx).Error; err != nil {
			log.Errorf(err.Error())
			return
		}

		// распределяем ноды для вц

		var nodes []WapiNode

		if err = w.db.Table("wapi_node").Where("tech = ?", "go").Find(&nodes).Error; err != nil {
			log.Errorf(err.Error())
			return
		}

		node_map := make(map[WapiNode]int)
		for _, n := range nodes {
			var count int64
			err = w.db.Table("wapi_container").Where("node_id = ?", n.ID).Count(&count).Error
			if err != nil {
				log.Errorf(err.Error())
				return
			}
			node_map[n] = int(count)
		}

		var mini_node WapiNode
		mini_node_count := -1
		for n, count := range node_map {
			if mini_node_count == -1 || count < mini_node_count {
				mini_node = n
				mini_node_count = count
			}
		}

		if len(nodes) == 0 {
			log.Errorf("No active nodes available")
			return
		}

		container.NodeID = &mini_node.ID
		container.Node = &mini_node

		if err = w.db.Table("wapi_container").Where("profile_id = ?", profile.ID).UpdateColumn("node_id", container.NodeID).Error; err != nil {
			log.Errorf(err.Error())
			return
		}

		if !w.User.WithPayments || createCont {
			container.ContainerName = proto.String(strconv.Itoa(profile.ID))

			cont, err := container.DockerCreate(context.TODO(), Whatsapp, nil)
			if err != nil {
				log.Errorf(err.Error())
				return
			}

			container.ContainerID = &cont

			if err = w.db.Table("wapi_container").Where("profile_id = ?", profile.ID).UpdateColumn("container_name", container.ContainerName).Error; err != nil {
				log.Errorf(err.Error())
				return
			}
			if err = w.db.Table("wapi_container").Where("profile_id = ?", profile.ID).UpdateColumn("container_id", container.ContainerID).Error; err != nil {
				log.Errorf(err.Error())
				return
			}

			err = container.Start(context.TODO())
			if err != nil {
				log.Errorf(err.Error())
				return
			}
		}
	}
}

func (w *Wapi) UpdateUserToSklad(skladID, skladToken string) error {
	if w.User.Moysklad != "" || w.User.MoyskladToken != "" {
		return fmt.Errorf("profile %v was already activated as sklad account. Deny rewrite sklad id from [%v] to [%v]", w.User.ID, w.User.Moysklad, skladID)
	}
	w.User.Moysklad = skladID
	w.User.MoyskladToken = skladToken
	err := w.db.Table("wapi_user").Save(w.User).Error
	if err != nil {
		return fmt.Errorf("can't update user profile %v to sklad account with skladId %v and token %v: %v", w.User.ID, skladID, skladToken, err)
	}
	log.Resultf("profile %v has been updated to sklad account with sklad id %v and token %v", w.User.ID, skladID, skladToken)
	return nil
}

func (w *Wapi) NeedRestoreTG() error {
	var err error

	if w.Container.ContainerID != nil && *w.Container.ContainerID != "deleted" && w.Container.WaStatus == "close" {
		if err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).UpdateColumn("container_id", "deleted").Error; err != nil {
			return fmt.Errorf("can't restore container %v: %v", w.Container.ID, err)
		}
	}

	if err := w.Container.Remove(context.Background()); err != nil {
		return fmt.Errorf("can't remove container %v: %v", w.Container.ID, err)
	}

	// распределяем прокси
	w.Profile.NeedRestore = true
	w.Profile.TgProxyID = nil
	w.Profile.TgSpamProxyID = nil
	w.Profile.Authorized = false
	var proxy_table string
	var proxy_relatation string

	if w.User.Spamer {
		proxy_table = "wapi_tgspamproxy"
		proxy_relatation = "tg_spam_proxy_id"
	} else {
		proxy_table = "wapi_telegramproxy"
		proxy_relatation = "tg_proxy_id"
	}

	var proxies []WapiProxy
	w.db.Table(proxy_table).Find(&proxies)

	if len(proxies) == 0 {
		return errors.New("no active proxies available")
	}

	proxy_map := make(map[uint]int)
	for _, p := range proxies {
		var count int64
		err := w.db.Table("wapi_profile").Where(proxy_relatation+" = ?", p.ID).Count(&count).Error
		if err != nil {
			return fmt.Errorf("can't get profile count: %v", err)
		}
		proxy_map[p.ID] = int(count)
	}

	var mini_prxy_id uint
	mini_proxy_count := -1
	for id, count := range proxy_map {
		if mini_proxy_count == -1 || count < mini_proxy_count {
			mini_prxy_id = id
			mini_proxy_count = count
		}
	}

	prm := int(mini_prxy_id)
	if w.User.Spamer {
		w.Profile.TgSpamProxyID = &prm
	} else {
		w.Profile.TgProxyID = &prm
	}

	if err = w.db.Table("wapi_profile").Where("id = ?", w.Profile.ID).Save(&w.Profile).Error; err != nil {
		return fmt.Errorf("can't save profile %v: %v", w.Profile.ID, err)
	}

	// распределяем ноду
	w.Container.ContainerID = nil
	w.Container.NodeID = nil
	var nodes []WapiNode

	if err = w.db.Table("wapi_node").Where("tech = ?", "tg").Find(&nodes).Error; err != nil {
		return fmt.Errorf("can't get nodes: %v", err)
	}

	node_map := make(map[WapiNode]int)
	for _, n := range nodes {
		var count int64
		err = w.db.Table("wapi_container").Where("node_id = ?", n.ID).Count(&count).Error
		if err != nil {
			return fmt.Errorf("can't get node count: %v", err)
		}
		node_map[n] = int(count)
	}

	var mini_node WapiNode
	mini_node_count := -1
	for n, count := range node_map {
		if mini_node_count == -1 || count < mini_node_count {
			mini_node = n
			mini_node_count = count
		}
	}

	if len(nodes) == 0 {
		log.Errorf("No active nodes available")
		return errors.New("no active nodes available")
	}

	var miniNodeObj WapiNode
	err = w.db.Table("wapi_node").Where("id = ?", mini_node.ID).First(&miniNodeObj).Error
	if err != nil {
		return fmt.Errorf("can't get node %v: %v", mini_node.ID, err)
	}

	w.Container.NodeID = &mini_node.ID
	w.Container.Node = &miniNodeObj

	if err := w.db.Table("wapi_container").Where("profile_id = ?", w.Profile.ID).UpdateColumn("node_id", w.Container.NodeID).Error; err != nil {
		return fmt.Errorf("can't update container %v: %v", w.Container.ID, err)
	}

	log.Debugf(w.Container)

	if w.Container.ContainerID == nil || (w.Container.ContainerID != nil && *w.Container.ContainerID == "") || (w.Container.ContainerID != nil && *w.Container.ContainerID == "deleted") {
		_, err := w.Container.DockerCreate(context.TODO(), Telegram, nil)
		if err != nil {
			return fmt.Errorf("can't create container for profile %v: %v", w.Profile.ID, err)
		}

		err = w.db.Table("wapi_container").Where("id = ?", w.Container.ID).Save(&w.Container).Error
		if err != nil {
			return fmt.Errorf("can't save container %v: %v", w.Container.ID, err)
		}

		log.Warnf("Container Start: %s", w.Container.ContainerID)
		if err := w.Container.Start(context.TODO()); err != nil {
			return fmt.Errorf("can't start container %v: %v", w.Container.ID, err)
		}
	}
	return nil
}

func (w *Wapi) GetProfileByUUID(uuid string) (*WapiProfile, error) {
	var profile WapiProfile
	if err := w.db.Table("wapi_profile").Where("uuid = ?", uuid).First(&profile).Error; err != nil {
		return nil, err
	}
	return &profile, nil
}
