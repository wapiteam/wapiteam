package wapi

import (
	"time"

	"go.mau.fi/whatsmeow/types"
)

type Chat struct {
	ID                        types.JID          `json:"id" bson:"id"`
	Contact                   *types.ContactInfo `json:"contact" bson:"contact"`
	Chat                      types.UserInfo     `json:"chat" bson:"chat"`
	Group                     *types.GroupInfo   `json:"group" bson:"group"`
	IsGroup                   bool               `json:"isGroup" bson:"isGroup"`
	Picture                   string             `json:"image" bson:"picture"`
	Thumbnail                 string             `json:"thumbnail" bson:"thumbnail"`
	AvatarUpdatedAt           int64              `json:"avatar_updated_at" bson:"avatar_updated_at"`
	LastMessageDeliveryStatus string             `json:"last_message_delivery_status" bson:"last_message_delivery_status"`
	LastMessageId             string             `json:"last_message_id" bson:"last_message_id"`
	LastTimeMessage           time.Time          `json:"last_timestamp" bson:"last_timestamp"`
	LastTimestampMessage      int64              `json:"last_time" bson:"last_time"`
	LastMessageType           string             `json:"last_message_type" bson:"last_message_type"`
	LastMessageSender         WAContact          `json:"last_message_sender" bson:"last_message_sender"`
	LastMessageData           string             `json:"last_message_data" bson:"last_message_data"`
	Name                      string             `json:"name" bson:"name"`
	UnreadCount               int32              `json:"unread_count" bson:"unread_count"`
	IsDeleted                 bool               `json:"isDeleted" bson:"isDeleted"`
	IsArchived                bool               `json:"isArchived" bson:"isArchived"`
}
