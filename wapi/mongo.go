package wapi

import (
	"context"
	"fmt"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (w *Wapi) GetMongoForProfile() (*mongo.Client, error) {

	if w.Container == nil {
		return nil, fmt.Errorf("Container is nil")
	}

	if w.Container.Node == nil {
		return nil, fmt.Errorf("Node is nil")
	}

	if w.Container.Node.MongoIp == "" {
		return nil, fmt.Errorf("MongoIp is empty")
	}

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(w.Container.Node.MongoIp))
	if err != nil {
		log.Errorf("mongo.NewClient ERROR", err)
	}

	return client, err

}

func (w *Wapi) GetProfileChatsDays(ctx context.Context, daysOffset, daysLimit int, order string, isDeleted, isArchived bool) ([]Chat, int64, error) {

	client, err := w.GetMongoForProfile()
	if err != nil {
		log.Errorf("mongo.NewClient ERROR", err)
		return nil, 0, err
	}

	defer client.Disconnect(ctx)
	colName := fmt.Sprintf("%d_chats", w.Profile.ID)

	collection := client.Database("wapi").Collection(colName)

	now := time.Now()

	offsetStart := now.AddDate(0, 0, -daysOffset)
	limitEnd := offsetStart.AddDate(0, 0, -daysLimit)

	startUnix := offsetStart.Unix()
	endUnix := limitEnd.Unix()

	filter := bson.M{
		"last_time": bson.M{
			"$gte": endUnix,
			"$lt":  startUnix,
		},
		"isDeleted":  isDeleted,
		"isArchived": isArchived,
	}

	options := options.Find()
	if order == "desc" {
		options.SetSort(bson.D{{"last_time", -1}})
	} else {
		options.SetSort(bson.D{{"last_time", 1}})
	}

	cursor, err := collection.Find(ctx, filter, options)
	if err != nil {
		return nil, 0, err
	}

	all_count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("mongo CountDocuments ERROR", err)
		return nil, 0, err
	}

	defer cursor.Close(ctx)

	var chats []Chat
	for cursor.Next(ctx) {
		var chat Chat
		if err := cursor.Decode(&chat); err != nil {
			log.Errorf("Ошибка декодирования документа: %v", err)
			continue
		}
		chat.ID.Server = strings.ReplaceAll(chat.ID.Server, "s.whatsapp.net", "c.us")
		chats = append(chats, chat)
	}

	return chats, all_count, nil
}

func (w *Wapi) GetProfileChatsFilter(ctx context.Context, filters map[string]interface{}) ([]Chat, int64, error) {

	client, err := w.GetMongoForProfile()
	if err != nil {
		log.Errorf("mongo.NewClient ERROR", err)
		return nil, 0, err
	}

	defer client.Disconnect(ctx)

	colName := fmt.Sprintf("%d_chats", w.Profile.ID)
	collection := client.Database("wapi").Collection(colName)

	filter := bson.M{}

	if searchString, ok := filters["client_name"].(string); ok && strings.TrimSpace(searchString) != "" {
		str := strings.TrimSpace(strings.TrimPrefix(strings.ReplaceAll(searchString, "+", ""), " "))
		filter["$or"] = bson.A{
			bson.M{"id.user": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"name": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"last_message_data": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"contact.FirstName": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"contact.FullName": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"contact.PushName": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"contact.BusinessName": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
		}
	}

	options := options.Find()
	options.SetSort(bson.D{{"last_time", -1}})

	cursor, err := collection.Find(ctx, filter, options)
	if err != nil {
		return nil, 0, err
	}

	all_count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("mongo CountDocuments ERROR", err)
		return nil, 0, err
	}

	defer cursor.Close(ctx)

	var chats []Chat
	for cursor.Next(ctx) {
		var chat Chat
		if err := cursor.Decode(&chat); err != nil {
			log.Errorf("Ошибка декодирования документа: %v", err)
			continue
		}
		chat.ID.Server = strings.ReplaceAll(chat.ID.Server, "s.whatsapp.net", "c.us")
		chats = append(chats, chat)
	}

	if err := cursor.Err(); err != nil {
		log.Errorf("Ошибка курсора: %v", err)
		return nil, 0, err
	}

	return chats, all_count, nil
}

func (w *Wapi) GetProfileTGChatsDays(ctx context.Context, daysOffset, daysLimit int, order string, isDeleted, isArchived bool) ([]CustomTGChat, int64, error) {

	client, err := w.GetMongoForProfile()
	if err != nil {
		log.Errorf("mongo.NewClient ERROR", err)
		return nil, 0, err
	}

	defer client.Disconnect(ctx)

	collection := client.Database("chats").Collection(w.Profile.UUID)

	now := time.Now()

	offsetStart := now.AddDate(0, 0, -daysOffset)
	limitEnd := offsetStart.AddDate(0, 0, -daysLimit)

	startUnix := offsetStart.Unix()
	endUnix := limitEnd.Unix()

	filter := bson.M{
		"last_timestamp": bson.M{
			"$gte": endUnix,
			"$lt":  startUnix,
		},
		"isDeleted":  isDeleted,
		"isArchived": isArchived,
	}

	options := options.Find()
	if order == "desc" {
		options.SetSort(bson.D{{"last_timestamp", -1}})
	} else {
		options.SetSort(bson.D{{"last_timestamp", 1}})
	}

	cursor, err := collection.Find(ctx, filter, options)
	if err != nil {
		return nil, 0, err
	}

	all_count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("mongo CountDocuments ERROR", err)
		return nil, 0, err
	}

	defer cursor.Close(ctx)

	var chats []CustomTGChat
	for cursor.Next(ctx) {
		var customResult CustomTGChat
		if err := cursor.Decode(&customResult); err != nil {
			log.Errorf("Ошибка декодирования документа: %v", err)
			continue
		}

		chats = append(chats, customResult)
	}

	return chats, all_count, nil
}

func (w *Wapi) GetProfileTGChatsFilter(ctx context.Context, filters map[string]interface{}) ([]CustomTGChat, int64, error) {

	client, err := w.GetMongoForProfile()
	if err != nil {
		log.Errorf("mongo.NewClient ERROR", err)
		return nil, 0, err
	}

	defer client.Disconnect(ctx)

	collection := client.Database("chats").Collection(w.Profile.UUID)

	filter := bson.M{}

	if searchString, ok := filters["client_name"].(string); ok && strings.TrimSpace(searchString) != "" {
		str := strings.TrimSpace(strings.TrimPrefix(strings.ReplaceAll(searchString, "+", ""), " "))
		filter["$or"] = bson.A{
			bson.M{"user.phone": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"chat.title": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"last_message_data": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"channel.title": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"user.username": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"user.firstname": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"user.lastname": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
			bson.M{"name": bson.M{
				"$regex":   str,
				"$options": "i",
			}},
		}
	}

	options := options.Find()
	options.SetSort(bson.D{{"last_timestamp", -1}})

	cursor, err := collection.Find(ctx, filter, options)
	if err != nil {
		return nil, 0, err
	}

	all_count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("mongo CountDocuments ERROR", err)
		return nil, 0, err
	}

	defer cursor.Close(ctx)

	var chats []CustomTGChat
	for cursor.Next(ctx) {
		var customResult CustomTGChat
		if err := cursor.Decode(&customResult); err != nil {
			log.Errorf("Ошибка декодирования документа: %v", err)
			continue
		}
		chats = append(chats, customResult)
	}

	return chats, all_count, nil
}
