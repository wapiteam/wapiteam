package wapi

import (
	"time"

	"github.com/gotd/td/tg"
	"gitlab.com/wapiteam/wapiteam/wapi/tgmodels"
)

type TGChat struct {
	ID                        string                  `json:"id,omitempty"`
	User                      *tg.User                `json:"user,omitempty"`
	Chat                      *tg.Chat                `json:"chat,omitempty"`
	Channel                   *tgmodels.CustomChannel `json:"channel,omitempty"`
	UnreadCount               int                     `json:"unread_count" bson:"unread_count"`
	Participants              []TGParticipant         `json:"participants,omitempty"`
	IsDeleted                 bool                    `json:"isDeleted" bson:"isDeleted"`
	IsArchived                bool                    `json:"isArchived" bson:"isArchived"`
	IsPinned                  bool                    `json:"isPinned" bson:"isPinned"`
	Name                      string                  `json:"name,omitempty"`
	Picture                   string                  `json:"picture"`
	Thumbnail                 string                  `json:"thumbnail,omitempty"`
	LastMessageID             string                  `json:"last_message_id,omitempty" bson:"last_message_id"`
	LastMessageData           string                  `json:"last_message_data,omitempty" bson:"last_message_data"`
	LastMessageTimeStamp      int                     `json:"last_timestamp,omitempty" bson:"last_timestamp"`
	LastMessageSender         *TGContact              `json:"last_message_sender,omitempty" bson:"last_message_sender"`
	LastMessageType           string                  `json:"last_message_type,omitempty" bson:"last_message_type"`
	LastMessageTime           time.Time               `json:"last_time,omitempty" bson:"last_time"`
	Type                      string                  `json:"type" bson:"type"`
	LastMessageDeliveryStatus string                  `json:"last_message_delivery_status" bson:"last_message_delivery_status"`
	TotalMessages             int                     `json:"total_messages" bson:"total_messages"`
}

type TGParticipant struct {
	UserID          string `json:"user_id"`
	IsAdmin         bool   `json:"is_admin"`
	IsCreator       bool   `json:"is_creator"`
	Name            string `json:"name"`
	UserName        string `json:"username"`
	Phone           string `json:"phone"`
	IsBot           bool   `json:"is_bot"`
	IsMe            bool   `json:"is_me"`
	InviterID       string `json:"inviter_id"`
	InviteTimestamp int    `json:"invite_timestamp"`
}

type TGContact struct {
	ID          string  `json:"id"`
	ContactType *string `json:"type"`
	Number      string  `json:"number"`
	Pushname    string  `json:"pushname"`
	FirstName   string  `json:"firstName"`
	LastName    string  `json:"lastName"`
	Picture     *string `json:"picture"`
	Thumbnail   *string `json:"thumbnail"`
	Username    string  `json:"username"`
	IsMe        bool    `json:"isMe"`
}

type CustomTGChat struct {
	ID                        string                  `json:"id,omitempty"`
	User                      *tgmodels.CustomUser    `json:"user,omitempty"`
	Chat                      *tgmodels.CustomChat    `json:"chat,omitempty"`
	Channel                   *tgmodels.CustomChannel `json:"channel,omitempty"`
	Participants              []TGParticipant         `json:"participants,omitempty"`
	UnreadCount               int                     `json:"unread_count" bson:"unread_count"`
	IsDeleted                 bool                    `json:"isDeleted" bson:"isDeleted"`
	IsArchived                bool                    `json:"isArchived" bson:"isArchived"`
	IsPinned                  bool                    `json:"isPinned" bson:"isPinned"`
	Name                      string                  `json:"name,omitempty"`
	Picture                   string                  `json:"picture"`
	Thumbnail                 string                  `json:"thumbnail,omitempty"`
	LastMessageID             string                  `json:"last_message_id,omitempty" bson:"last_message_id"`
	LastMessageData           string                  `json:"last_message_data,omitempty" bson:"last_message_data"`
	LastMessageTimeStamp      int                     `json:"last_timestamp,omitempty" bson:"last_timestamp"`
	LastMessageSender         TGContact               `json:"last_message_sender,omitempty" bson:"last_message_sender"`
	LastMessageType           string                  `json:"last_message_type,omitempty" bson:"last_message_type"`
	LastMessageTime           time.Time               `json:"last_time,omitempty" bson:"last_time"`
	Type                      string                  `json:"type" bson:"type"`
	LastMessageDeliveryStatus string                  `json:"last_message_delivery_status" bson:"last_message_delivery_status"`
	TotalMessages             int                     `json:"total_messages" bson:"total_messages"`
}

type CustomChatChat struct {
	Chat             *tgmodels.CustomChat `json:"chat,omitempty"`
	ChatParticipants []tg.ChatParticipant `json:"chat_participants,omitempty"`
}

type CustomChannelChannel struct {
	Channel             *tgmodels.CustomChannel `json:"channel,omitempty"`
	ChannelParticipants []tg.ChannelParticipant `json:"channel_participants,omitempty"`
}

func (cp *CustomTGChat) CustomTGChatToChat() (TGChat, error) {

	switch cp.LastMessageType {
	case "image":
		cp.LastMessageData = "🖼️ Image"
	case "audio":
		cp.LastMessageData = "🎵 Audio"
	case "document":
		cp.LastMessageData = "📄 Document"
	case "video":
		cp.LastMessageData = "🎥 Video"
	case "not_supported":
		cp.LastMessageData = "📱 Unsupported message type"
	}

	// dd
	tgChat := TGChat{
		ID:                        cp.ID,
		Type:                      cp.Type,
		UnreadCount:               cp.UnreadCount,
		IsDeleted:                 cp.IsDeleted,
		IsArchived:                cp.IsArchived,
		IsPinned:                  cp.IsPinned,
		Name:                      cp.Name,
		Picture:                   cp.Picture,
		Thumbnail:                 cp.Thumbnail,
		LastMessageID:             cp.LastMessageID,
		LastMessageData:           cp.LastMessageData,
		LastMessageTimeStamp:      cp.LastMessageTimeStamp,
		LastMessageSender:         &cp.LastMessageSender,
		LastMessageType:           cp.LastMessageType,
		LastMessageTime:           cp.LastMessageTime,
		LastMessageDeliveryStatus: cp.LastMessageDeliveryStatus,
		TotalMessages:             cp.TotalMessages,
		Participants:              cp.Participants,
	}

	if cp.User != nil {
		tgChat.User = cp.User.ConvertCustomUserToTGUser()
		return tgChat, nil
	} else if cp.Chat != nil {
		tgChat.Chat = cp.Chat.ConvertCustomChatToTGChat()
		return tgChat, nil
	} else if cp.Channel != nil {
		tgChat.Channel = cp.Channel
		return tgChat, nil
	} else {
		log.Errorf("udenfined chat type")
		return tgChat, nil
	}
}
