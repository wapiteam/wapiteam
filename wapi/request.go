package wapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"gitlab.com/wapiteam/wapiteam/wapi/wamodels"
)

type JsonBody struct {
	Recipient       string           `json:"recipient,omitempty"`
	Body            string           `json:"body,omitempty"`
	Caption         string           `json:"caption,omitempty"`
	B64File         string           `json:"b64_file,omitempty"`
	FileName        string           `json:"file_name,omitempty"`
	Latitude        float64          `json:"latitude,omitempty"`
	Longitude       float64          `json:"longitude,omitempty"`
	Address         string           `json:"address,omitempty"`
	Phone           string           `json:"phone,omitempty"`
	MessageId       string           `json:"message_id,omitempty"`
	Name            string           `json:"name,omitempty"`
	Participants    []string         `json:"participants,omitempty"`
	Participant     string           `json:"participant,omitempty"`
	GroupID         string           `json:"group_id,omitempty"`
	Description     string           `json:"description,omitempty"`
	Url             string           `json:"url,omitempty"`
	Buttons         []BodyButtons    `json:"buttons,omitempty"`
	Title           string           `json:"title,omitempty"`
	List            *ButtonList      `json:"list,omitempty"`
	BtnText         string           `json:"btn_text,omitempty"`
	PollOptions     []string         `json:"poll_options,omitempty"`
	PollSelectCount uint32           `json:"poll_select_count,omitempty"`
	OTPLive         int              `json:"otp_live,omitempty"`
	TimeoutFrom     int              `json:"timeout_from,omitempty"`
	TimeoutTo       int              `json:"timeout_to,omitempty"`
	MassPostingID   *string          `json:"mass_posting_id,omitempty"`
	AuthCode        string           `json:"auth_code,omitempty"`
	PwdCode         string           `json:"pwd_code,omitempty"`
	Source          string           `json:"source,omitempty"`
	Token           string           `json:"token,omitempty"`
	ChatName        string           `json:"chat_name,omitempty"`
	Filter          string           `json:"filter,omitempty"`
	URLAttachments  *[]URLAtatchment `json:"attachments,omitempty"`
}

type URLAtatchment struct {
	Name string `json:"name"`
	Url  string `json:"url"`
	Type string `json:"type"`
}

type BodyButtons struct {
	Body string  `json:"body,omitempty"`
	Id   *string `json:"id,omitempty"`
}

type ButtonList struct {
	Title string     `json:"title,omitempty"`
	Rows  []ListRows `json:"rows,omitempty"`
}

type ListRows struct {
	ID          string `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
}

func (w *Wapi) SendTextMessage(recipinet string, body string, intervals int) (*WapiStringResponse, error) {

	var globalErr error

	jsonMSG := &JsonBody{
		Recipient: recipinet,
		Body:      body,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/sync/message/send"
	var platform string

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else if w.Profile.Platform == "vk" {
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
		wapi_url = "http://172.21.0.12:8686/"
		platform = "vkapi"
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
	} else {
		platform = "api"
	}

	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			var tt interface{}
			err := json.Unmarshal(responseBody, &tt)
			if err != nil {
				log.Errorf(err.Error())
			}
			log.Debugf(tt)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

// для отправки файла реплаем - в боди должен быть урл на файл
func (w *Wapi) SendReply(replyMsgId, body string, intervals int, extParams ...map[string]string) (*WapiStringResponse, error) {

	var globalErr error

	jsonMSG := &JsonBody{
		MessageId: replyMsgId,
		Body:      body,
	}

	if len(extParams) > 0 {
		if recipient, ok := extParams[0]["recipient"]; ok {
			jsonMSG.Recipient = recipient
		}
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/sync/message/reply"
	var platform string
	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else if w.Profile.Platform == "vk" {
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
		wapi_url = "http://172.21.0.12:8686/"
		platform = "vkapi"
		//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST//DEV TEST
	} else {
		platform = "api"
	}

	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) SendFileUrl(recipinet string, fileName, caption string, url string, intervals int) (*WapiStringResponse, error) {

	var globalErr error
	jsonMSG := &JsonBody{
		Recipient: recipinet,
		Caption:   caption,
		FileName:  fileName,
		Url:       url,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/sync/message/file/url/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else if w.Profile.Platform == "vk" {
		platform = "vkapi"
	} else {
		platform = "api"
	}

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			var tt interface{}
			err := json.Unmarshal(responseBody, &tt)
			if err != nil {
				log.Errorf(err.Error())
			}
			log.Debugf(tt)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) SendFileUrlWithPostingID(recipinet string, caption string, url string, intervals int, postingID string) (*WapiStringResponse, error) {

	var globalErr error
	jsonMSG := &JsonBody{
		Recipient:     recipinet,
		Caption:       caption,
		Url:           url,
		MassPostingID: &postingID,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/sync/message/file/url/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else {
		platform = "api"
	}

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID+"&bot_id="+postingID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) SendTextMessageWithPostingID(recipinet string, body string, intervals int, postingID string) (*WapiStringResponse, error) {

	var globalErr error

	jsonMSG := &JsonBody{
		Recipient:     recipinet,
		Body:          body,
		MassPostingID: &postingID,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/sync/message/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else {
		platform = "api"
	}

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("POST", wapi_url+platform+method+"?profile_id="+w.Profile.UUID+"&bot_id="+postingID, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) SendAsyncTextMessage(recipinet, body string, timeout_from, timeout_to int, intervals int) (*WapiStringResponse, error) {

	var globalErr error

	jsonMSG := &JsonBody{
		Recipient:   recipinet,
		Body:        body,
		TimeoutFrom: timeout_from,
		TimeoutTo:   timeout_to,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/async/message/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else {
		platform = "api"
	}

	fullUrl := fmt.Sprintf("https://%s/%s%s?profile_id=%s&timeout_from=%d&timeout_to=%d", os.Getenv("WAPI_HOST"), platform, method, w.Profile.UUID, timeout_from, timeout_to)
	req, err := http.NewRequest("POST", fullUrl, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) SendAsyncFileUrl(recipinet, caption, url string, timeout_from, timeout_to int, intervals int) (*WapiStringResponse, error) {

	var globalErr error
	jsonMSG := &JsonBody{
		Recipient:   recipinet,
		Caption:     caption,
		Url:         url,
		TimeoutFrom: timeout_from,
		TimeoutTo:   timeout_to,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/async/message/file/url/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		platform = "avito"
	} else {
		platform = "api"
	}

	if timeout_from == 0 {
		timeout_from = 2
	}

	if timeout_to == 0 {
		timeout_to = 5
	}

	fullUrl := fmt.Sprintf("https://%s/%s%s?profile_id=%s&timeout_from=%d&timeout_to=%d", os.Getenv("WAPI_HOST"), platform, method, w.Profile.UUID, timeout_from, timeout_to)
	req, err := http.NewRequest("POST", fullUrl, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

func (w *Wapi) CheckContact(recipinet string, intervals int) (bool, error) {
	var globalErr error
	var method string

	if w.Profile.Platform == "tg" {
		method = "tapi/sync/contact/get?recipient="
	} else if w.Profile.Platform == "wz" {
		method = "api/sync/contact/check?phone="
	} else {
		return false, fmt.Errorf("неизвестная платформа: %s", w.Profile.Platform)
	}
	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("GET", wapi_url+method+recipinet+"&profile_id="+w.Profile.UUID+"&add=true", nil)

	if err != nil {
		return false, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = fmt.Errorf("Ошибка запроса: %v", err)
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			resp.Body.Close()
			continue
		}

		if w.Profile.Platform == "tg" {
			var response WapiContactResponse
			if err := json.Unmarshal(responseBody, &response); err != nil {
				globalErr = fmt.Errorf("не распарсили ответ: %v", err)
				continue
			}
			if len(response.Contact.ID) > 0 {
				return true, nil
			} else {
				return false, nil
			}
		} else if w.Profile.Platform == "wz" {
			response := &WapiIsRegistredResponse{}
			if err := json.Unmarshal(responseBody, &response); err != nil {
				globalErr = fmt.Errorf("не распарсили ответ: %v", err)
				continue
			}
			return response.OnWhatsapp, nil
		} else {
			return false, fmt.Errorf("неизвестная платформа: %s", w.Profile.Platform)
		}

	}
	return false, globalErr
}

func (w *Wapi) GetContactInfo(recipinet string, intervals int) (*wamodels.ContactInfo, error) {
	var globalErr error
	var method string

	if w.Profile.Platform == "tg" {
		method = "tapi/sync/contact/get?recipient="
	} else if w.Profile.Platform == "wz" {
		method = "api/sync/contact/info?user_id="
	} else {
		return nil, fmt.Errorf("неизвестная платформа: %s", w.Profile.Platform)
	}
	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("GET", wapi_url+method+recipinet+"&profile_id="+w.Profile.UUID+"&add=true", nil)

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = fmt.Errorf("ошибка запроса: %v", err)
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		defer resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			continue
		}
		response := &wamodels.ContactInfo{}
		response.ChannelType = w.Profile.Platform
		// if w.Profile.Platform == "tg" {
		// 	return nil, fmt.Errorf("tg get info not implemented")
		// } else if w.Profile.Platform == "wz" {

		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
			//return nil, fmt.Errorf("не распарсили ответ: %v", err)
		}
		return response, nil
		// } else {
		// 	return nil, fmt.Errorf("неизвестная платформа: %s", w.Profile.Platform)
		// }

	}
	return nil, globalErr
}

func (w *Wapi) SendAsyncTextMessageWithBotID(recipinet, body string, timeout_from, timeout_to int, intervals int, bot_id string) (*WapiStringResponse, error) {

	var globalErr error

	jsonMSG := &JsonBody{
		Recipient:   recipinet,
		Body:        body,
		TimeoutFrom: timeout_from,
		TimeoutTo:   timeout_to,
	}

	msg, err := json.Marshal(jsonMSG)
	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}

	var method = "/async/message/send"
	var platform string

	if w.Profile.Platform == "tg" {
		platform = "tapi"
	} else if w.Profile.Platform == "wz" {
		platform = "api"
	} else if w.Profile.Platform == "av" {
		method = "/sync/message/send"
		platform = "avito"
	} else {
		platform = "api"
	}

	fullUrl := fmt.Sprintf("https://%s/%s%s?profile_id=%s&timeout_from=%d&timeout_to=%d&bot_id=%s", os.Getenv("WAPI_HOST"), platform, method, w.Profile.UUID, timeout_from, timeout_to, bot_id)
	req, err := http.NewRequest("POST", fullUrl, bytes.NewBuffer(msg))

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}

// пометить все сообщения в чате прочитанными
func (w *Wapi) MarkAllRead(chatId string, intervals int) (*WapiStringResponse, error) {

	var globalErr error
	var method = "/sync/messages/get"
	var platform string

	switch w.Profile.Platform {
	case "tg":
		platform = "tapi"
	case "wz":
		platform = "api"
	default:
		return nil, fmt.Errorf("тип профиля не поддерживатеся для метода : %v", w.Profile.Platform)
	}

	wapi_url := "https://" + os.Getenv("WAPI_HOST") + "/"
	req, err := http.NewRequest("GET", wapi_url+platform+method+"?profile_id="+w.Profile.UUID+"&chat_id="+chatId+"&limit=1&mark_all=true", nil)

	if err != nil {
		return nil, fmt.Errorf("не сформировал реквест: %v", err)
	}
	req.Header.Set("User-Agent", "WAPPI WH")
	req.Header.Set("Authorization", w.Token.Key)

	httpClient := &http.Client{}

	for attempt := 0; attempt <= intervals; attempt++ {
		if attempt > 0 {
			time.Sleep(time.Duration(attempt) * 2 * time.Second)
		}

		resp, err := httpClient.Do(req)
		if err != nil {
			globalErr = err
			log.Errorf("Ошибка запроса: %v", err)
			continue
		}

		responseBody, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			globalErr = fmt.Errorf("не считали тело ответа: %v", err)
			continue
		}

		var response WapiStringResponse
		if err := json.Unmarshal(responseBody, &response); err != nil {
			globalErr = fmt.Errorf("не распарсили ответ: %v", err)
			continue
		}

		if resp.StatusCode != 200 {
			log.Errorf("Статус код не 200, profileUUID: %s, StatusCode: %d", w.Profile.UUID, resp.StatusCode)
			if response.Detail != "" {
				log.Errorf("Ошибка: %s", response.Detail)
			}
			resp.Body.Close()
			continue
		}
		return &response, nil
	}
	return nil, globalErr
}
