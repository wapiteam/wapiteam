package wapi

import "time"

type ProfileMapa struct {
	ProfileUUID string   `json:"profile_id"`
	Chats       []string `json:"chats"`
}

// CREATE TABLE fcm_tokens (
//     id SERIAL PRIMARY KEY,
//     user_id INTEGER NOT NULL,
//     token TEXT NOT NULL,
//     device_id TEXT NOT NULL,
//     platform TEXT NOT NULL,
//     created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
//     updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
// );

type FCMToken struct {
	ID        int       `json:"id"`
	UserID    int       `json:"user_id"`
	Token     string    `json:"token"`
	DeviceID  string    `json:"device_id"`
	Platform  string    `json:"platform"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
