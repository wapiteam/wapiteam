package wapi

type TGApp struct {
	ID     int    `json:"id"`
	AppID  int    `json:"app_id"`
	UserID int    `json:"user_id" `
	Token  string `json:"token"`
}
