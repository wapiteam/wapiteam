package retailclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type RetailClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewRetailClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *RetailClient {
	return &RetailClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *RetailClient) SendMessage(mssg *RetailMessage) error {

	jmsg, err := json.Marshal(mssg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
