package retailclient

import (
	v1 "github.com/retailcrm/mg-transport-api-client-go/v1"
	"gitlab.com/wapiteam/wapiteam/wapi"
)

// model
type RetailMessage struct {
	v1.SendData
	Event        string             `json:"event"`
	ProfileUUID  string             `json:"ProfileUUID"`
	IsFromMe     bool               `json:"IsFromMe"`
	AttachedUrl  string             `json:"AttachedUrl"`
	FileName     string             `json:"FileName"`
	FromWhere    string             `json:"from_where"`
	FromMsgrType string             `json:"from_msgr_type"`
	IsReply      bool               `json:"is_reply"`
	ChatType     string             `json:"chatType"`
	Attachments  *[]wapi.Attachment `json:"attachments,omitempty"`
}
