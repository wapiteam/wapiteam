package cascade

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/wapiteam/wapiteam/wapi"
)

type CasClient struct {
	domain string
	token  string
}

func InitClient(domain, token string) *CasClient {
	return &CasClient{
		domain: domain,
		token:  token,
	}
}

func (cc *CasClient) SendMessage(cr CasRequest) (*wapi.WapiStringResponse, error) {
	url := cc.domain + "/csender/cascade/send"
	payload, _ := json.Marshal(cr)
	resp, status, err := sendRequest(url, payload, "POST", cc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error send cascade message... resp: %v, status: %v, error: %v", string(resp), status, err)
	}
	var answer wapi.WapiStringResponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v\n recieved data: %v", err, string(resp))
	}
	return &answer, nil
}

func (cc *CasClient) GetCascade(csId string) (*CasReresponse, error) {
	url := cc.domain + "/csender/cascade/get?cascade_id=" + csId

	resp, status, err := sendRequest(url, []byte(""), "GET", cc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error get cascade... resp: %v, status: %v, error: %v", string(resp), status, err)
	}
	var answer CasReresponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v\n recieved data: %v", err, string(resp))
	}
	return &answer, nil
}

func (cc *CasClient) UpdateCascade(cs Cascade) (*CasReresponse, error) {
	url := cc.domain + "/csender/cascade/update"
	payload, _ := json.Marshal(cs)
	resp, status, err := sendRequest(url, payload, "POST", cc.token)
	if err != nil || status != 200 {
		return nil, fmt.Errorf("error update cascade message... resp: %v, status: %v, error: %v", string(resp), status, err)
	}
	var answer CasReresponse
	err = json.Unmarshal(resp, &answer)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v\n recieved data: %v", err, string(resp))
	}
	return &answer, nil
}

func sendRequest(url string, payload []byte, reqType, token string) ([]byte, int, error) {
	req, err := http.NewRequest(reqType, url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, 0, fmt.Errorf("error creating request:%v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	client := &http.Client{
		Timeout: 20 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, fmt.Errorf("error sending request:%v", err)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, fmt.Errorf("error reading body:%v", err)
	}
	defer resp.Body.Close()
	return body, resp.StatusCode, nil
}
