package cascade

type CasRequest struct {
	Recipient string `json:"recipient"`
	Body      string `json:"body"`
	Caption   string `json:"caption"`
	CascadeID string `json:"cascade_id"`
	FileName  string `json:"file_name"`
	URL       string `json:"url"`
}

type CasReresponse struct {
	Cascade Cascade `json:"cascade"`
	Status  string  `json:"status"`
}

type Cascade struct {
	CascadeID   string  `json:"cascade_id" bson:"cascade_id"`
	Name        string  `json:"name" bson:"name"`
	Description string  `json:"description" bson:"description"`
	Orders      []Order `json:"order" bson:"order"`
	CrmID       string  `json:"crm_id" bson:"crm_id"`
	CrmType     string  `json:"crm_type" bson:"crm_type"`
}

type Order struct {
	ID          int    `json:"id" bson:"_id"`
	ProfileUUID string `json:"profile_uuid" bson:"profile_uuid"`
	Platform    string `json:"platform" bson:"platform"`
}

type CascadeRequest struct {
	CascadeID string `json:"cascade_id" bson:"cascade_id"`
	UserID    string `json:"user_id" bson:"user_id"`
	Token     string `json:"token" bson:"token"`
}

type JsonBody struct {
	Recipient string `json:"recipient,omitempty"`
	Body      string `json:"body,omitempty"`
	Caption   string `json:"caption,omitempty"`
	CascadeID string `json:"cascade_id,omitempty"`
	FileName  string `json:"file_name,omitempty"`
	Url       string `json:"url,omitempty"`
}

type WapiRequest struct {
	Recipient string `json:"recipient,omitempty"`
	Body      string `json:"body,omitempty"`
	Caption   string `json:"caption,omitempty"`
	FileName  string `json:"file_name,omitempty"`
	Url       string `json:"url,omitempty"`
}
