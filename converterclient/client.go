package converterclient

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strings"

	"gitlab.com/wapiteam/wapiteam/wapilog"
)

var log = wapilog.NewWapiLogger("INFO", "")

type ConverterClient struct {
	Url string
}

type OGGtoMp3Response struct {
	Link     string `json:"link"`
	Filename string `json:"filename"`
}

func NewConverterClient(url string) *ConverterClient {
	return &ConverterClient{
		Url: url,
	}
}

func (cc *ConverterClient) Mp4ToMp3(url string) (*OGGtoMp3Response, error) {

	payload := strings.NewReader(`{
  "url": "` + url + `"
}`)

	request, err := http.NewRequest("POST", cc.Url, payload)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	resp, err := client.Do(request)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	var result *OGGtoMp3Response
	json.Unmarshal(respBody, &result)

	return result, nil
}

func (cc *ConverterClient) OggToMp3(ogg []byte) (*OGGtoMp3Response, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	part, err := writer.CreateFormFile("file", "file.ogg")
	if err != nil {
		return nil, err
	}
	_, err = part.Write(ogg)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}
	err = writer.Close()
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	request, err := http.NewRequest("POST", cc.Url, body)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}
	request.Header.Set("Content-Type", writer.FormDataContentType())

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	resp, err := client.Do(request)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	var result *OGGtoMp3Response
	json.Unmarshal(respBody, &result)

	return result, nil
}
