package notifyclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapilog"
	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

var log = wapilog.NewWapiLogger("INFO", "")

type NotifierClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewNotifierClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *NotifierClient {
	return &NotifierClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *NotifierClient) SendMessage(msg *NotifierMessage) error {

	log.Infof("Sending message to exchange %s with routing key %s", bc.Exchange, bc.RoutingKey)

	jmsg, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	log.Resultf("Sending message %v", msg)

	return nil
}
