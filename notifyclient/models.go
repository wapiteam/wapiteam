package notifyclient

import "time"

type NotifierMessage struct {
	Event           string          `json:"event"`
	Details         string          `json:"details,omitempty"`
	ProfileUUID     string          `json:"profile_uuid,omitempty"`
	DataDebtMessage DebtDataMessage `json:"data_debt_message,omitempty"`
}

type DebtDataMessage struct {
	Debtor3Profiles []DebtorUser `json:"debtor_3_profiles,omitempty"`
	Debtor0Profiles []DebtorUser `json:"debtor_0_profiles,omitempty"`
	Date3           *time.Time   `json:"date3,omitempty"`
	Date0           *time.Time   `json:"date0,omitempty"`
}

type DebtorUser struct {
	Name string `json:"name,omitempty"`
	UUID string `json:"uuid,omitempty"`
}
