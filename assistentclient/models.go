package assistentclient

import "gitlab.com/wapiteam/wapiteam/wapi"

type AssistentMessage struct {
	NewMsg        *wapi.IncomingOutgoingMessage
	Ack           *wapi.AckWH
	AssistantType string `json:"assistant_type" bson:"assistant_type"`
	// Event
	Event           string           `json:"event" bson:"event"`
	WapiAvitoAdInfo *WapiAvitoAdInfo `json:"wapi_avito_ad_info,omitempty"`
	UserID          int              `json:"user_id" bson:"user_id"`
}

type WapiAvitoAdInfo struct {
	Status     string `json:"status"`
	StartTime  string `json:"start_time"`
	FinishTime string `json:"finish_time"`
	Url        string `json:"url"`
	Name       string `json:"name"`
	Price      string `json:"price"`
	BuyerName  string `json:"buyer_name"`
	BuyerUrl   string `json:"buyer_url"`
	DialogUrl  string `json:"dialog_url"`
	Location   string `json:"location"`
}

type WapiAvitoAdResponse struct {
	Item   WapiAvitoAdInfo `json:"item,omitempty"`
	Status string          `json:"status,omitempty"`
}
