package assistentclient

import (
	"context"
	"encoding/json"

	"gitlab.com/wapiteam/wapiteam/wapirabbit"
)

type AssistentClient struct {
	Exchange   string
	RoutingKey string
	broker     *wapirabbit.AmqpBroker
}

func NewAssistentClient(broker *wapirabbit.AmqpBroker, exchange string, routingKey string) *AssistentClient {
	return &AssistentClient{
		Exchange:   exchange,
		RoutingKey: routingKey,
		broker:     broker,
	}
}

func (bc *AssistentClient) SendMessage(mssg *AssistentMessage) error {

	jmsg, err := json.Marshal(mssg)
	if err != nil {
		return err
	}

	if err := bc.broker.Publish(context.TODO(), bc.Exchange, bc.RoutingKey, jmsg); err != nil {
		return err
	}

	return nil
}
